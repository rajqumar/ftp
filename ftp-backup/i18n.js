const NextI18Next = require('next-i18next').default;
const config = require('next/config').default();
const get = require('lodash/get');

const localeSubpaths = get(config, 'publicRuntimeConfig.localeSubpaths', 'none');

const EN = 'en';
const ES = 'es';
const SC = 'sc';

const localeSubpathMapping = {
  none: {},
  en: EN,
  es: ES,
  sc: SC,
  all: {
    en: EN,
    es: ES,
    sc: SC,
  },
};

module.exports = new NextI18Next({
  defaultNS: 'common',
  defaultLanguage: 'es',
  otherLanguages: [EN, ES, SC],
  localeSubpaths: localeSubpathMapping[localeSubpaths],
});
