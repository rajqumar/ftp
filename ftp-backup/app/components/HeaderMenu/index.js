import React, { useState } from 'react';
import { Collapse, NavbarToggler, Nav } from 'reactstrap';
import MenuItems from '../MenuItems';
import { faSearch, faUser, faUserPlus } from '@fortawesome/free-solid-svg-icons';

// conditions to follow for this component
// menu ==> logged out state
// toggle button ==> buyer / seller logged in dashboard
// blank ==> manager / admin logged in dashboard

const HeaderMenu = () => {
  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen);

  var menus = [
    { name: 'Search Parts', icon: faSearch, url: '/' },
    { name: 'Login', icon: faUser, url: '/login' },
    { name: 'Register', icon: faUserPlus, url: '/register' },
    // {name:'Dashboard',icon:faUserPlus,url:'/dashboard'},
    // {name:'Profile',icon:faUserPlus,url:'/profile'},
    //{name:'Users',icon:faUserPlus,url:'/users'},
    //{name:'CreateAdmin',icon:faUserPlus,url:'/createAdmin'},
    //{name:'IntegrateERP',icon:faUserPlus,url:'/erpIntegration'}
  ];

  return (
    <div>
      <NavbarToggler onClick={toggle} className="mr-2" />
      <Collapse isOpen={isOpen} navbar>
        <Nav navbar>
          {menus.map((m, i) => (
            <MenuItems key={i} name={m.name} icon={m.icon} url={m.url} />
          ))}
        </Nav>
      </Collapse>
    </div>
  );
};

export default HeaderMenu;
