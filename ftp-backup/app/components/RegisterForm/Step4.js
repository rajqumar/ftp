import React from 'react';
import Link from 'next/link';
import PropTypes from 'prop-types';

function Step4(props) {
  const { step } = props;

  if (step !== 4) {
    return null;
  }
  return (
    <React.Fragment>
      <fieldset className="col-md-12">
        <h2 className="fs_title_last">
          <i className="fa fa-check" aria-hidden="true"></i> Thank you
        </h2>

        <p className="thank_msg">
          Entrusted by our community, we will do some verification checks on your submitted information and will contact
          you at +65 9876 5432 in the next 5 working days.
        </p>
        <center>
          <a href="register.html">
            <Link href="/">
              <button type="button" className="btn btn-primary back_to_home">
                Back to Home
              </button>
            </Link>
          </a>
        </center>
      </fieldset>
      <style jsx>{`
 .fs_title_last
 {
   text-align:center;
 }
 .thank_msg 
 {
  margin: 0 auto;
  font-size: 14px;
  max-width: 50%;
  color: #000;
  text-align: center;
}
 fieldset {
  border: 0 none;
  border-radius: 0px;
  padding: 20px 30px;
transform:unset !important;
  box-sizing: border-box;
  position: relative !important;
}

 fieldset:not(:first-of-type) {
  display: none;
}

/*inputs*/
 input,  textarea {
 display: block;
  width: 100%;
  height: 34px;
  padding: 6px 12px;
  font-size: 14px;
  line-height: 1.42857143;
  color: #555;
margin-bottom:20px;
  background-color: #fff;
  background-image: none;
  border: 1px solid #ccc;
  border-radius: 4px;
  -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
  box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
  -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
  -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
  transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
}

 input:focus,  textarea:focus {
  -moz-box-shadow: none !important;
  -webkit-box-shadow: none !important;
  box-shadow: none !important;
  border: 1px solid #d2232a;
  outline-width: 0;
 
}

/*buttons*/

 .action-button {
    width: 150px;
  background: #d2232a;
  color: #fff;
  border: 0 none;
  cursor: pointer;
  padding: 5px;
float:right;
  margin: 15px auto;
}
 .back_to_home
{
  width: 150px;
  background: #d2232a;
  color: #fff;
  border: 0 none;
  cursor: pointer;
  padding: 5px;
  margin: 15px auto;
}
 .action-button-previous {
  
width: 150px;
  background: #d2232a;
  color: #fff;
  border: 0 none;
  display: -webkit-inline-box;
  cursor: pointer;
  padding: 5px;
float:right;
  margin: 15px 15px;
}
}
      `}</style>
    </React.Fragment>
  );
}

Step4.propTypes = {
  step: PropTypes.number,
  handleChange: PropTypes.func,
};

export default Step4;
