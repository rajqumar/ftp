import React from 'react';
import PropTypes from 'prop-types';

function Step2(props) {
  const { step, handleChange } = props;

  if (step !== 2) {
    return null;
  }
  return (
    <React.Fragment>
      <fieldset className="col-md-12">
        <h2 className="fs-title">
          <i className="fa fa-user" aria-hidden="true"></i> Your Information
        </h2>
        <div className="row">
          <div className="col-md-6">
            <div className="form-group">
              <label htmlFor="name" className="fl-left">
                Name
              </label>
              <input
                type="text"
                className="form-control"
                id="u_name"
                name="u_name"
                placeholder="John Applessed"
                onChange={handleChange}
              />
            </div>

            <div className="form-group">
              <label htmlFor="Title" className="fl-left">
                Title
              </label>
              <input
                type="text"
                className="form-control"
                id="u_title"
                name="u_title"
                placeholder="Director"
                onChange={handleChange}
              />
            </div>
          </div>
          <div className="col-md-6">
            <div className="form-group">
              <label htmlFor="email" className="fl-left">
                Email
              </label>
              <input
                type="email"
                className="form-control"
                id="u_email"
                name="u_email"
                placeholder="name@example.com"
                onChange={handleChange}
              />
            </div>

            <div className="form-group">
              <label htmlFor="Phone" className="fl-left">
                Phone
              </label>
              <input
                type="number"
                className="form-control"
                id="u_phone"
                name="u_phone"
                placeholder="+65 9876 5432"
                onChange={handleChange}
              />
            </div>
          </div>
        </div>
      </fieldset>
      <style jsx>{`
            .action-button {
              width: 150px;
              background: #d2232a;
              color: #fff;
              border: 0 none;
              cursor: pointer;
              padding: 5px;
              float: right;
              margin: 15px auto;
          }
.fl-left {
  float: left;
  font-size: 16px;
  margin-bottom: 5px;
  font-weight: 700;
}
 fieldset {
  border: 0 none;
  border-radius: 0px;
  padding: 20px 30px;
transform:unset !important;
  box-sizing: border-box;
  position: relative !important;
}

 fieldset:not(:first-of-type) {
  display: none;
}

/*inputs*/
 input,  textarea {
 display: block;
  width: 100%;
  height: 34px;
  padding: 6px 12px;
  font-size: 14px;
  line-height: 1.42857143;
  color: #555;
margin-bottom:20px;
  background-color: #fff;
  background-image: none;
  border: 1px solid #ccc;
  border-radius: 4px;
  -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
  box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
  -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
  -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
  transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
}

 input:focus,  textarea:focus {
  -moz-box-shadow: none !important;
  -webkit-box-shadow: none !important;
  box-shadow: none !important;
  border: 1px solid #d2232a;
  outline-width: 0;
 
}

/*buttons*/

 .action-button {
    width: 150px;
  background: #d2232a;
  color: #fff;
  border: 0 none;
  cursor: pointer;
  padding: 5px;
float:right;
  margin: 15px auto;
}
 .back_to_home
{
  width: 150px;
  background: #d2232a;
  color: #fff;
  border: 0 none;
  cursor: pointer;
  padding: 5px;
  margin: 15px auto;
}
 .action-button-previous {
  
width: 150px;
  background: #d2232a;
  color: #fff;
  border: 0 none;
  display: -webkit-inline-box;
  cursor: pointer;
  padding: 5px;
float:right;
  margin: 15px 15px;
}
}
      `}</style>
    </React.Fragment>
  );
}

Step2.propTypes = {
  step: PropTypes.number,
  handleChange: PropTypes.func,
};

export default Step2;
