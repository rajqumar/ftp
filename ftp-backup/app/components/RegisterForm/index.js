import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useToasts } from 'react-toast-notifications';
import axios from 'axios';

import Step1 from './Step1';
import Step2 from './Step2';
import Step3 from './Step3';
import Step4 from './Step4';

const RegisterForm = () => {
  const { addToast } = useToasts();

  const [step, setStep] = useState(1);
  const [submitted, setSubmit] = useState(false);

  const [form, setState] = useState({
    c_name: '',
    c_uen: '',
    c_phone: '',
    c_email: '',
    c_addr1: '',
    c_addr2: '',
    country: '',
    u_name: '',
    u_email: '',
    u_title: '',
    u_phone: '',
  });

  const handleChange = e => {
    const { name, value } = e.target;
    setState({
      ...form,
      [name]: value,
    });
  };

  const handleSubmit = () => {
    var { c_name, c_uen, c_phone, c_email, c_addr1, c_addr2, country, u_name, u_email, u_title, u_phone } = form;

    var sn = u_name.split(' ');

    var data = {
      u_l_name: sn[1],
      u_f_name: sn[0],
      u_email: u_email,
      password: 'password',
      u_phone: u_phone,
      u_title: u_title,
      c_name: c_name,
      c_uen: c_uen,
      c_email: c_email,
      c_addr1: c_addr1,
      c_addr2: c_addr2,
      c_phone: c_phone,
      country: country,
    };

    axios
      .post('http://ftp-backend-git-ftp-backends.apps.us-east-1.starter.openshift-online.com/api/create-company/', data)
      .then(r => {
        // console.log('API *** ', r);
        setSubmit(true);
        addToast('Saved Successfully', { appearance: 'success', autoDismiss: true });
        return r;
      })
      .catch(e => {
        // console.log(e);
        setSubmit(true);
        addToast('error.message', { appearance: 'error', autoDismiss: true });
        return e;
      });

    // u_name => split into 2 u_f_name u_l_name
  };

  const _next = () => setStep(step >= 3 ? 4 : step + 1);

  const _prev = () => setStep(step <= 1 ? 1 : step - 1);

  const previousButton = () => {
    if (step !== 1 && step !== 4) {
      return (
        <button className="btn btn-secondary action-button-previous" type="button" onClick={_prev}>
          Previous
          <style jsx>{`
            .action-button-previous {
              width: 150px;
              background: #d2232a;
              color: #fff;
              border: 0 none;
              display: -webkit-inline-box;
              cursor: pointer;
              padding: 5px;
              float: right;
              margin: 15px 15px;
            }
          `}</style>
        </button>
      );
    }
    return null;
  };

  const nextButton = () => {
    if (step === 4 && !submitted) {
      handleSubmit();
    }
    if (step < 4) {
      return (
        <button className="btn btn-success action-button" type="button" onClick={_next}>
          Next
          <style jsx>{`
            .action-button {
              width: 150px;
              background: #d2232a;
              color: #fff;
              border: 0 none;
              cursor: pointer;
              padding: 5px;
              float: right;
              margin: 15px auto;
            }
          `}</style>
        </button>
      );
    }
    return null;
  };

  return (
    <React.Fragment>
      <ul id="progressbar">
        <li className="active">Company Information</li>
        <li>Your Information</li>
        <li>Review</li>
        <li>Verification</li>
      </ul>

      <form id="msform">
        <Step1
          step={step}
          handleChange={handleChange}
          c_name={form.c_name}
          c_uen={form.c_uen}
          c_phone={form.c_phone}
          c_email={form.c_email}
          c_addr1={form.c_addr1}
          c_addr2={form.c_addr2}
          country={form.country}
        />

        <Step2
          step={step}
          handleChange={handleChange}
          u_name={form.u_name}
          u_email={form.u_email}
          u_title={form.u_title}
          u_phone={form.u_phone}
        />

        <Step3 step={step} handleChange={handleChange} />
        <Step4 step={step} handleChange={handleChange} />

        {previousButton()}
        {nextButton()}
      </form>

      <style jsx>{`
        .action-button {
          width: 150px;
          background: #d2232a;
          color: #fff;
          border: 0 none;
          cursor: pointer;
          padding: 5px;
          float: right;
          margin: 15px auto;
        }
        .#progressbar {
          overflow: hidden;
          counter-reset: step;
        }
        #progressbar li {
          list-style-type: none;
          color: rgba(34, 34, 34, 1);
          text-transform: capitalize;
          font-size: 12px;
          width: 24.33%;
          font-weight: 500;
          float: left;
          z-index: 1001;
          position: relative;
          letter-spacing: 1px;
        }
        #progressbar li.active:before,
        #progressbar li.active:after {
          background: #d2232a;
          color: white;
        }
        #progressbar li:before {
          content: '';
          width: 15px;
          height: 15px;
          line-height: 26px;
          display: block;
          font-size: 12px;
          color: #333;
          background: #fff;
          border: 1px solid #ccc;
          border-radius: 25px;
          margin: 0 auto 10px auto;
          z-index: 1000;
        }
        #progressbar li:after {
          content: '';
          width: 100%;
          height: 2px;
          background: #ccc;
          position: absolute;
          left: -45%;
          top: 7px;
          z-index: -1;
        }
      `}</style>
    </React.Fragment>
  );
};

RegisterForm.propTypes = {
  step: PropTypes.number,
  handleChange: PropTypes.func,
};

export default RegisterForm;
