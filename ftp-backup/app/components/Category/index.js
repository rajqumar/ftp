import React from 'react';

function Category() {
  return (
    <div>
      <h3 className="category_head">Browse Parts by Category</h3>
      <ul className="browse_list">
        <li>
          <a href="#">
            <div id="box-1" className="white-box">
              <span className="box-icon">
                <img src="/static/images/integrated_circuits.png" className="img-responsive" />
              </span>
              <strong className="box-title">Integrated Circuits</strong>
            </div>
          </a>
        </li>
        <li>
          <a href="#">
            <div id="box-1" className="white-box">
              <span className="box-icon">
                <img src="/static/images/discrete_semiconducors.png" className="img-responsive" />
              </span>
              <strong className="box-title">Discrete Semiconductors</strong>
            </div>
          </a>
        </li>
        <li>
          <a href="#">
            <div id="box-1" className="white-box">
              <span className="box-icon">
                <img src="/static/images/passive_conductors.png" className="img-responsive" />
              </span>
              <strong className="box-title">Passive Components</strong>
            </div>
          </a>
        </li>

        <li>
          <a href="#">
            <div id="box-1" className="white-box">
              <span className="box-icon">
                <img src="/static/images/circuit_protection.png" className="img-responsive" />
              </span>
              <strong className="box-title">Circuit Protection</strong>
            </div>
          </a>
        </li>
      </ul>
      <ul className="browse_list">
        <li>
          <a href="#">
            <div id="box-1" className="white-box">
              <span className="box-icon">
                <img src="/static/images/connectors.png" className="img-responsive" />
              </span>
              <strong className="box-title">Connectors</strong>
            </div>
          </a>
        </li>
        <li>
          <a href="#">
            <div id="box-1" className="white-box">
              <span className="box-icon">
                <img src="/static/images/sensors.png" className="img-responsive" />
              </span>
              <strong className="box-title">Sensors</strong>
            </div>
          </a>
        </li>
        <li>
          <a href="#">
            <div id="box-1" className="white-box">
              <span className="box-icon">
                <img src="/static/images/optoelectronics.png" className="img-responsive" />
              </span>
              <strong className="box-title">Optoelectronics</strong>
            </div>
          </a>
        </li>
        <li>
          <a href="#">
            <div id="box-1" className="white-box">
              <span className="box-icon">
                <img src="/static/images/power_products.png" className="img-responsive" />
              </span>
              <strong className="box-title">Power Products</strong>
            </div>
          </a>
        </li>
      </ul>
      <ul className="browse_list">
        <li>
          <a href="#">
            <div id="box-1" className="white-box">
              <span className="box-icon">
                <img src="/static/images/electromechanical.png" className="img-responsive" />
              </span>
              <strong className="box-title">Electromechanical</strong>
            </div>
          </a>
        </li>
        <li>
          <a href="#">
            <div id="box-1" className="white-box">
              <span className="box-icon">
                <img src="/static/images/cables_wires.png" className="img-responsive" />
              </span>
              <strong className="box-title">Cables & Wire</strong>
            </div>
          </a>
        </li>
      </ul>
      <style>{`
 .ftp_first_block {
    margin-top: 25px;
}
.about_ftp {
    text-align: left;
    font-size: 19px;
    font-weight: 600;
    word-break: break-all;
    margin: 0px;
    padding: 10px 0 5px 10px;
    color: #2b2626;
    border-radius: 5px 5px 0 0;
}
.box-icon img {
    height: 50px;
    width: 50px;
    margin: 10px auto;
}
.box-title {
    color: #333;
    font-weight: 400;
    font-size: 12px;
    line-height: 0;
    word-break: break-word;
}
.line {
    height: 2px;
    width: 100px;
    margin-left: 10px;
    margin-bottom: 10px;
    color: #d31e25;
    background-color: #f14141;
}
.list_man {
    list-style: none;
    margin-left: -40px;
    margin-bottom: 0px;
}
.list_man li {
    color: #423636;
    font-weight: 600;
    line-height: 30px;
    padding: 0px 10px;
    font-size: 14px;
    border-bottom: 1px dashed #e2dfdfab;
    cursor: pointer;
}
.category_head {
    padding: 30px 0;
    text-align: center;
    font-weight: 700;
    font-size: 28px;
  }

  .browse_list {
    display: inline-flex;
    width: 110%;
    list-style: none;
    margin-left: -40px;
}
  .browse_list li {
    margin-right: 10px;
    width: 20%;
  }
  .col-md-5 {
    width: 41.66666667%;
}
.white-box {
    display: block;
    background-color: #FAFAFA;
    border-radius: 3px;
    width: 100px;
    padding: 7px;
    height: 130px;
    text-align: center;
    box-shadow: 0 15px 45px 0 rgba(0, 0, 0, 0.05);
    transition: all 0.3s ease-in-out 0s;
    border: 1px solid #e1e0e0ba;
    transition: transform .5s ease;
}`}</style>
    </div>
  );
}
export default Category;
