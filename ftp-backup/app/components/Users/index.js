import React from 'react';
import Link from 'next/link';

const UsersList = () => {
  return (
    <div>
      <div className="create_user_block">
        <button type="button" className="btn btn-success create_user_btn">
          Create User
        </button>
        <button type="button" className="btn btn-defualt create_admin_btn">
          <Link href="/createAdmin">Create Admin</Link>
        </button>
      </div>
      <div className="box">
        <div className="box-body">
          <div className="table-responsive">
            <table id="example1" className="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>User</th>
                  <th>Type</th>
                  <th>Role</th>
                  <th>Last Login</th>
                  <th>Status</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>
                    <p className="red_name">Shrishailya Deshmukh</p>
                    <p className="mb-0">
                      <a href="#">
                        <i className="fa fa-mobile mob_icon" aria-hidden="true"></i>
                      </a>
                      <a href="#">
                        <i className="fa fa-envelope mail_icon" aria-hidden="true"></i>
                      </a>
                    </p>
                  </td>
                  <td>Admin</td>
                  <td>IT Manager</td>
                  <td>1 Jan 2019, 1.40 PM</td>
                  <td>
                    <button type="button" className="btn btn-success btn_active">
                      Active
                    </button>
                  </td>
                  <td></td>
                </tr>

                <tr>
                  <td>
                    <p className="red_name">Vijendra Patil</p>
                    <p className="mb-0">
                      <a href="#">
                        <i className="fa fa-mobile mob_icon" aria-hidden="true"></i>
                      </a>
                      <a href="#">
                        <i className="fa fa-envelope mail_icon" aria-hidden="true"></i>
                      </a>
                    </p>
                  </td>
                  <td>Admin</td>
                  <td>Procurement Manager</td>
                  <td>1 Jan 2019, 1.40 PM</td>
                  <td>
                    <button type="button" className="btn btn-success btn_active">
                      Active
                    </button>
                  </td>
                  <td></td>
                </tr>

                <tr>
                  <td>
                    <p className="red_name">Pranshu Rastogi</p>
                    <p className="mb-0">
                      <a href="#">
                        <i className="fa fa-mobile mob_icon" aria-hidden="true"></i>
                      </a>
                      <a href="#">
                        <i className="fa fa-envelope mail_icon" aria-hidden="true"></i>
                      </a>
                    </p>
                  </td>
                  <td>User</td>
                  <td>Procurement</td>
                  <td>1 Jan 2019, 1.40 PM</td>
                  <td>
                    <button type="button" className="btn btn-success btn_active">
                      Active
                    </button>
                  </td>
                  <td></td>
                </tr>

                <tr>
                  <td>
                    <p className="red_name">Rajkumar Tiwari</p>
                    <p className="mb-0">
                      <a href="#">
                        <i className="fa fa-mobile mob_icon" aria-hidden="true"></i>
                      </a>
                      <a href="#">
                        <i className="fa fa-envelope mail_icon" aria-hidden="true"></i>
                      </a>
                    </p>
                  </td>
                  <td>User</td>
                  <td>Fianance</td>
                  <td>2 Jan 2019, 1.40 PM</td>
                  <td>
                    <button type="button" className="btn btn-success btn_inactive">
                      Inactive
                    </button>
                  </td>
                  <td></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <style jsx>{`
      create_user_block {
        margin-bottom: 30px;
        margin-top: 10px;
    }
    .create_user_btn {
      background-color: #6DC144;
      border: 1px solid #6DC144;
      color: #fff;
      text-transform: uppercase;
      margin: 0 10px;
      font-size: 12px;
      padding: 10px 20px;
    }
    .create_admin_btn {
      background-color: #fff;
      border: 1px solid #ccc;
      color: #000;
      text-transform: uppercase;
      margin: 0 10px;
      font-size: 12px;
      padding: 10px 20px;
  }
  .box {
    position: relative;
    border-radius: 3px;
    background: #ffffff;
    border-top: 3px solid #d2d6de;
    margin-bottom: 20px;
    width: 100%;
    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
}
  box-body {
    border-top-left-radius: 0;
    border-top-right-radius: 0;
    border-bottom-right-radius: 3px;
    border-bottom-left-radius: 3px;
    padding: 10px;
}
.box-body:before, .box-footer:before, .box-header:after, .box-body:after, .box-footer:after {
  content: " ";
  display: table;
}

}
.btn_inactive {
  background-color: #D31E25;
  border: 1px solid #D31E25;
  border-radius: 50px;
  font-size: 14px;
  padding: 0px 15px;
}
table.dataTable thead .sorting:after {
  opacity: 0.2;
  content: "e150";
}
.btn_active {
  background-color: #6DC144;
  border: 1px solid #6DC144;
  border-radius: 50px;
  font-size: 14px;
  padding: 0px 20px;
}
div.table-responsive>div.dataTables_wrapper>div.row>div[class^="col-"]:first-child {
    padding-left: 0;
}
.table-responsive {
  min-height: .01%;
  overflow-x: auto;
}
      `}</style>
    </div>
  );
};

export default UsersList;
