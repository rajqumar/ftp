import React from 'react';

function Inventory() {
  return (
    <div>
      <div className="inventory_block mt-25">
        <div className="table-responsive">
          <table className="table table-bordered inventory_table">
            <thead className="back_primary">
              <tr>
                <th scope="col" className="white border_left border_side_none f_18 fw-600">
                  Inventory
                </th>
                <th scope="col" className="white border_none">
                  <div className="btn-group">
                    <button
                      type="button"
                      className="btn btn-secondary dropdown-toggle btn-sm dropdown_qty fw-600"
                      data-toggle="dropdown"
                      aria-haspopup="true"
                      aria-expanded="false">
                      Part No
                    </button>
                    <ul className="dropdown-menu dropdown-menu-right drop_block">
                      <li className="dropdown-item drop_list">PSDJ2F6-DF</li>
                      <li className="dropdown-item drop_list">KBDI128-JK</li>
                      <li className="dropdown-item drop_list">QEQE2QW-OP</li>
                      <li className="dropdown-item drop_list">NCMS248-DM</li>
                    </ul>
                  </div>
                </th>
                <th scope="col" className="white border_none">
                  <div className="btn-group">
                    <button
                      type="button"
                      className="btn btn-secondary dropdown-toggle btn-sm dropdown_qty fw-600"
                      data-toggle="dropdown"
                      aria-haspopup="true"
                      aria-expanded="false">
                      Qty
                    </button>
                    <ul className="dropdown-menu dropdown-menu-right drop_block">
                      <li className="dropdown-item drop_list">1000</li>
                      <li className="dropdown-item drop_list">5000</li>
                      <li className="dropdown-item drop_list">6000</li>
                      <li className="dropdown-item drop_list">4000</li>
                    </ul>
                  </div>
                </th>
                <th scope="col" className="white border_right border_sider_none">
                  <div className="btn-group">
                    <button
                      type="button"
                      className="btn btn-secondary dropdown-toggle btn-sm dropdown_qty fw-600"
                      data-toggle="dropdown"
                      aria-haspopup="true"
                      aria-expanded="false">
                      Country
                    </button>
                    <ul className="dropdown-menu dropdown-menu-right drop_block">
                      <li className="dropdown-item drop_list">Singapore</li>
                      <li className="dropdown-item drop_list">India</li>
                      <li className="dropdown-item drop_list">Canada</li>
                      <li className="dropdown-item drop_list">China</li>
                    </ul>
                  </div>
                </th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th scope="row">Part No</th>
                <td>
                  <strong> Qty </strong>
                </td>
                <td>
                  <strong> Price(USD) </strong>
                </td>
                <td>
                  <strong> Company </strong>
                </td>
              </tr>
              <tr>
                <td scope="row">
                  PSDJ22326WFR16Z-DF
                  <br />
                  Sk Hynix
                </td>
                <td>1090</td>
                <td>1</td>
                <td>SRS Pvt Ltd.</td>
              </tr>
              <tr>
                <td scope="row">
                  KBDI1264928148-JK
                  <br />
                  Intel
                </td>
                <td>2320</td>
                <td>0.5</td>
                <td>Tash Pvt Ltd.</td>
              </tr>
              <tr>
                <td scope="row">
                  QEQE243124QW-OP
                  <br />
                  Taiwan conductor
                </td>
                <td>9876</td>
                <td>0.20</td>
                <td>Infra Pvt Ltd.</td>
              </tr>
              <tr>
                <td scope="row">
                  ZASK12492147-WQ
                  <br />
                  Micron Technology
                </td>
                <td>3456</td>
                <td>1</td>
                <td>Sunpower Ltd.</td>
              </tr>
              <tr>
                <td scope="row">
                  ALUSG1232141-DF
                  <br />
                  Broadcom
                </td>
                <td>1223</td>
                <td>1</td>
                <td>ideass Pvt Ltd.</td>
              </tr>
              <tr>
                <td scope="row">
                  IWQD13142242-PF
                  <br />
                  Qualcomm
                </td>
                <td>9752</td>
                <td>1.5</td>
                <td>Gamify Pvt Ltd.</td>
              </tr>
              <tr>
                <td scope="row">
                  QSLOIP312321-QW
                  <br />
                  Texas Instruments
                </td>
                <td>1090</td>
                <td>1</td>
                <td>Vasast Pvt Ltd.</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <style>{`

    .mt-25 {
        margin-top: 25px;
    }
    .table-responsive {
        min-height: .01%;
        overflow-x: auto;
    }
    .table-bordered {
        border: 1px solid #f4f4f4;
    }
    .back_primary {
        background-color: #D31E25;
    }
    .dropdown_qty {
        background: transparent;
        color: #fff;
        font-weight: 700;
        font-size: 15px;
        box-shadow: unset;
        /* border: 1px solid #fff; */
        border-radius: 50px;
        padding: 0px;
        margin-top: 0px;
        box-shadow: none !Important;
    }
    .fw-600 {
        font-weight: 600;
    }
    .drop_block {
        padding: 10px;
        text-align: left;
        min-width: 110px;
    }
    .dropdown-menu {
        box-shadow: none;
        border-color: #eee;
    }
    .dropdown-menu-right {
        right: 0;
        left: auto;
    }
    .dropdown-menu {
        position: absolute;
        top: 100%;
        left: 0;
        z-index: 1000;
        display: none;
        float: left;
        min-width: 160px;
        padding: 5px 0;
        margin: 2px 0 0;
        font-size: 14px;
        text-align: left;
        list-style: none;
        background-color: #fff;
        -webkit-background-clip: padding-box;
        background-clip: padding-box;
        border: 1px solid #ccc;
        border: 1px solid rgba(0,0,0,.15);
        border-radius: 4px;
        -webkit-box-shadow: 0 6px 12px rgba(0,0,0,.175);
        box-shadow: 0 6px 12px rgba(0,0,0,.175);
    }
    ol, ul {
        margin-top: 0;
        margin-bottom: 10px;
    }
    .btn-group.btn-group-vertical {
        position: relative;
        display: inline-block;
        vertical-align: middle;
    }
    
    `}</style>
    </div>
  );
}
export default Inventory;
