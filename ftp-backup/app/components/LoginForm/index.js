import React from 'react';
import Button from 'components/Button';
// import { useToasts } from 'react-toast-notifications';
import { reduxForm, Field } from 'redux-form';
// import Router from 'next/router';
import PropTypes from 'prop-types';

function LoginForm(props) {
  const { submitFormHandler, handleSubmit, loginToken } = props;

  // const { addToast } = useToasts();

  // if (loginToken) {
  // console.log(loginToken);
  // }

  //       // console.log('API *** ', r);
  // addToast('Logged in Successfully', { appearance: 'success', autoDismiss: true });
  //       Router.push('/');
  //       return;
  //     })
  //     .catch(e => {
  //       // console.log(e);
  // addToast('Something wrong', { appearance: 'error', autoDismiss: true });
  //       return;
  //     });

  //   // u_name => split into 2 u_f_name u_l_name
  // };

  return (
    <div className="login-box">
      <div className="login-box-body">
        <h4 className="login_head">Log In</h4>
        <p>{loginToken}</p>
        <form onSubmit={handleSubmit(submitFormHandler)}>
          <div className="form-group has-feedback">
            <label htmlFor="phone" className="fl-left">
              Email
            </label>
            <Field type="email" component="input" name="email" placeholder="name@example.com" />
          </div>

          <div className="form-group has-feedback">
            <label htmlFor="phone" className="fl-left">
              Password
            </label>
            <Field component="input" type="password" name="password" placeholder="Type a password here" />
          </div>
          <div className="row">
            <div className="col-md-12">
              <span>
                <Button color="red" value="LOGIN" shape="rect" position="left" className="btn-block" />
              </span>
            </div>
          </div>
        </form>

        <p className="ft_pass">
          <a href="#" className="forgot_pwd">
            forgot password?
          </a>
        </p>
        <p className="dont_class">
          {`Don't have an account?`}
          <a href="register.html" className="text-center red_color">
            &nbsp; Sign up
          </a>
        </p>
      </div>

      <style jsx>{`
        .login-box,
        .register-box {
          width: 360px;
          margin: 7% auto;
        }

        @media (max-width: 768px) {
          .login-box,
          .register-box {
            width: 90%;
            margin-top: 20px;
          }
        }

        .login-box-body,
        .register-box-body {
          padding: 20px;
          background: #fff;
          border: 1px solid #dddcdc;
          border-radius: 5px;
          min-height: 100%;
          box-shadow: 0 0 15px 1px rgba(0, 0, 0, 0.07);
        }
        .ft_pass {
          margin-bottom: 5px;
        }
        .dont_class {
          font-size: 14px;
        }
        .dont_class a {
          font-size: 14px;
        }
        .login-box-body .form-control-feedback,
        .register-box-body .form-control-feedback {
          color: #777;
        }

        .login-box-msg,
        .register-box-msg {
          margin: 0;
          text-align: center;
          padding: 0 20px 20px 20px;
        }

        .red_color {
          color: #d2232a;
        }

        .login_head {
          font-size: 32px;
          font-weight: bold;
          margin: 0px 0 20px 0;
          color: #333;
          text-align: center;
        }

        .forgot_pwd {
          margin-bottom: 20px;
          color: #333;
          font-size: 14px;
        }

        .fl-left {
          float: left;
          font-size: 16px;
          margin-bottom: 5px;
          font-weight: 700;
        }
      `}</style>
    </div>
  );
}

LoginForm.propTypes = {
  submitFormHandler: PropTypes.func,
  handleSubmit: PropTypes.func,
  loginToken: PropTypes.string,
};

export default reduxForm({ form: 'login' })(LoginForm);
