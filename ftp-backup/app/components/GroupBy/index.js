import React from 'react';

function GroupBy() {
  return (
    <div className="ftp_second_block">
      <h5 className="about_ftp mb-10">Group By</h5>
      <div className="line"></div>
      <center>
        <img src="/static/images/people.jpg" className="img-responsive" />
      </center>
      <p className="reverse_para">
        MOQ is an acronym which stands for minimum order quantity, and refers to the minimum amount that can be ordered
        from a supplier.
      </p>
      <div className="table-responsive bidding_table">
        <table className="table table-bordered inventory_table bidding_table_bt">
          <thead className="back_primary">
            <tr>
              <th scope="col" className="white border_left border_side_none  fw-600">
                Group By
              </th>
              <th scope="col" className="white border_none"></th>
              <th scope="col" className="white border_right border_sider_none">
                <div className="btn-group">
                  <button
                    type="button"
                    className="btn btn-secondary dropdown-toggle btn-sm dropdown_qty fw-600"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false">
                    User Group
                  </button>
                  <ul className="dropdown-menu dropdown-menu-right drop_block">
                    <li className="dropdown-item drop_list">Retailer</li>
                    <li className="dropdown-item drop_list">Wholesaler</li>
                  </ul>
                </div>
              </th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">Part No</th>
              <td>
                <strong> MOQ </strong>
              </td>
              <td>
                <strong> Join Group </strong>
              </td>
            </tr>
            <tr>
              <td scope="row">PSDJ22376</td>
              <td>1090/5000</td>
              <td>
                <button type="button" className="btn btn-danger bidding_btn">
                  <img src="dist/img/teamwork.png" height="16" width="16" />
                  Join{' '}
                </button>
              </td>
            </tr>
            <tr>
              <td scope="row">KBDI12567</td>
              <td>2320/3000</td>
              <td>
                <button type="button" className="btn btn-danger bidding_btn">
                  <img src="dist/img/teamwork.png" height="16" width="16" />
                  Join{' '}
                </button>
              </td>
            </tr>
            <tr>
              <td scope="row">QEQE2451</td>
              <td>376/400</td>
              <td>
                <button type="button" className="btn btn-danger bidding_btn">
                  <img src="dist/img/teamwork.png" height="16" width="16" />
                  Join{' '}
                </button>
              </td>
            </tr>
            <tr>
              <td scope="row">ZASK1244</td>
              <td>3456/4000</td>
              <td>
                <button type="button" className="btn btn-danger bidding_btn">
                  <img src="dist/img/teamwork.png" height="16" width="16" />
                  Join{' '}
                </button>
              </td>
            </tr>
          </tbody>
        </table>
      </div>

      <style>{`
    .table thead th {
        vertical-align: bottom;
        border-bottom: 2px solid #dee2e6;
    }
    .fw-600 {
        font-weight: 600;
    }
    .border_side_none {
        border-right: none !important;
        border-bottom: none !important;
    }
    .white {
        color: #fff;
    }
    .border_left {
        border-radius: 5px 0 0 0;
        border-left: 1px solid #d31e25;
    }
    .inventory_table {
        border: 1px solid #d1cbcb;
    }
    .bidding_btn {
        background: #d31e25;
        border: #d31e25;
        padding: 3px 10px;
    }
    .btn-danger {
        background-color: #dd4b39;
        border-color: #d73925;
    }
    `}</style>
    </div>
  );
}
export default GroupBy;
