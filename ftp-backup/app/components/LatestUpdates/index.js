import React from 'react';

import ReverseAuction from 'components/ReverseAuction';
import BlockchainProcureMent from 'components/BlockchainProcureMent';
import Category from 'components/Category';
import Inventory from 'components/Inventory';
import HotOffer from 'components/HotOffer';
import GroupBy from 'components/GroupBy';

function LatestUpdates() {
  return (
    <React.Fragment>
      <div className="col-md-4">
        <div className="ftp_first_block">
          <h5 className="about_ftp">Latest Updates</h5>
          <div className="line"></div>
          <ul className="list_man">
            <li>Key Factors for Purchasing Semiconductor Components</li>
            <li>A Framework for Semiconductor Supply Chain Planning</li>
            <li>Strategic sourcing to significant cost reduction</li>
            <li>Plastic Packaging Procurement Research</li>
            <li>Solutions for Chemical Purchasing and Procurement </li>
            <li>A comprehensive for assessing supply performance</li>
            <li>Strategic sourcing to significant cost reduction</li>
          </ul>
        </div>
        <ReverseAuction />
        <BlockchainProcureMent />
        <br />
      </div>
      <div className="col-md-5">
        <Inventory />
        <Category />
      </div>
      <div className="col-md-3">
        <HotOffer />
        <GroupBy />
        <div className="add_inner mb-20 mt-20">
          <img src="/static/images/add12.gif" className="img-responsive" />
        </div>
      </div>

      <style jsx>{`
        .ftp_first_block {
          margin-top: 25px;
        }
        .about_ftp {
          text-align: left;
          font-size: 19px;
          font-weight: 600;
          word-break: break-all;
          margin: 0px;
          padding: 10px 0 5px 10px;
          color: #2b2626;
          border-radius: 5px 5px 0 0;
        }
        .box-icon img {
          height: 50px;
          width: 50px;
          margin: 10px auto;
        }
        .box-title {
          color: #333;
          font-weight: 400;
          font-size: 12px;
          line-height: 0;
          word-break: break-word;
        }
        .mt-20 {
          margin-top: 20px;
        }
        .line {
          height: 2px;
          width: 100px;
          margin-left: 10px;
          margin-bottom: 10px;
          color: #d31e25;
          background-color: #f14141;
        }
        .list_man {
          list-style: none;
          margin-left: -40px;
          margin-bottom: 0px;
        }
        .list_man li {
          color: #423636;
          font-weight: 600;
          line-height: 30px;
          padding: 0px 10px;
          font-size: 14px;
          border-bottom: 1px dashed #e2dfdfab;
          cursor: pointer;
        }
        .category_head {
          padding: 30px 0;
          text-align: center;
          font-weight: 700;
          font-size: 28px;
        }

        .browse_list {
          display: inline-flex;
          width: 110%;
          list-style: none;
          margin-left: -40px;
        }
        .browse_list li {
          margin-right: 10px;
          width: 20%;
        }
        .col-md-5 {
          width: 41.66666667%;
        }
        .white-box {
          display: block;
          background-color: #fafafa;
          border-radius: 3px;
          width: 100px;
          padding: 7px;
          height: 130px;
          text-align: center;
          box-shadow: 0 15px 45px 0 rgba(0, 0, 0, 0.05);
          transition: all 0.3s ease-in-out 0s;
          border: 1px solid #e1e0e0ba;
          transition: transform 0.5s ease;
        }
        .add_inner {
          padding: 15px;
          background: #ebebeb;
          border-radius: 5px;
        }
        .mb-20 {
          margin-bottom: 20px !important;
        }
      `}</style>
    </React.Fragment>
  );
}

export default LatestUpdates;
