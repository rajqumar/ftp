import React from 'react';

function HotOffer() {
  return (
    <div className="hot_offer">
      <h5 className="hot_offer_head">
        <i className="fa fa-tags" aria-hidden="true"></i>
        Hot Offer
      </h5>

      <div className="hot_offer_slider">
        <ul className="hot_offer_ul">
          <li className="hot_offer_list">
            <a href="#">
              <div className="offer_season">
                <div className="row">
                  <div className="col-md-6 ">
                    <span className="date_span">20/12/2019</span>
                  </div>
                  <div className="col-md-6">
                    <span className="blink_offer blink_me">35% off</span>
                  </div>
                  <div className="col-md-3">
                    <img src="/static/images/semi4.jpg" className="img-responsive" />
                  </div>
                  <div className="col-md-8">
                    <span className="desc_semi"> XC4VSX55-11FF1148I having sale </span>
                  </div>
                </div>
              </div>
            </a>
          </li>
          <li className="hot_offer_list">
            <a href="#">
              <div className="offer_season">
                <div className="row">
                  <div className="col-md-6">
                    <span className="date_span">21/07/2019</span>
                  </div>
                  <div className="col-md-6">
                    <span className="blink_offer blink_me">20% off</span>
                  </div>
                  <div className="col-md-3">
                    <img src="/static/images/semi1.jpg" className="img-responsive thumb_semi" />
                  </div>
                  <div className="col-md-8">
                    <span className="desc_semi"> XC4VSX55-11FF1148I having sale </span>
                  </div>
                </div>
              </div>
            </a>
          </li>
          <li className="hot_offer_list">
            <a href="#">
              <div className="offer_season">
                <div className="row">
                  <div className="col-md-6 ">
                    <span className="date_span">11/12/2019</span>
                  </div>
                  <div className="col-md-6 ">
                    <span className="blink_offer blink_me">50% off</span>
                  </div>
                  <div className="col-md-3">
                    <img src="/static/images/semi3.jpg" className="img-responsive thumb_semi" />
                  </div>
                  <div className="col-md-8">
                    <span className="desc_semi"> XC4VSX55-11FF1148I having sale </span>
                  </div>
                </div>
              </div>
            </a>
          </li>
          <li className="hot_offer_list">
            <a href="#">
              <div className="offer_season">
                <div className="row">
                  <div className="col-md-6">
                    <span className="date_span">12/11/2019</span>
                  </div>
                  <div className="col-md-6">
                    <span className="blink_offer blink_me">40% off</span>
                  </div>
                  <div className="col-md-3">
                    <img src="/static/images/semi4.jpg" className="img-responsive thumb_semi" />
                  </div>
                  <div className="col-md-8">
                    <span className="desc_semi"> XC4VSX55-11FF1148I having sale </span>
                  </div>
                </div>
              </div>
            </a>
          </li>
        </ul>
      </div>
      <style>{`
            .hot_offer {
                border: 1px solid #D31E25;
                margin-top: 25px;
                border-radius: 5px;
            }
            .hot_offer_head {
                text-align: center;
                font-size: 18px;
                font-weight: 600;
                background: #D31E25;
                margin: 0px;
                padding: 10px 0;
                color: #fff;
                border-radius: 5px 5px 0 0;
            }
            .hot_offer_slider {
                overflow: hidden;
            }
            .hot_offer_ul{
                list-style: none;
                position: relative;
                margin-left: -40px;
                margin-bottom: 0px;
            }
            .hot_offer_list {
                padding: 5px;
                text-align: center;
                border-bottom: 1px solid #d31e25;
            }
            .offer_season {
                padding: 0px 10px;
                text-align: left;
            }
            .date_span{
                margin-bottom:10px;
                    font-weight:700;
                font-size:14px;
                color:#4b4848;
            }
            .blink_me {
                -webkit-animation-name: blinker;
                -webkit-animation-duration: 1s;
                -webkit-animation-timing-function: linear;
                -webkit-animation-iteration-count: infinite;
            
                -moz-animation-name: blinker;
                -moz-animation-duration: 1s;
                -moz-animation-timing-function: linear;
                -moz-animation-iteration-count: infinite;
            
                animation-name: blinker;
                animation-duration: 1s;
                animation-timing-function: linear;
                animation-iteration-count: infinite;
            }

`}</style>
    </div>
  );
}
export default HotOffer;
