import React from 'react';

function SearchInput() {
  return (
    <div>
      <div className="search_part">
        <div className="container">
          <div className="row">
            <h2 className="seadrch_head">Search parts</h2>
            <div className="col-md-12">
              <form className="sidebar-form">
                <div className="input-group">
                  <input
                    type="text"
                    name="q"
                    className="form-control search-form-control"
                    placeholder="Search by keywords, specifications or part number"
                  />
                  <span className="input-group-btn">
                    <button type="button" name="search" id="search-btn" className="btn btn-flat btn_search_new">
                      <i className="fa fa-search"></i>
                    </button>
                  </span>
                </div>
              </form>
              <span className="search_note">Or try an example search: NE555</span>
            </div>
          </div>
        </div>
      </div>

      <div className="strip_search">
        <div className="container">
          <p>
            HelloFTP is the easiest seacrh engine for electronic parts.Search across hundreds of suppliers and thousands
            of manufactures.
          </p>
        </div>
      </div>

      <style jsx>{`
        .search_part {
          background-color: #d31e25;
          padding: 40px;
        }

        .seadrch_head {
          color: #fff;
          font-weight: 600;
        }

        .sidebar-form,
        .sidebar-menu > li.header {
          overflow: hidden;
          text-overflow: clip;
        }

        .search_note {
          color: #fff;
          font-size: 12px;
        }

        .strip_search {
          background-color: #b0131a;
        }

        .strip_search p {
          color: #fff;
          padding: 15px 0;
          margin: 0px 0px 0px -10px;
        }
      `}</style>
    </div>
  );
}

export default SearchInput;
