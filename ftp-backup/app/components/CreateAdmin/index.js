import React from 'react';
import { faUser } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const CreateAdminForm = () => {
  return (
    <div className="col-md-6 col-md-offset-3 reg_form_new">
      <form id="msform">
        <h5 className="register_main_head">Create Admin</h5>
        <fieldset>
          <h2 className="fs-title">
            <FontAwesomeIcon icon={faUser} width="16" className="fa" />
            Basic Information
          </h2>
          <div className="row">
            <div className="col-md-6">
              <div className="form-group">
                <label htmlFor="name" className="fl-left">
                  Name
                </label>
                <input type="text" className="form-control" id="name" name="name" placeholder="Shrishailya Deshmukh" />
              </div>
              <div className="form-group">
                <label htmlFor="Title" className="fl-left">
                  Title
                </label>
                <input type="text" className="form-control" id="Title" name="Title" placeholder="Director" />
              </div>
              <div className="form-group">
                <label htmlFor="password" className="fl-left">
                  New Password
                </label>
                <input
                  type="password"
                  className="form-control"
                  id="new_password"
                  name="new_password"
                  placeholder="Leave blank if unchanged"
                />
              </div>
              <div className="form-group">
                <label htmlFor="task" className="fl-left">
                  Task
                </label>
                <div className="clearfix"></div>
                <div className="bootstrap-demo fl-left mb-15">
                  <label className="checkbox-inline">
                    <input type="checkbox" id="inlineCheckbox1" value="option1" className="inline_check_issue" /> Buy
                  </label>
                  <label className="checkbox-inline">
                    <input type="checkbox" id="inlineCheckbox2" value="option2" className="inline_check_issue" /> Sell
                  </label>
                </div>
              </div>
              <div className="clearfix"></div>
              <div className="form-group">
                <label htmlFor="Title" className="fl-left">
                  Role
                </label>
                <select className="form-control select2">
                  <option value="default">Select Role</option>
                  <option>IT Manager</option>
                  <option>Procurement Manager</option>
                  <option>Procurement Lead</option>
                  <option>Executive Assistant</option>
                </select>
              </div>
            </div>
            <div className="col-md-6">
              <div className="form-group">
                <label htmlFor="Email" className="fl-left">
                  Email
                </label>
                <input type="email" className="form-control" id="Email" name="Email" placeholder="shri@hellosme.com" />
              </div>
              <div className="form-group">
                <label htmlFor="phone" className="fl-left">
                  Phone
                </label>
                <input type="number" className="form-control" id="phone" name="phone" placeholder="6567891234" />
              </div>
              <div className="form-group">
                <label htmlFor="confirm_password" className="fl-left">
                  Confirm Password
                </label>
                <input
                  type="password"
                  className="form-control"
                  id="confirm_password"
                  name="confirm_password"
                  placeholder="Leave blank if unchanged"
                />
              </div>
              <div className="form-group">
                <label htmlFor="confirm_password" className="fl-left">
                  Status
                </label>
                <div className="clearfix"></div>
                <div className="bootstrap-demo fl-left mb-15">
                  <label className="checkbox-inline">
                    <input type="radio" id="inlineCheckbox1" value="option1" className="inline_radio_issue" /> Active
                  </label>
                  <label className="checkbox-inline">
                    <input type="radio" id="inlineCheckbox2" value="option2" className="inline_radio_issue" />
                    Inactive
                  </label>
                </div>
              </div>
            </div>
          </div>
          <center>
            {/* <Button color="red" value="Save changes" shape="rect" position="left" className="btn-block" /> */}

            <button type="button" className="btn btn-success save_changes">
              Save changes
            </button>
          </center>
        </fieldset>
      </form>

      <style jsx>{`
        .reg_form_new {
          background: #fff;
          border: 1px solid #dddcdc;
          border-radius: 5px;
          min-height: 100%;
          box-shadow: 0 0 15px 1px rgba(0, 0, 0, 0.07);
        }
        #msform fieldset {
          border: 0 none;
          border-radius: 0px;
          padding: 20px 30px;
          transform: unset !important;
          box-sizing: border-box;
          position: relative !important;
        }
        #msform {
          text-align: center;
          position: relative;
        }
        .register_main_head {
          font-size: 32px;
          font-weight: bold;
          margin: 25px auto;
        }
        fieldset {
          min-width: 0;
          padding: 0;
          margin: 0;
          border: 0;
        }
        .fs-title {
          font-size: 21px;
          text-transform: capitalize;
          color: #333;
          margin-bottom: 20px;
          letter-spacing: 1px;
          text-align: left;
          font-weight: bold;
        }
        .fl-left {
          float: left;
          font-size: 16px;
        }
        label {
          display: inline-block;
          max-width: 100%;
          margin-bottom: 5px;
          font-weight: 700;
        }
        #msform input,
        #msform textarea {
          display: block;
          width: 100%;
          height: 34px;
          padding: 6px 12px;
          font-size: 14px;
          line-height: 1.42857143;
          color: #555;
          margin-bottom: 20px;
          background-color: #fff;
          background-image: none;
          border: 1px solid #ccc;
          border-radius: 4px;
          -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
          box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
          -webkit-transition: border-color ease-in-out 0.15s, -webkit-box-shadow ease-in-out 0.15s;
          -o-transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
          transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
        }
        .form-control:not(select) {
          -webkit-appearance: none;
          -moz-appearance: none;
          appearance: none;
        }
        .form-control {
          border-radius: 0;
          box-shadow: none;
          border-color: #d2d6de;
        }
        .checkbox-inline,
        .radio-inline {
          position: relative;
          display: inline-block;
          padding-left: 20px;
          margin-bottom: 0;
          font-weight: 400;
          vertical-align: middle;
          cursor: pointer;
        }
        .select2-container--default .select2-selection--single .select2-selection__rendered {
          color: #444;
          line-height: 28px;
        }
        .form-group {
          margin-bottom: 1rem;
        }
        .form-control {
          display: block;
          width: 100%;
          height: calc(1.5em + 0.75rem + 2px);
          padding: 0.375rem 0.75rem;
          font-size: 1rem;
          font-weight: 400;
          line-height: 1.5;
          color: #495057;
          background-color: #fff;
          background-clip: padding-box;
          border: 1px solid #ced4da;
          border-radius: 0.25rem;
          transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
        }
        .select2-container .select2-selection--single .select2-selection__rendered {
          display: block;
          padding-left: 8px;
          padding-right: 20px;
          overflow: hidden;
          text-overflow: ellipsis;
          white-space: nowrap;
          float: left;
        }
        .save_changes {
          background-color: #d2232a;
          border: 1px solid #d2232a;
          margin-top: 25px;
          text-transform: uppercase;
          font-size: 14px;
        }
        .fa {
          display: inline-block;
          font: normal normal normal 14px/1 FontAwesome;
          font-size: inherit;
          text-rendering: auto;
          -webkit-font-smoothing: antialiased;
          -moz-osx-font-smoothing: grayscale;
        }
        .col-md-offset-3 {
          margin-left: 25%;
        }
        @media (min-width: 992px) .col-md-offset-3 {
          margin-left: 25%;
        }
        @media (min-width: 992px) .col-md-6 {
          width: 50%;
        }
      `}</style>
    </div>
  );
};
export default CreateAdminForm;
