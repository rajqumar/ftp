import React from 'react';

const DashboardForm = () => {
  return (
    <div className="login-box">
      <div className="login-box-body">
        <h4 className="login_head">dashboard</h4>
      </div>

      <style jsx>{`
        .login-box,
        .register-box {
          width: 360px;
          margin: 7% auto;
        }

        @media (max-width: 768px) {
          .login-box,
          .register-box {
            width: 90%;
            margin-top: 20px;
          }
        }

        .login-box-body,
        .register-box-body {
          padding: 20px;
          background: #fff;
          border: 1px solid #dddcdc;
          border-radius: 5px;
          min-height: 100%;
          box-shadow: 0 0 15px 1px rgba(0, 0, 0, 0.07);
        }
        .ft_pass {
          margin-bottom: 5px;
        }
        .dont_class {
          font-size: 14px;
        }
        .dont_class a {
          font-size: 14px;
        }
        .login-box-body .form-control-feedback,
        .register-box-body .form-control-feedback {
          color: #777;
        }

        .login-box-msg,
        .register-box-msg {
          margin: 0;
          text-align: center;
          padding: 0 20px 20px 20px;
        }

        .red_color {
          color: #d2232a;
        }

        .login_head {
          font-size: 32px;
          font-weight: bold;
          margin: 0px 0 20px 0;
          color: #333;
          text-align: center;
        }

        .forgot_pwd {
          margin-bottom: 20px;
          color: #333;
          font-size: 14px;
        }

        .fl-left {
          float: left;
          font-size: 16px;
          margin-bottom: 5px;
          font-weight: 700;
        }
      `}</style>
    </div>
  );
};

export default DashboardForm;
