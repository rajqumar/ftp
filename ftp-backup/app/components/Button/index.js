import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import PropTypes from 'prop-types';

function Button(props) {
  var { value, color, shape, position, icon } = props;

  var r_icon = position === 'left' ? <FontAwesomeIcon icon={icon} width="16" /> : '';
  var l_icon = position === 'right' ? <FontAwesomeIcon icon={icon} width="16" /> : '';

  var color_class = `btn_sign_in btn-block ${color} ${shape}`;

  return (
    <React.Fragment>
      <button type="submit" className={color_class}>
        {r_icon}
        {value}
        {l_icon}
      </button>

      <style jsx>{`
        .btn_sign_in {
          margin-bottom: 20px;
          padding: 10px;
          font-size: 16px;
        }

        .red {
          background-color: rgba(210, 35, 42, 1);
          border: 1px solid rgba(210, 35, 42, 1);
          color: #fff;
        }

        .green {
          background-color: green;
          border-color: rgba(210, 35, 42, 1) !important;
        }

        .silver {
          background-color: grey;
          border-color: rgba(210, 35, 42, 1) !important;
        }

        .rect {
          border-radius: 4px !important;
        }

        .oval {
          border-radius: 24px !important;
        }
      `}</style>
    </React.Fragment>
  );
}

Button.propTypes = {
  value: PropTypes.string,
  color: PropTypes.string,
  shape: PropTypes.string,
  position: PropTypes.string,
  icon: PropTypes.object,
};

export default Button;
