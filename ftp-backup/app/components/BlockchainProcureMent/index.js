import React from 'react';

function BlockchainProcureMent() {
  return (
    <div className="ftp_lst_block">
      <h5 className="about_ftp">How will Blockchain impact Procurem-ent and Supply Chain?</h5>
      <ul className="list_man">
        <p className="reverse_para">
          Blockchain is a very fast emerging technology mainly used in the financial services industry and mostly known
          for Bitcoins or crypto currencies.Blockchain might disrupt entire industry as well as core processes in
          companies, incl.{' '}
          <a href="#" className="read_more">
            Read More
          </a>
        </p>
        <img src="/static/images/blockchain.jpg" className="img-responsive" />
      </ul>

      <style>{`
         .ftp_lst_block {
            margin-top: 10px;
        }
        .about_ftp {
            text-align: left;
            font-size: 19px;
            font-weight: 600;
            word-break: break-all;
            margin: 0px;
            padding: 10px 0 5px 10px;
            color: #2b2626;
            border-radius: 5px 5px 0 0;
        }
        .list_man {
            list-style: none;
            margin-left: -40px;
            margin-bottom: 0px;
        }
        .reverse_para {
            padding-left: 8px;
            text-align: justify;
            font-size: 14px;
            font-weight: 500;
        }
        .img-responsive {
            display: block;
            max-width: 100%;
            height: auto;
        }
         `}</style>
    </div>
  );
}

export default BlockchainProcureMent;
