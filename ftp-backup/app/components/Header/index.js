import React from 'react';
import { Navbar, NavbarBrand } from 'reactstrap';
import HeaderMenu from 'components/HeaderMenu';

function Header() {
  return (
    <header className="main-header">
      <Navbar color="light" light expand="md">
        <div className="container">
          <NavbarBrand href="/" className="mr-auto">
            <img src="/static/images/logo_fav.png" className="logo_register img-responsive" />
          </NavbarBrand>
          <p className="logo_subhead">Powered by <span class="yellow_color">Hello</span><span className="red_logo">SME</span></p>

          <HeaderMenu />
        </div>
      </Navbar>

      <style jsx>{`
        .main-header {
          :relative ;
          max-height: 100px;
          z-index: 1030;
        }
        .logo_subhead {
          color: #333;
          font-size: 14px;
          margin-bottom: 10px;
        }
      `}</style>
    </header>
  );
}

export default Header;
