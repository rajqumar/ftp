import React from 'react';
import { faGavel } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

function ReverseAuction() {
  return (
    <div className="ftp_second_block">
      <h5 className="about_ftp mb-10">Reverse Auction</h5>
      <div className="line"></div>
      <center>
        <img src="/static/images/education.png" />
      </center>
      <p className="reverse_para">
        As a buyer you have opportunity to sell back your left over quantities back to the suppliers.{' '}
      </p>

      <div className="table-responsive bidding_table">
        <table className="table table-bordered inventory_table bidding_table_bt">
          <thead className="back_primary">
            <tr>
              <th scope="col" className="white border_left border_side_none  fw-600">
                Reverse Auction
              </th>
              <th scope="col" className="white border_none"></th>
              <th scope="col" className="white border_none"></th>
              <th scope="col" className="white border_right border_sider_none"></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">Part No</th>
              <td>
                <strong> Qty </strong>
              </td>
              <td>
                <strong> Price(USD) </strong>
              </td>
              <td>
                <strong> Bidding </strong>
              </td>
            </tr>
            <tr>
              <td scope="row">PSDJ22326WFR1</td>
              <td>1090</td>
              <td>1</td>
              <td>
                <button type="button" className="btn btn-danger bidding_btn">
                  <FontAwesomeIcon icon={faGavel} width="12" />
                  Bidding
                </button>
              </td>
            </tr>
            <tr>
              <td scope="row">KBDI12649281</td>
              <td>2320</td>
              <td>0.5</td>
              <td>
                <button type="button" className="btn btn-danger bidding_btn">
                  <FontAwesomeIcon icon={faGavel} width="12" />
                  Bidding
                </button>
              </td>
            </tr>
            <tr>
              <td scope="row">QEQE243124B</td>
              <td>9876</td>
              <td>0.20</td>
              <td>
                <button type="button" className="btn btn-danger bidding_btn">
                  <FontAwesomeIcon icon={faGavel} width="12" />
                  Bidding
                </button>
              </td>
            </tr>
            <tr>
              <td scope="row">ZASK12492147</td>
              <td>3456</td>
              <td>1</td>
              <td>
                <button type="button" className="btn btn-danger bidding_btn">
                  <FontAwesomeIcon icon={faGavel} width="12" />
                  Bidding
                </button>
              </td>
            </tr>
          </tbody>
        </table>
      </div>

      <style>{`
        .ftp_second_block {
            margin-top: 15px;
        }
        .about_ftp {
            text-align: left;
            font-size: 19px;
            font-weight: 600;
            word-break: break-all;
            margin: 0px;
            padding: 10px 0 5px 10px;
            color: #2b2626;
            border-radius: 5px 5px 0 0;
        }
        .mb-10 {
            margin-bottom: 10px;
        }
        .line {
            height: 2px;
            width: 100px;
            margin-left: 10px;
            margin-bottom: 10px;
            color: #d31e25;
            background-color: #f14141;
        }
        center {
            display: block;
            text-align: -webkit-center;
        }
        .reverse_para {
            padding-left: 8px;
            text-align: justify;
            font-size: 14px;
            font-weight: 500;
        }
        .bidding_table {
            padding: 0 10px;
        }
        .table-responsive {
            min-height: .01%;
            overflow-x: auto;
        }
        .bidding_table_bt {
            margin-bottom: 0px;
        }
        .table-bordered {
            border: 1px solid #f4f4f4;
        }
        .table {
            width: 100%;
            max-width: 100%;
            margin-bottom: 20px;
            background-color: transparent;
            border-spacing: 0;
            border-collapse: collapse;
            display: table;
        }
        .back_primary {
            background-color: #D31E25;
        }
        `}</style>
    </div>
  );
}
export default ReverseAuction;
