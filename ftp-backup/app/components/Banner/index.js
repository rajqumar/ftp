import React from 'react';
import PropTypes from 'prop-types';

import styled from '@emotion/styled';

import Logo from '../Logo';

import { withTranslation } from 'utils/with-i18next';

const BannerRoot = styled('div')`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  margin-top: 50px;
`;

const SubTitle = styled('h2')`
  font-size: 20px;
  margin: 0;
  font-weight: 300;
`;

export function Banner({ t }) {
  return (
    <BannerRoot>
      <p>Test:</p>
      <Logo />

      <SubTitle>{t('subTitle')}</SubTitle>
    </BannerRoot>
  );
}

Banner.propTypes = {
  t: PropTypes.func,
};

export default withTranslation('banner')(Banner);
