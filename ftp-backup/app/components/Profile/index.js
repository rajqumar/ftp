import React from 'react';
import { faUser, faBuilding } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const ProfileForm = () => {
  return (
    <div className="col-md-6 col-md-offset-3 reg_form_new">
      <form id="msform">
        <h5 className="register_main_head">My Profile</h5>
        <fieldset>
          <h2 className="fs-title">
            <FontAwesomeIcon icon={faBuilding} width="16" />
            Company Information
          </h2>
          <div className="row">
            <div className="col-md-6">
              <div className="form-group">
                <label htmlFor="name" className="fl-left">
                  Name
                </label>
                <input
                  type="text"
                  className="form-control"
                  id="name"
                  name="name"
                  value=""
                  placeholder="Shrishailya Deshmukh"></input>
              </div>
              <div className="form-group">
                <label htmlFor="phone" className="fl-left">
                  Phone
                </label>
                <input type="number" className="form-control" id="phone" name="phone" value="6567891234" />
              </div>
              <div className="form-group">
                <label htmlFor="Address" className="fl-left">
                  Address
                </label>
                <input
                  type="text"
                  className="form-control"
                  id="Address"
                  name="Address"
                  value="Scared world,Singapore"
                />
              </div>
            </div>
            <div className="col-md-6">
              <div className="form-group">
                <label htmlFor="uen" className="fl-left">
                  UEN
                </label>
                <input type="text" className="form-control" id="name" name="name" value="201898924E" />
              </div>
              <div className="form-group">
                <label htmlFor="Email" className="fl-left">
                  Email
                </label>
                <input type="email" className="form-control" id="Email" name="Email" value="shri@hellosme.com" />
              </div>
              <div className="form-group">
                <label htmlFor="Country" className="fl-left">
                  Country
                </label>
                <input type="text" className="form-control" id="Country" name="Country" value="Singapore" />
              </div>
            </div>
          </div>
          <h2 className="fs-title">
            <FontAwesomeIcon icon={faUser} width="12" />
            Your Information
          </h2>
          <div className="row">
            <div className="col-md-6">
              <div className="form-group">
                <label htmlFor="name" className="fl-left">
                  Name
                </label>
                <input type="text" className="form-control" id="name" name="name" placeholder="Shrishailya Deshmukh" />
              </div>

              <div className="form-group">
                <label htmlFor="Title" className="fl-left">
                  Title
                </label>
                <input type="text" className="form-control" id="Title" name="Title" placeholder="Director" />
              </div>
              <div className="form-group">
                <label htmlFor="confirm_password" className="fl-left">
                  New Password
                </label>
                <input
                  type="password"
                  className="form-control"
                  id="new_password"
                  name="new_password"
                  placeholder="Leave blank if unchanged"
                />
              </div>
            </div>
            <div className="col-md-6">
              <div className="form-group">
                <label htmlFor="Email" className="fl-left">
                  Email
                </label>
                <input type="email" className="form-control" id="Email" name="Email" placeholder="shri@hellosme.com" />
              </div>

              <div className="form-group">
                <label htmlFor="phone" className="fl-left">
                  Phone
                </label>
                <input type="number" className="form-control" id="phone" name="phone" placeholder="65131241241" />
              </div>
              <div className="form-group">
                <label htmlFor="confirm_password" className="fl-left">
                  Confirm Password
                </label>
                <input
                  type="password"
                  className="form-control"
                  id="confirm_password"
                  name="confirm_password"
                  placeholder="Leave blank if unchanged"
                />
              </div>
            </div>
          </div>
          <center>
            <button type="button" className="btn btn-success save_changes">
              Save changes
            </button>
          </center>
        </fieldset>
      </form>
      <style jsx>{`
        .reg_form_new {
          background: #fff;
          border: 1px solid #dddcdc;
          border-radius: 5px;
          min-height: 100%;
          box-shadow: 0 0 15px 1px rgba(0, 0, 0, 0.07);
        }

        .fs-title {
          font-size: 21px;
          text-transform: capitalize;
          color: #333;
          margin-bottom: 20px;
          letter-spacing: 1px;
          text-align: left;
          font-weight: bold;
        }

        .register_main_head {
          font-size: 32px;
          font-weight: bold;
          margin: 25px auto;
        }

        .red_color {
          color: #d2232a;
        }
        .save_changes {
          background-color: #d2232a;
          border: 1px solid #d2232a;
          margin-top: 25px;
          text-transform: uppercase;
          font-size: 14px;
        }
        #msform fieldset {
          border: 0 none;
          border-radius: 0px;
          padding: 20px 30px;
          transform: unset !important;
          box-sizing: border-box;
          position: relative !important;
        }
        #msform {
          text-align: center;
          position: relative;
        }
        fieldset {
          min-width: 0;
          padding: 0;
          margin: 0;
          border: 0;
        }

        .fl-left {
          float: left;
          font-size: 16px;
          margin-bottom: 5px;
          font-weight: 700;
        }
        .col-md-offset-3 {
          margin-left: 25%;
        }
        @media (min-width: 992px) .col-md-6 {
          width: 50%;
        }

        @media (min-width: 992px) .col-md-offset-3 {
          margin-left: 25%;
        }
      `}</style>
    </div>
  );
};

export default ProfileForm;
