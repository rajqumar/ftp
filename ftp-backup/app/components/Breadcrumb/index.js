import React from 'react';
import { withRouter } from 'next/router';
import PropTypes from 'prop-types';

function Breadcrumb({ router }) {
  var path = router.pathname;
  path = path.split('/');

  var links = path.map((p, i) => (
    <React.Fragment key={i}>
      <li>
        <a href="#" className="white text_bred">
          {' '}
          &nbsp;{p}
        </a>
      </li>
      <style jsx>{`
        .white {
          color: #fff !important;
        }
        .text_bred {
          text-transform: capitalize;
        }
      `}</style>
    </React.Fragment>
  ));

  return (
    <div>
      <section className="breadcrumb_top">
        <div className="container">
          <ol className="breadcrumb">
            <li>
              <a href="/">Home /</a>
            </li>
            {links}
          </ol>
        </div>
      </section>

      <style jsx>{`
        .breadcrumb_top {
          background-color: rgba(210, 35, 42, 1);
        }

        .breadcrumb {
          background-color: transparent !important;
          padding: 10px 15px !important;
          margin-bottom: 0px !important;
        }

        .breadcrumb li a {
          color: #fff !important;
        }
        .white {
          color: #fff !important;
        }
      `}</style>
    </div>
  );
}

Breadcrumb.propTypes = {
  router: PropTypes.object,
};

export default withRouter(Breadcrumb);
