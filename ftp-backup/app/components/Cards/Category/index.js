import React from 'react';
import PropTypes from 'prop-types';

function Category(props) {
  const { link, image, text, key } = props;

  return (
    <React.Fragment key={key}>
      <li className="browse_list_child">
        <a href={link} className="box_href">
          <div id="box-1" className="white-box">
            <p className="box-icon">
              <img src={image} className="img-responsive" />
            </p>
            <strong className="box-title">{text}</strong>
          </div>
        </a>
      </li>

      <style jsx>{`
        .white-box {
          display: block;
          background-color: #fafafa;
          margin: 15px auto;
          border-radius: 3px;
          padding: 15px 15px 25px 15px;
          text-align: center;
          box-shadow: 0 15px 45px 0 rgba(0, 0, 0, 0.12);
          transition: all 0.3s ease-in-out 0s;
          border: 1px solid #ccc;
          transition: transform 0.5s ease;
        }

        .box-icon img {
          height: 70px;
          width: 70px;
          margin: 30px auto;
        }

        .box-title {
          color: #333;
          font-weight: 400;
        }
        .box_href:hover {
          text-decoration: none;
        }

        .box-header > .fa,
        .box-header > .glyphicon,
        .box-header > .ion,
        .box-header .box-title {
          display: inline-block;
          font-size: 18px;
          margin: 0;
          line-height: 1;
        }
        .browse_list_child {
          margin-right: 10px;
          width: 19%;
        }
      `}</style>
    </React.Fragment>
  );
}

Category.propTypes = {
  link: PropTypes.string,
  image: PropTypes.string,
  text: PropTypes.string,
  key: PropTypes.integer,
};

export default Category;
