import React from 'react';

export function Logo() {
  return (
    <span>
      <img src="/static/images/logo.png" alt="hellosme logo" />

      <style jsx>{`
        img {
        }
      `}</style>
    </span>
  );
}

export default Logo;
