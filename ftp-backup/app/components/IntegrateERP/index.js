import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBolt } from '@fortawesome/free-solid-svg-icons';

const IntegrateERPForm = () => {
  return (
    <div>
      <h1 className="integrate_erp_head">Integrate Your ERP</h1>
      <div className="clearfix"></div>
      <div className="welcome_box">
        <h4>Welcome onboard, John!</h4>
        <p>To get Started, please link your ERP system to our system so that we can fetch the data over.</p>
      </div>
      <div className="choose_btn_section">
        <h4>1.Choose Your ERP System</h4>
        <ul className="btn_group_erp">
          <li>
            <a href="#">
              <button type="button" className="btn btn-default btn_list_erp">
                SAP HANA
              </button>
            </a>
          </li>
          <li>
            <a href="#">
              <button type="button" className="btn btn-default btn_list_erp active_erp">
                ORACLE ERP CLOUD
              </button>
            </a>
          </li>
          <li>
            <a href="#">
              <button type="button" className="btn btn-default btn_list_erp">
                MICROSOFT DYNAMICS
              </button>
            </a>
          </li>
          <li>
            <a href="#">
              <button type="button" className="btn btn-default btn_list_erp">
                ERP 4
              </button>
            </a>
          </li>
          <li>
            <a href="#">
              <button type="button" className="btn btn-default btn_list_erp">
                ERP 5
              </button>
            </a>
          </li>
        </ul>
        <h4>2.ERP System Details</h4>
        <div className="row">
          <div className="col-md-6">
            <div className="box box-widget">
              <div className="box-header with-border">
                <h5 className="head_test">
                  <FontAwesomeIcon icon={faBolt} width="16" />
                  &nbsp;&nbsp;&nbsp; Testing
                </h5>
              </div>
              <div className="box-body">
                <form id="msform">
                  <fieldset className="padding_form">
                    <div className="form-group">
                      <label htmlFor="name" className="fl-left">
                        API Token
                      </label>
                      <input type="text" className="form-control" id="name" name="name" placeholder="HIVE9UW739FKS" />
                    </div>
                    <div className="form-group">
                      <label htmlFor="name" className="fl-left">
                        Password
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        id="name"
                        name="name"
                        placeholder="Type password here"
                      />
                    </div>
                    <div className="form-group">
                      <label htmlFor="name" className="fl-left">
                        URL
                      </label>
                      <input type="text" className="form-control" id="name" name="name" placeholder="Type URL Here" />
                    </div>
                  </fieldset>
                </form>
              </div>
            </div>
          </div>
          <div className="col-md-6">
            <div className="box box-widget">
              <div className="box-header with-border">
                <h5 className="head_test">
                  {' '}
                  <FontAwesomeIcon icon={faBolt} width="16" />
                  &nbsp;&nbsp;&nbsp; Production
                </h5>
              </div>
              <div className="box-body">
                <form id="msform">
                  <fieldset className="padding_form">
                    <div className="form-group">
                      <label htmlFor="name" className="fl-left">
                        API Token
                      </label>
                      <input type="text" className="form-control" id="name" name="name" placeholder="HIVE9UW739FKS" />
                    </div>
                    <div className="form-group">
                      <label htmlFor="name" className="fl-left">
                        Password
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        id="name"
                        name="name"
                        placeholder="Type password here"
                      />
                    </div>
                    <div className="form-group">
                      <label htmlFor="name" className="fl-left">
                        URL
                      </label>
                      <input type="text" className="form-control" id="name" name="name" placeholder="Type URL here" />
                    </div>
                  </fieldset>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <style>{`
        .integrate_erp_head {
            float: left;
            font-size: 32px;
            color: #000;
            margin-bottom: 30px;
            font-weight: 700;
        }
        .btn-default {
            background-color: #f4f4f4;
            color: #444;
            border-color: #ddd;
        }
        .welcome_box {
            background-color: #fff;
            padding: 15px 35px;
            border-left: 5px solid #FFC000;
        }
        .choose_btn_section h4
        {
            margin:20px 0;
            color:#333;
            font-weight:700;
            font-size:20px;
        }
        
        .btn_group_erp {
            list-style: none;
            margin-left: -40px;
            margin-bottom: 20px;
        }
        .active_erp {
            background-color: #d2232a;
            border: 1px solid #d2232a;
            color: #fff;
        }
        .btn_list_erp {
            width: 170px;
            background-color: #fff;
            font-weight: 700;
            padding: 10px;
        }
        .btn_group_erp li {
            display: inline-block;
            margin-right: 40px;
        }
        .btn-default:hover, .btn-default:active, .btn-default.hover {
            background-color: #e7e7e7;
        }
        .btn-default:hover {
            color: #333;
            background-color: #e6e6e6;
            border-color: #adadad;
        }
        @media (min-width: 992px)
        .col-md-6 {
            width: 50%;
        }
        .box-widget {
            border: 1px solid #e1dede;
            position: relative;
        }
        .box {
            position: relative;
            border-radius: 3px;
            background: #ffffff;
            border-top: 3px solid #d2d6de;
            margin-bottom: 20px;
            width: 100%;
            box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
        }
        .box-header {
            color: #444;
            display: block;
            padding: 10px;
            position: relative;
        }
        .head_test {
            font-size: 16px;
            font-weight: 700;
        }
        .box-body {
            border-top-left-radius: 0;
            border-top-right-radius: 0;
            border-bottom-right-radius: 3px;
            border-bottom-left-radius: 3px;
            padding: 10px;
        }
        #msform {
            text-align: center;
            position: relative;
        }
        .padding_form {
            padding: 0px 10px !important;
        }
        fieldset {
            min-width: 0;
            padding: 0;
            margin: 0;
            border: 0;
        }
        #msform fieldset {
            border: 0 none;
            border-radius: 0px;
            padding: 20px 30px;
            transform: unset !important;
            box-sizing: border-box;
            position: relative !important;
        }
        .padding_form {
            padding: 0px 10px !important;
        }
        .form-group {
            margin-bottom: 15px;
        }
        .fl-left {
            float: left;
            font-size: 16px;
        }
        label {
            display: inline-block;
            max-width: 100%;
            margin-bottom: 5px;
            font-weight: 700;
        }
        #msform input, #msform textarea {
            display: block;
            width: 100%;
            height: 34px;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.42857143;
            color: #555;
            margin-bottom: 20px;
            background-color: #fff;
            background-image: none;
            border: 1px solid #ccc;
            border-radius: 4px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
            box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
            -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
            -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        }
        .form-control:not(select) {
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
        }
       
        `}</style>
    </div>
  );
};
export default IntegrateERPForm;
