import React from 'react';
import Document, { Html, Head, Main, NextScript } from 'next/document';
import { Global, css } from '@emotion/core';

class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx);
    return { ...initialProps };
  }

  render() {
    return (
      <Html>
        <Global
          styles={css`
            body {
              font-weight: 400;
              overflow-x: hidden;
              overflow-y: auto;
            }

            body,
            html,
            container {
              font-family: 'Metropolis' !important;
            }
          `}
        />
        <Head />
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default MyDocument;
