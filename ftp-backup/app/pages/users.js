import React from 'react';
import Users from 'containers/Users';

class UsersPage extends React.PureComponent {
  render() {
    return <Users />;
  }
}

export default UsersPage;
