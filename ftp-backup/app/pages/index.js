import React from 'react'
import Home from 'containers/Home'
import HomeLoggedOut from 'containers/HomeLoggedOut'
import HomeOption4 from 'containers/HomeOption4'

class IndexPage extends React.PureComponent {
  componentDidMount() {
    localStorage.setItem('ftp_user', false);
  }

  render() {

    var currentHomeVersion = 'logged_out'

    if (currentHomeVersion === 'logged_in') {
      return <Home />
    } else if (currentHomeVersion === 'logged_out') {
      return <HomeLoggedOut />
    } else if (currentHomeVersion === 'option4') {
      return (<HomeOption4 />)
    }
  }
}

export default IndexPage;
