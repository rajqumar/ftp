const categories = [
  {
    text: 'Integrated Circuits',
    link: '/',
    image: '/static/images/integrated_circuits.png',
  },
  {
    text: 'Discrete Semiconductors',
    link: '/',
    image: '/static/images/discrete_semiconducors.png',
  },
  {
    text: 'Passive Components',
    link: '/',
    image: '/static/images/passive_conductors.png',
  },
  {
    text: 'Electromechanical',
    link: '/',
    image: '/static/images/electromechanical.png',
  },
  {
    text: 'Connectors',
    link: '/',
    image: '/static/images/connectors.png',
  },
  {
    text: 'Sensors',
    link: '/',
    image: '/static/images/sensors.png',
  },
  {
    text: 'Optoelectronics',
    link: '/',
    image: '/static/images/optoelectronics.png',
  },
  {
    text: 'Circuit Protection',
    link: '/',
    image: '/static/images/circuit_protection.png',
  },
  {
    text: 'Power Products',
    link: '/',
    image: '/static/images/power_products.png',
  },
  {
    text: 'Cables & Wire',
    link: '/',
    image: '/static/images/cables_wires.png',
  },
];

export default categories;
