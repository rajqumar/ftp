import React from 'react';
import Layout from 'containers/Layout';
import ProfileForm from 'components/Profile';

function Profile() {
  return (
    <Layout>
      <ProfileForm />
    </Layout>
  );
}

export default Profile;
