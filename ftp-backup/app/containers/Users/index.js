import React from 'react';
import Layout from 'containers/Layout';
import UsersList from 'components/Users';

function Users() {
  return (
    <Layout>
      <UsersList />
    </Layout>
  );
}

export default Users;
