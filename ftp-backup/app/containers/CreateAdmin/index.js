import React from 'react';
import Layout from 'containers/Layout';
import CreateAdminForm from 'components/CreateAdmin';

function CreateAdmin() {
  return (
    <Layout>
      <CreateAdminForm />
    </Layout>
  );
}

export default CreateAdmin;
