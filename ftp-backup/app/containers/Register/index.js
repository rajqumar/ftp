import React from 'react';
import Layout from 'containers/Layout';
import RegisterForm from 'components/RegisterForm';
// import fetch from 'isomorphic-unfetch';

export function Register() {
  return (
    <Layout>
      <div className="container">
        <section className="content">
          <div className="row">
            <div className="  reg_form_new">
              <RegisterForm />
            </div>
          </div>
        </section>
      </div>

      <style jsx>{`
        .reg_form_new {
          background: #fff;
          border: 1px solid #dddcdc;
          border-radius: 5px;
          margin: 30px auto;
          padding: 20px;
          min-height: 100%;
          box-shadow: 0 0 15px 1px rgba(0, 0, 0, 0.07);
        }
      `}</style>
    </Layout>
  );
}

// Register.getInitialProps = async function() {
//   const res = await fetch('https://api.tvmaze.com/search/shows?q=batman');
//   const data = await res.json();

//   console.log(`Show data fetched. Count: ${data.length}`);

//   return {
//     shows: data.map(entry => entry.show),
//   };
// };

export default Register;
