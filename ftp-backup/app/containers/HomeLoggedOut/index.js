import React from 'react';
import Layout from 'containers/Layout';
import SearchInput from 'components/SearchInput';
import LatestUpdates from 'components/LatestUpdates';
export function HomeLoggedOut() {
  return (
    <Layout>
      <SearchInput />

      <div className="browse_by_category">
        <div className="container">
          <div className="row">
            <LatestUpdates />
          </div>
        </div>
      </div>

      <style jsx>{`
        .category_head {
          padding: 30px 0;
          text-align: center;
          font-weight: 700;
          font-size: 28px;
        }

        .browse_list {
          display: flex;
          flex-wrap: wrap;
          list-style: none;
        }

        .browse_list li {
          margin-right: 10px;
          width: 20%;
        }
      `}</style>
    </Layout>
  );
}

export default HomeLoggedOut;
