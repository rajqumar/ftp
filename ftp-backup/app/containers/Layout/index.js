import React from 'react';
import Header from 'components/Header';
import Footer from 'components/Footer';
import PropTypes from 'prop-types';
import Breadcrumb from 'components/Breadcrumb';

// Header
// Subheader
// BreadCrumb
// footer

function Layout({ children }) {
  const isLoggedIn = false;

  return (
    <>
      <div className="wrapper">
        <Header />
        {/* <SubHeader /> */}

        {isLoggedIn ? <Breadcrumb /> : null}

        <main>{children}</main>

        <Footer />

        <style jsx>{`
          .wrapper {
            height: 100%;
            position: relative;
            overflow-x: hidden;
            overflow-y: auto;
          }
        `}</style>
      </div>
    </>
  );
}

Layout.propTypes = {
  children: PropTypes.node,
};

export default Layout;
