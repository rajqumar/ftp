import React, { memo } from 'react';

import { compose } from 'redux';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { useInjectReducer } from 'utils/inject-reducer';
import { useInjectSaga } from 'utils/inject-saga';

import saga from './saga';
import reducer from './reducer';
import { submitFormHandler } from './actions';

import { loginToken } from './selectors';

import Layout from 'containers/Layout';
import LoginForm from 'components/LoginForm';

function Login() {
  useInjectSaga({ key: 'login_token', saga });
  useInjectReducer({ key: 'login_token', reducer });

  return (
    <Layout>
      <LoginForm submitFormHandler={submitFormHandler} loginToken={loginToken} />
    </Layout>
  );
}

const mapStateToProps = createStructuredSelector({
  loginToken: loginToken(),
});

const withConnect = connect(mapStateToProps, null);
compose(withConnect, memo);
export default Login;
