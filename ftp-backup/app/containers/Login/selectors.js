import { createSelector } from 'reselect';

import { initialState } from './reducer';

const selectLoginToken = state => state.login_token || initialState;

const loginToken = () => createSelector(selectLoginToken, subState => subState);

export { loginToken, selectLoginToken };
