//IntegrateERP

import React from 'react';
import Layout from 'containers/Layout';
import IntegrateERPForm from 'components/IntegrateERP';

function IntegrateERP() {
  return (
    <Layout>
      <IntegrateERPForm />
    </Layout>
  );
}

export default IntegrateERP;
