import React from 'react';
import Layout from 'containers/Layout';
import DashboardForm from 'components/Dashboard';

function Dashboard() {
  return (
    <Layout>
      <DashboardForm />
    </Layout>
  );
}

export default Dashboard;
