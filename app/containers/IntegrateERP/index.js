//IntegrateERP

import React from 'react';
import Layout from 'containers/Layout';
import IntegrateERPForm from 'components/IntegrateERP';
import Router from 'next/router';

function IntegrateERP() {
  if (window.localStorage.getItem('ftp_token') == null) {
    Router.push('/login');
  }
  return (
    <Layout>
      <IntegrateERPForm />
    </Layout>
  );
}

export default IntegrateERP;
