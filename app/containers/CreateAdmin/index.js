import React from 'react';
import Layout from 'containers/Layout';
import CreateAdminForm from 'components/CreateAdmin';
import PropTypes from 'prop-types';
import Router from 'next/router';

function CreateAdmin(props) {
  const { title } = props;
  if (window.localStorage.getItem('ftp_token') == null) {
    Router.push('/login');
  }
  return (
    <Layout>
      <CreateAdminForm title={title} />
    </Layout>
  );
}
CreateAdmin.propTypes = {
  title: PropTypes.string,
};
export default CreateAdmin;
