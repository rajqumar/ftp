import { createSelector } from 'reselect';

import { initialState } from './reducer';

const selectLoginToken = state =>
  state.token && state.token.token ? state.token.token : window.localStorage.getItem('ftp_token') || initialState.token;

const loginToken = () => createSelector(selectLoginToken, subState => subState);

export { loginToken, selectLoginToken };
