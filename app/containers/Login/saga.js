import { takeLatest, put, call } from 'redux-saga/effects';

import { loginUser } from './actions';
import axios from 'axios';
import Router from 'next/router';

export function* loginRequest(action) {
  const { values } = action.payload;
  try {
    yield put(loginUser.request());

    var url = 'http://ftp-backend-git-ftp-backends.apps.us-east-1.starter.openshift-online.com/api/login/';

    const response = yield call(() => axios.post(url, values));
    if (localStorage.getItem('rememberme') != null && localStorage.getItem('rememberme') == 'true') {
      localStorage.setItem('userEmail', values.email);
    } else {
      localStorage.removeItem('userEmail');
    }
    localStorage.setItem('ftp_token', response.data.success.data.token);
    yield put(loginUser.success(response.data.success.data.token));
    Router.push('/Dashboard/manager');
  } catch (err) {
    console.log(err.response.data.error.message, 'err.response.data.error.message');
    yield put(loginUser.failure(err.response.data.error.message.error));
  } finally {
    yield put(loginUser.fulfill());
  }
}

export default function* dataShowcases() {
  yield takeLatest(loginUser.TRIGGER, loginRequest);
}
