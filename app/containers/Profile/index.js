import React from 'react';
import Layout from 'containers/Layout';
import ProfileForm from 'components/Profile';
import Router from 'next/router';

function Profile() {
  if (window.localStorage.getItem('ftp_token') == null) {
    Router.push('/login');
  }
  return (
    <Layout>
      <ProfileForm />
    </Layout>
  );
}

export default Profile;
