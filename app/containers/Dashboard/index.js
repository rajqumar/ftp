import React, { memo } from 'react';
import Layout from 'containers/Layout';
import DashboardForm from 'components/Dashboard';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCog, faMobile, faEnvelope } from '@fortawesome/free-solid-svg-icons';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { loginToken } from '../Login/selectors';
import Router from 'next/router';
import PropTypes from 'prop-types';

const data = [
  {
    purchaseNumber: 6,
    purchaseRequest: 'Purchase ',
    subtitle: 'Requests',
    id: 1,
  },
  {
    purchaseNumber: 14,
    purchaseRequest: 'RFQ ',
    subtitle: 'Received',
    id: 2,
  },
  {
    purchaseNumber: 6,
    purchaseRequest: 'Quotes',
    subtitle: 'Accepted',
    id: 3,
  },
  {
    purchaseNumber: 6,
    purchaseRequest: 'Purchase ',
    subtitle: 'Orders',
    id: 4,
  },
  {
    purchaseNumber: 6,
    purchaseRequest: 'Delivery ',
    subtitle: 'Orders',
    id: 5,
  },
  {
    purchaseNumber: 6,
    purchaseRequest: 'Invoices',
    subtitle: 'Outstanding',

    id: 6,
  },
  {
    purchaseNumber: 6,
    purchaseRequest: 'Pending  ',
    subtitle: 'Payments',
    id: 7,
  },
  {
    purchaseNumber: 6,
    purchaseRequest: 'Pending',
    subtitle: 'Acknnowledgements',
    id: 8,
  },
];
const rating = [
  {
    id: 1,
    title: 'Payment',
    rate: 2,
  },
  {
    id: 2,
    title: 'Responsive',
    rate: 3,
  },
  {
    id: 3,
    title: 'Shipment',
    rate: 4,
  },
];
const YearToDateDummy = [
  {
    title: 'Quotations Sent',
    id: 1,
    number: 200,
  },
  {
    title: 'RFQ Received',
    id: 2,
    number: 150,
  },
  {
    title: 'Shipments Completed',
    id: 3,
    number: 45,
  },
];
const PlatformUtilisation = [
  {
    title: 'Transcations',
    id: 1,
    number: '3,400',
  },
];
const datasheet = [
  {
    label: 'somethingA',

    values: [
      { x: 'Jan', y: 57 },
      { x: 'Feb', y: 62 },
      { x: 'Mar', y: 9 },
      { x: 'Apr', y: 3 },
      { x: 'May', y: 66 },
      { x: 'Jun', y: 10 },
      { x: 'Jul', y: 28 },
      { x: 'Aug', y: 56 },
      { x: 'Sep', y: 83 },
      { x: 'Oct', y: 11 },
      { x: 'Nov', y: 75 },
      { x: 'Dec', y: 93 },
    ],
  },
];
var action = (
  <div className="dropdown">
    <button className="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">
      <FontAwesomeIcon icon={faCog} />
    </button>
    <ul className="dropdown-menu">
      <li>
        <a href="#">Settings1</a>
      </li>
      <li>
        <a href="#">Settings2</a>
      </li>
    </ul>
  </div>
);

const columns = [
  {
    name: 'User',
  },

  {
    name: 'Role',
  },
  {
    name: 'LastLogin',
  },
  {
    name: 'Status',
  },
  {
    name: 'Actions',
  },
];
var activeBtn = (
  <button type="button" className="btn btn-success btn_active">
    Active
  </button>
);
var inactiveBtn = (
  <button type="button" className="btn btn-success btn_inactive">
    Inactive
  </button>
);

var nameTitle = (
  <div>
    <p className="red_name">Shrishailya Deshmukh</p>
    <p className="mb-0">
      <a href="#">
        <FontAwesomeIcon icon={faMobile} width="12" className="mob_icon" />
      </a>
      <a href="#">
        <FontAwesomeIcon icon={faEnvelope} width="12" className="mail_icon" />
      </a>
    </p>
  </div>
);
const userLoginData = [
  [nameTitle, 'User', '1 Jan 2019, 1.40 PM', activeBtn, action],
  [nameTitle, 'User', '2 Jan 2019, 1.40 PM', activeBtn, action],
  [nameTitle, 'User', '3 Jan 2019, 1.40 PM', inactiveBtn, action],
];
const options = {
  filterType: 'dropdown',
  pagination: true,
  search: false,
  filter: true,
  download: false,
  print: false,
  viewColumns: false,
  toolbar: false,
  filterTable: false,
  searchOpen: false,
};

function Dashboard() {
  // const isLoggedIn = loginToken.loginToken.token;
  // if (isLoggedIn.length == 0) {
  //   console.log('hello dashboard');
  //   Router.push('/login');
  // }
  if (window.localStorage.getItem('ftp_token') == null) {
    Router.push('/login');
  }
  return (
    <Layout>
      <DashboardForm
        datasheet={datasheet}
        PlatformUtilisation={PlatformUtilisation}
        YearToDateDummy={YearToDateDummy}
        rating={rating}
        userLoginData={userLoginData}
        columns={columns}
        options={options}
        data={data}
      />
    </Layout>
  );
}

Dashboard.PropTypes = {
  loginToken: PropTypes.func,
};
const mapStateToProps = createStructuredSelector({
  loginToken: loginToken(),
});
const withConnect = connect(mapStateToProps, null);
export default compose(withConnect, memo)(Dashboard);
