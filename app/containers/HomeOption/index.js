import React from 'react';
import Layout from 'containers/Layout';
import SearchInput from 'components/SearchInput';
import FTPValueAddedServices from 'components/FTPValueAddedServices';
import NewClearanceSell from 'components/NewClearanceSell';
import NewBrowsePartsByCategory from 'components/NewBrowsePartsByCategory';
import NewBlockchainProcureMent from 'components/NewBlockchainProcureMent';
import NewHotDeals from 'components/NewHotDeals';
import ReverseAuction from 'components/ReverseAuction';
import GroupBy from 'components/GroupBy';

export function HomeOption() {
  return (
    <Layout>
      <SearchInput title={'Fast Transaction'} />
      <NewBrowsePartsByCategory />
      <div className="browse_by_category">
        <div className="container">
          <div className="row order_contain">
            <div className="col-md-9 one">
              <FTPValueAddedServices />
              <NewBlockchainProcureMent />
            </div>
            <div className="col-md-3 two">
              <NewHotDeals />
              <NewClearanceSell />
              <br />
              <GroupBy />
              <ReverseAuction />
            </div>
          </div>
          <div className="block_img">
            <div className="row">
              <div className="col-md-4">
                <div className="img_div mb-20 mt-30">
                  <img src="/static/images/img1.jpg" className="img-responsive" />
                </div>
              </div>
              <div className="col-md-4">
                <div className="img_div mb-20 mt-30">
                  <img src="/static/images/img2.jpg" className="img-responsive" />
                </div>
              </div>
              <div className="col-md-4">
                <div className="img_div mb-20 mt-30">
                  <img src="/static/images/img3.jpg" className="img-responsive" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* <NewClearanceSell/> */}
      <style>{`
      //block image
      .col-md-4 {
        width: 33.33333333%;
    }
    .img-responsive {
      display: block;
      max-width: 100%;
      height: auto;
    }
    .mt-30 {
      margin-top: 30px;
     }
    .mb-20 {
        margin-bottom: 20px !important;
    }
      .mt-5
      {
        margin-top:5px;
      }
      .pa-0
    {
      padding-left:0px !important;
      padding-right:0px !important;
    }
    .part_supp
    {
          display: block;
        background-color: #FAFAFA;
        border-radius: 3px;
        width: 100px;
        height: 130px;
        text-align: left;
        box-shadow: 0 15px 45px 0 rgba(0, 0, 0, 0.05);
        transition: all 0.3s ease-in-out 0s;
        border: 1px solid #e1e0e0ba;
        transition: transform .5s ease;
    }
   
  .new_part {
    color: #333;
    font-size: 13px;
    background: #fff !important;
    border: 1px solid #fff !important;
    color: #333;
    padding: 0px;
    /* float: left; */
    padding-left: 5px;
    text-align: left;
    margin-bottom: 10px !important;
    cursor: pointer;
    margin-top: 10px !important;
    box-shadow: unset !important;
}
      `}</style>
    </Layout>
  );
}
export default HomeOption;
