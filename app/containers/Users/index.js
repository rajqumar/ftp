import React from 'react';
import Layout from 'containers/Layout';
import UsersList from 'components/Users';
import { faMobile, faEnvelope } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Router from 'next/router';

const columns = [
  {
    name: 'User',
  },
  {
    name: 'Type',
  },
  {
    name: 'Role',
  },
  {
    name: 'LastLogin',
  },
  {
    name: 'Status',
  },
];
var activeBtn = (
  <button type="button" className="btn btn-success btn_active">
    Active
  </button>
);
var inactiveBtn = (
  <button type="button" className="btn btn-success btn_inactive">
    Inactive
  </button>
);

var nameTitle = (
  <div>
    <p className="red_name">Shrishailya Deshmukh</p>
    <p className="mb-0">
      <a href="#">
        <FontAwesomeIcon icon={faMobile} width="12" className="mob_icon" />
      </a>
      <a href="#">
        <FontAwesomeIcon icon={faEnvelope} width="12" />
      </a>
    </p>
  </div>
);

const data = [
  [nameTitle, 'User', 'Procurement Manager', '1 Jan 2019, 1.40 PM', activeBtn],
  ['Rajkumar Tiwari', 'User', 'Procurement Manager', '2 Jan 2019, 1.40 PM', activeBtn],
  ['Pranshu Rastogi', 'User', 'Procurement Manager', '3 Jan 2019, 1.40 PM', inactiveBtn],
];
const options = {
  filterType: 'dropdown',
  pagination: true,
  search: true,
  filter: true,
  download: false,
  print: false,
  viewColumns: false,
  toolbar: false,
  filterTable: false,
  searchOpen: true,
};
function Users() {
  if (window.localStorage.getItem('ftp_token') == null) {
    Router.push('/login');
  }
  return (
    <Layout>
      <UsersList userLoginData={data} columns={columns} options={options} />
    </Layout>
  );
}

export default Users;
