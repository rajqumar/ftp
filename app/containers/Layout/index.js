import React, { memo, useState } from 'react';
import Header from 'components/Header';
import Footer from 'components/Footer';
import PropTypes from 'prop-types';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { loginToken } from '../Login/selectors';
import { useRouter } from 'next/router';

function Layout({ children, loginToken }) {
  const router = useRouter();

  const [isLoggedIn] = useState((loginToken && loginToken.length > 0) ? true : false);

  if (!isLoggedIn && router.pathname !== '/login' && router.pathname !== '/register' && router.pathname !== '/') {
    router.push('/');
  } else {
    return (
      <div className="wrapper">
        <Header isLoggedIn={isLoggedIn} />
        <main>{children}</main>

        <Footer />

        <style jsx>{`
          .wrapper {
            height: 100%;
            position: relative;
            overflow-x: hidden;
            overflow-y: auto;
          }
        `}</style>
      </div>
    );
  }
}

Layout.propTypes = {
  children: PropTypes.node,
  loginToken: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  loginToken: loginToken(),
});

const withConnect = connect(mapStateToProps, null);

export default compose(withConnect, memo)(Layout);
