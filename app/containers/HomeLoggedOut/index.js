import React from 'react';
import Layout from 'containers/Layout';
import SearchInput from 'components/SearchInput';
import FTPValueAddedServices from 'components/FTPValueAddedServices';
import NewClearanceSell from 'components/NewClearanceSell';
import BrowsePartsByCategory from 'components/BrowsePartsByCategory';
import NewBlockchainProcureMent from 'components/NewBlockchainProcureMent';
import NewHotDeals from 'components/NewHotDeals';
import ReverseAuction from 'components/ReverseAuction';
import GroupBy from 'components/GroupBy';
import Link from 'next/link';

export function HomeLoggedOut() {
  return (
    <Layout>
      <SearchInput title={'Fast Transaction'} />
      <BrowsePartsByCategory />
      <div className="browse_by_category">
        <div className="container">
          <div className="row order_contain">
            <div className="col-md-9 one">
              <FTPValueAddedServices />
              <NewBlockchainProcureMent />
            </div>
            <div className="col-md-3 two">
              <NewHotDeals />
              <NewClearanceSell />
              <br />
              <GroupBy />
              <ReverseAuction />
            </div>
          </div>
          <div className="block_img mt-50">
            <div className="row">
              <div className="col-md-4">
                <div className="bottom_div_ad">
                  <p className="pt-20">
                    <img src="/static/images/profit.png" height="40" width="40" />
                  </p>
                  <a
                    className="a-tag"
                    href="https://www.xero.com/blog/2020/02/business-continuity-planning-and-reinforcing-stability-during-uncertain-times/">
                    <h5 className="bottom_ad_head">Business continuity in uncertain times</h5>
                  </a>

                  <Link href="https://www.xero.com/blog/2020/02/business-continuity-planning-and-reinforcing-stability-during-uncertain-times/">
                    <a className="a-tag" target="_blank">
                      <p className="pb-20">Read the blog</p>
                    </a>
                  </Link>
                </div>
              </div>

              <div className="col-md-4">
                <div className="bottom_div_ad1">
                  <p className="pt-20">
                    <img src="/static/images/cloud.png" height="40" width="40" />
                  </p>
                  <a
                    className="a-tag"
                    href="https://www.xero.com/sg/resources/small-business-guides/business-management/mobile-office/">
                    {' '}
                    <h5 className="bottom_ad_head">Running a business without an office</h5>
                  </a>

                  <Link href="https://www.xero.com/sg/resources/small-business-guides/business-management/mobile-office/">
                    <a className="a-tag" target="_blank">
                      <p className="pb-20">Read the blog</p>
                    </a>
                  </Link>
                </div>
              </div>
              <div className="col-md-4">
                <div className="bottom_div_ad2">
                  <p className="pt-20">
                    <img src="/static/images/phone (1).png" height="40" width="40" />
                  </p>
                  <a
                    className="a-tag"
                    href="https://www.xero.com/sg/resources/small-business-guides/cloud-accounting/mobile-accounting-app/">
                    {' '}
                    <h5 className="bottom_ad_head">Do I need a mobile accounting app?</h5>
                  </a>
                  <Link href="https://www.xero.com/sg/resources/small-business-guides/cloud-accounting/mobile-accounting-app/">
                    <a className="a-tag" target="_blank">
                      <p className="pb-20">Read the blog</p>
                    </a>
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* <NewClearanceSell/> */}
      <style>{`
      //block image
      .col-md-4 {
        width: 33.33333333%;
    }
   
    .img-responsive {
      display: block;
      max-width: 100%;
      height: auto;
    }
    .mt-30 {
      margin-top: 30px;
     }
    .mb-20 {
        margin-bottom: 20px !important;
    }
      .mt-5
      {
        margin-top:5px;
      }
      .mt-50 {
        margin-top: 50px;
    }
      .pa-0
    {
      padding-left:0px !important;
      padding-right:0px !important;
    }
    .part_supp
    {
          display: block;
        background-color: #FAFAFA;
        border-radius: 3px;
        width: 100px;
        height: 130px;
        text-align: left;
        box-shadow: 0 15px 45px 0 rgba(0, 0, 0, 0.05);
        transition: all 0.3s ease-in-out 0s;
        border: 1px solid #e1e0e0ba;
        transition: transform .5s ease;
    }
    .new_part {
      color: #333;
      font-size: 13px;
      background: #fff !important;
      border: 1px solid #fff !important;
      color: #333;
      padding: 0px;
      /* float: left; */
      padding-left: 5px;
      text-align: left;
      margin-bottom: 10px !important;
      cursor: pointer;
      margin-top: 10px !important;
      box-shadow: unset !important;
  }
    .bottom_div_ad {
      line-height: 300px;
      width: 100%;
      background-repeat: no-repeat;
      background-size: 100% 100%;
      background-image: linear-gradient(rgba(0, 0, 0, 0.35), rgba(0, 0, 0, 0.35)),
       url(/static/images/add1.png);
      text-align: center;
  }
  .bottom_div_ad p {
    font-size: 16px;
    color: #CED8E1;
    position: relative;
    vertical-align: middle;
    line-height: normal;
}
.pt-20 {
    padding-bottom: 20px;
    padding-top: 20px;
}
.pb-20 {
  padding-bottom: 20px;

}
.bottom_div_ad1 {
  line-height: normal;
  width: 100%;
  background-repeat: no-repeat;
  background-size: 100% 100%;
  background-image: linear-gradient(rgba(0, 0, 0, 0.35), rgba(0, 0, 0, 0.35)),
   url(/static/images/add2.png);
  text-align: center;
}
.bottom_div_ad1 p {
  font-size: 16px;
  color: #CED8E1;
  position: relative;
  vertical-align: middle;
}
.bottom_div_ad1 h5 {
  position: relative;
  color: #fff;
  font-weight: 600;
  font-size: 25px;
  padding: 0px 10px;
  vertical-align: middle;
}

.bottom_div_ad2 {
  line-height: normal;
  width: 100%;
  background-repeat: no-repeat;
  background-size: 100% 100%;
  background-image: linear-gradient(rgba(0, 0, 0, 0.35), rgba(0, 0, 0, 0.35)),
   url(/static/images/add3.png);
  text-align: center;
}
.bottom_div_ad2 p {
  font-size: 16px;
  color: #CED8E1;
  position: relative;
  vertical-align: middle;
  line-height: normal;
}
.bottom_div_ad2 h5 {
  position: relative;
  color: #fff;
  font-weight: 600;
  font-size: 25px;
  padding: 0px 10px;
  vertical-align: middle;
}
.bottom_div_ad h5 {
  position: relative;
  color: #fff;
  font-weight: 600;
  font-size: 25px;
  padding: 0px 10px;
  vertical-align: middle;
  line-height: normal;
}
.a-tag:hover{
  text-decoration:none;
}
.pt-20 {
padding-bottom: 20px;
}

      `}</style>
    </Layout>
  );
}
export default HomeLoggedOut;
