import React from 'react';
import Layout from 'containers/Layout';
import PurchaseRequestForm from 'components/PurchaseRequest';
import Router from 'next/router';

function PurchaseRequest() {
  if (window.localStorage.getItem('ftp_token') == null) {
    Router.push('/login');
  }
  return (
    <Layout>
      <PurchaseRequestForm />
    </Layout>
  );
}

export default PurchaseRequest;
