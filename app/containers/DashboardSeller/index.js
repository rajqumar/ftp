import React from 'react';
import Layout from 'containers/Layout';
import DashboardBuyerForm from 'components/DashboardBuyer';
import Router from 'next/router';

const YearToDateDummy = [
  {
    title: 'Quotations Sent',
    id: 1,
    number: 200,
  },
  {
    title: 'RFQ Received',
    id: 2,
    number: 150,
  },
  {
    title: 'Shipments Completed',
    id: 3,
    number: 45,
  },
];
const rating = [
  {
    id: 1,
    title: 'Payment',
    rate: 2,
  },
  {
    id: 2,
    title: 'Responsive',
    rate: 3,
  },
  {
    id: 3,
    title: 'Shipment',
    rate: 4,
  },
];
const data = [
  {
    purchaseNumber: 6,
    purchaseRequest: 'Purchase ',
    subtitle: 'Requests',
    id: 1,
  },
  {
    purchaseNumber: 14,
    purchaseRequest: 'RFQ ',
    subtitle: 'Received',
    id: 2,
  },
  {
    purchaseNumber: 3,
    purchaseRequest: 'Quotes',
    subtitle: 'Accepted',
    id: 3,
  },
  {
    purchaseNumber: 2,
    purchaseRequest: 'Purchase ',
    subtitle: 'Orders',
    id: 4,
  },
  {
    purchaseNumber: 4,
    purchaseRequest: 'Delivery',
    subtitle: 'Orders',
    id: 5,
  },
  {
    purchaseNumber: 2,
    purchaseRequest: 'Invoices  ',
    subtitle: 'Outstanding',
    id: 6,
  },
  {
    purchaseNumber: 2,
    purchaseRequest: 'Pending  ',
    subtitle: 'Payments',
    id: 7,
  },
  {
    purchaseNumber: 2,
    purchaseRequest: 'Pending  ',
    subtitle: 'Acknnowledgements',
    id: 8,
  },
];
const datasheet = [
  {
    label: 'somethingA',

    values: [
      { x: 'Jan', y: 57 },
      { x: 'Feb', y: 62 },
      { x: 'Mar', y: 9 },
      { x: 'Apr', y: 3 },
      { x: 'May', y: 66 },
      { x: 'Jun', y: 10 },
      { x: 'Jul', y: 28 },
      { x: 'Aug', y: 56 },
      { x: 'Sep', y: 83 },
      { x: 'Oct', y: 11 },
      { x: 'Nov', y: 75 },
      { x: 'Dec', y: 93 },
    ],
  },
];
function DashboardSeller() {
  if (window.localStorage.getItem('ftp_token') == null) {
    Router.push('/login');
    console.log('hello');
  }
  return (
    <Layout>
      <DashboardBuyerForm
        title="Your Procurement Journey"
        ratingTitle="Seller Rating"
        dynamicClass="sell_back_color"
        datasheet={datasheet}
        YearToDateDummy={YearToDateDummy}
        rating={rating}
        data={data}
      />
    </Layout>
  );
}

export default DashboardSeller;
