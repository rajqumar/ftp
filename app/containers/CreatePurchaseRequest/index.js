import React from 'react';
import Layout from 'containers/Layout';
import CreatePurchaseForm from 'components/CreatePurchaseRequest';
function CreatePurchaseRequest() {
  return (
    <Layout>
      <CreatePurchaseForm />
    </Layout>
  );
}

export default CreatePurchaseRequest;
