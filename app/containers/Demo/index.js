//IntegrateERP

import React from 'react';
import Layout from 'containers/Layout';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUser } from '@fortawesome/free-solid-svg-icons';
import { Table } from 'reactstrap';

function Demo() {
  // var my_time;
  // const onmouseover = () => {
  //   clearTimeout(my_time);
  // };
  // const onmouseout = () => {
  //   pageScroll();
  // };
  // const pageScroll = () => {
  //   var objDiv = document.getElementById('contain');
  //   objDiv.scrollTop = objDiv.scrollTop + 1;
  //   if (objDiv.scrollTop == objDiv.scrollHeight - 100) {
  //     objDiv.scrollTop = 0;
  //   }

  //   my_time = setTimeout(pageScroll, 300);
  // };

  return (
    <Layout>
      <div className="container">
        <div className="row order_contain">
          <div className="col-md-3 two">
            <div className="grp_buy">
              <div className="pa-0">
                <h5 className="buy_group_new">
                  <FontAwesomeIcon icon={faUser}></FontAwesomeIcon>
                  Group Buy
                </h5>
              </div>
              <Table className="table table-bordered inventory_table grp_table">
                <thead className="group_head">
                  <tr>
                    <th scope="row" className="heading">
                      Part No
                    </th>
                    <th className="heading">
                      <strong> MOQ </strong>
                    </th>
                    <th className="heading">
                      <strong> Join Group </strong>
                    </th>
                  </tr>
                </thead>
              </Table>

              <div id="contain">
                <div className="table-responsive bidding_table pa-none">
                  <Table className="table table-bordered inventory_table bidding_table_bt" id="table_scroll">
                    <tbody className="group_by">
                      <tr>
                        <td scope="row" className="heading-data group-marquee">
                          PSD630
                        </td>
                        <td className="heading-data group-marquee">100/400</td>
                        <td className="heading-data group-marquee">
                          <button type="button" className="btn btn-danger bidding_btn">
                            <img src="/static/images/teamwork.png" height="16" width="16" />
                            Join{' '}
                          </button>
                        </td>
                      </tr>

                      <tr>
                        <td scope="row" className="heading-data group-marquee">
                          PSD630
                        </td>
                        <td className="heading-data group-marquee">100/400</td>
                        <td className="heading-data group-marquee">
                          <button type="button" className="btn btn-danger bidding_btn">
                            <img src="/static/images/teamwork.png" height="16" width="16" />
                            Join{' '}
                          </button>
                        </td>
                      </tr>
                    </tbody>
                  </Table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <style>{`
        .order_contain
            {
                display: flex; 
                flex-flow: column;
                margin-bottom: 80px;
            margin-top: 80px;

            }
            .col-md-3 {
                width: 25%;
            }
            .two { order: 1; }
            .grp_buy {
                border: 1px solid #D31E25;
                border-radius: 5px 5px 0 0;
            }
            .pa-0 {
                padding-left: 0px !important;
                padding-right: 0px !important;
            }
            .buy_group_new {
                background: #D31E25;
                font-size: 18px;
                font-weight: 600;
                margin-bottom: 0px;
                margin-top: 0px;
                padding: 10px;
                border-radius: 5px 5px 0 0;
                text-align: center;
                color: #fff;
            }
            .grp_table {
                table-layout: fixed;
                margin-bottom: 0px;
            }
            
            .table-bordered {
                border: 1px solid #f4f4f4;
            }
            .inventory_table>tbody>tr>th {
                border: 1px solid #d1cbcb;
            }
            #contain {
                height: 200px;
                overflow-y: hidden;
            }
            .table-responsive {
                overflow: hidden !important;
              }
            .pa-none {
                padding: 0px !Important;
            }
            .heading {
                font-size: 11px;
                width: 70px;
                height: 50px;
                text-transform: capitalize;
                font-weight: 700;
            }
            .heading-data{
                font-size: 11px;
                width: 70px;
                height: 50px;
                text-transform: capitalize;
            }
            
            .bidding_table {
                padding: 0 10px;
            }
            #table_scroll {
                width: 100%;
                margin-top: 100px;
                margin-bottom: 100px;
                border-collapse: collapse;
            }
            
            .bidding_table_bt {
                margin-bottom: 0px;
            }
            .table-bordered {
                border: 1px solid #f4f4f4;
            }
            #table_scroll tbody td {
                border: 1px solid #d1cbcb;
                padding: 8px;
                line-height: 1.42857143;
                vertical-align: top;
            }
            .bidding_btn
                {
                        background: #d31e25;
                    border: #d31e25;
                    padding: 3px 10px;
                    font-size: 12px;
                    font-weight: 500;
                }
                .bidding_table
                {
                    padding:0 10px;
                }
                .group-marquee {
                  top: 6em;
                  position: relative;
                  box-sizing: border-box;
                  animation: group-marquee 15s linear infinite;
              }
               .group-marquee:hover {
                  animation-play-state: paused;
              }
               .group_by:hover  .group-marquee
              {
                  -webkit-animation-play-state: paused;
                  -moz-animation-play-state: paused;
                  -o-animation-play-state: paused;
                  animation-play-state: paused;
              }
              
              /* Make it move! */
              @keyframes group-marquee {
                  0%   { top:   8em }
                  100% { top: -11em }
              }
                

        `}</style>
    </Layout>
  );
}

export default Demo;
