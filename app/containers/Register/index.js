import React from 'react';
import Layout from 'containers/Layout';
import RegisterForm from 'components/RegisterForm';
// import { loginToken } from '../Login/selectors';
// import { createStructuredSelector } from 'reselect';
// import { compose } from 'redux';
// import { connect } from 'react-redux';
// import PropTypes from 'prop-types';
import Router from 'next/router';

export function Register() {
  // const isLoggedIn = loginToken.loginToken.token.length != 0 ? true : false;
  // if (isLoggedIn) {
  //   Router.push('/dashboard');
  // }
  if (window.localStorage.getItem('ftp_token') != null) {
    Router.push('/Dashboard/manager');
  }

  return (
    <Layout>
      <div className="vimeo-wrapper">
        <iframe
          src="https://player.vimeo.com/video/372627953?autoplay=1&loop=1&autopause=0&muted=1"
          frameBorder="0"
          className="reg_video"></iframe>
        <div className="container mt-5">
          <section className="content">
            <div className="row">
              <div className="col-md-6 col-md-offset-3 reg_form_new">
                <RegisterForm />
              </div>
            </div>
          </section>
        </div>
      </div>
      <style jsx>{`
        content {
          min-height: 250px;
          padding: 15px;
          margin-right: auto;
          margin-left: auto;
          padding-left: 15px;
          padding-right: 15px;
        }
        .mt-30 {
          margin-top: 30px;
        }
        .mt-5{
          margin-top:5%;
        }
        .reg_form_new {
          background: #fff;
          border: 1px solid #dddcdc;
          border-radius: 5px;
          margin: 30px auto;
          padding: 20px;
          min-height: 100%;
          margin-top: 51px !important;
          box-shadow: 0 0 15px 1px rgba(0, 0, 0, 0.07);
        }
        .vimeo-wrapper iframe {
          width: 100vw;
          height: 56.25vw;
          min-height: 100vh;
          min-width: 177.77vh;
          position: absolute;
          top: 50%;
          left: 50%;
          transform: translate(-50%, -50%);
        }
        .reg_video {
          position: fixed !important;
          top: 50% !important;
          /* left: 50%; */
          min-width: 100% !important;
          min-height: 100% !important;
          width: 200vh !important;
          z-index: -100 !important;
          -webkit-transform: translateX(-50%) translateY(-50%) !important;
          transform: translateX(-50%) translateY(-50%) !important;
          // background: url(polina.jpg) no-repeat !important;
          background-size: cover !important;
        }
      `}</style>
    </Layout>
  );
}

// Register.PropTypes = {
//   loginToken: PropTypes.func,
// };
// const mapStateToProps = createStructuredSelector({
//   loginToken: loginToken(),
// });
// const withConnect = connect(mapStateToProps, null);
// export default compose(withConnect, memo)(Register);
export default Register;
