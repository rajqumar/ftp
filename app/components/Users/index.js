import React from 'react';
import Tables from 'components/Tables';
import PropTypes from 'prop-types';
import Link from 'next/link';

const UsersList = props => {
  var { options, userLoginData, columns } = props;
  //CreateAdmin
  //ManagerUsers/CreateAdmin
  return (
    <div className="container mt-8">
      <section className="content">
        <div className="row">
          <div className="create_user_block">
            <Link href="/ManagerUsers/CreateUser">
              <button type="button" className="btn btn-success create_user_btn">
                Create User
              </button>
            </Link>
            <Link href="/ManagerUsers/createAdmin">
              <button type="button" className="btn btn-defualt create_admin_btn">
                Create Admin
              </button>
            </Link>
          </div>
          <div className="box">
            <div className="box-body">
              <Tables data={userLoginData} columns={columns} options={options} />
            </div>
          </div>
        </div>
      </section>
      <style>{`

  
  .content {
    min-height: 250px;
    padding: 15px;
    margin-right: auto;
    margin-left: auto;
    padding-left: 15px;
    padding-right: 15px;
  }
  .mt-8{
   margin-top:8%;
  }
  .create_user_block {
    margin-bottom: 30px;
    margin-top: 10px;
    }
    .create_user_btn {
      background-color: #6DC144;
      border: 1px solid #6DC144;
      color: #fff;
      text-transform: uppercase;
      margin: 0 10px;
      font-size: 12px;
      padding: 10px 20px;
  }
  
  .create_admin_btn {
    background-color: #fff;
    border: 1px solid #ccc;
    color: #000;
    text-transform: uppercase;
    margin: 0 10px;
    font-size: 12px;
    padding: 10px 20px;
  }
  
.btn-success {
    background-color: #00a65a;
    border-color: #008d4c;
}

.btn {
    border-radius: 3px;
    -webkit-box-shadow: none;
    box-shadow: none;
    border: 1px solid transparent;
}
  .box {
    position: relative;
    border-radius: 3px;
    background: #ffffff;
    border: 1px solid #ececec;
    margin-bottom: 20px;
    width: 100%;
    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
}
.box-body {
  border-top-left-radius: 0;
  border-top-right-radius: 0;
  border-bottom-right-radius: 3px;
  border-bottom-left-radius: 3px;
  padding: 10px;
}
.red_name {
  margin: 0px;
  color: #D2232A;
}
.mob_icon {
  font-size: 24px;
  margin-right: 5px;
  color: #D2232A;
}
.mail_icon {
  font-size: 20px;
  color: #D2232A;
}
  
  `}</style>
    </div>
  );
};
UsersList.propTypes = {
  columns: PropTypes.array,
  userLoginData: PropTypes.array,
  options: PropTypes.object,
};

export default UsersList;
