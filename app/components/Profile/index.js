import React from 'react';
import { faUser, faBuilding } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Countries from '../RegisterForm/countries';

import { InputGroup, InputGroupText, Input } from 'reactstrap';

const Title = [{ title: 'Developer' }, { title: 'Designer' }, { title: 'Manager' }, { title: 'User' }];

const ProfileForm = () => {
  const showoption = () => {
    var optionarray = [];
  
    Countries.country_list.filter((item, index) => {
      optionarray.push(
        <option key={index} value={item.name} className="capitalCountry">
          {item.name}
        </option>
      );
    });
    return optionarray;
  };
  return (
    <div className="container">
      <section className="content">
        <div className="row">
          <div className="col-md-6 col-md-offset-3 reg_form_new">
            <form id="msform">
              <h5 className="register_main_head">My Profile</h5>

              <fieldset>
                <h2 className="fs-title">
                  <FontAwesomeIcon icon={faBuilding} />
                  &nbsp;Company Information
                </h2>
                <div className="row">
                  <div className="col-md-6">
                    <div className="form-group">
                      <label htmlFor="name" className="fl-left">
                        Name
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        id="name"
                        name="name"
                        placeholder="Shrishailya Deshmukh"
                      />
                    </div>
                    <div className="form-group">
                      <label htmlFor="Email" className="fl-left">
                        Email
                      </label>
                      <input
                        type="email"
                        className="form-control"
                        id="Email"
                        name="Email"
                        placeholder="shri@hellosme.com"
                      />
                    </div>

                    <div className="form-group">
                      <label htmlFor="Address" className="fl-left">
                        Address
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        id="Address"
                        name="Address"
                        placeholder="Scared world,Singapore"
                      />
                    </div>
                    <div className="form-group">
                      <input
                        type="text"
                        className="form-control"
                        id="phone"
                        name="phone"
                        placeholder="Scared world,Singapore"
                      />
                    </div>
                  </div>

                  <div className="col-md-6">
                    <div className="form-group">
                      <label htmlFor="uen" className="fl-left">
                        UEN
                      </label>
                      <input type="text" className="form-control" id="name" name="name" placeholder="201898924E" />
                    </div>

                    <div className="form-group">
                      <label htmlFor="phone" className="fl-left">
                        Phone
                      </label>
                      <InputGroup size="sm">
                        <InputGroupText style={{ fontSize: '12px !important' }}>{'+'}</InputGroupText>
                        <Input
                          type="number"
                          className="form-control"
                          id="c_phone"
                          name="c_phone"
                          placeholder=" 6789 1234"
                          required
                        />
                      </InputGroup>
                      {/* <input type="number" className="form-control" id="phone" name="phone" placeholder="6567891234" /> */}
                    </div>

                    <div className="form-group">
                      <label htmlFor="email" className="fl-left">
                        Country
                      </label>
                      <select className="form-control select2" id="country" name="country" required>
                        <option value="default">Select Country</option>
                        {/* {Countries.country_list.map((c, i) => (
                          <option key={i} value={c.value} className="capitalCountry">
                            {c.value}
                          </option>
                        ))} */}
                       {showoption()}
                      </select>
                    </div>
                  </div>
                </div>
                <h2 className="fs-title">
                  <FontAwesomeIcon icon={faUser} />
                  &nbsp;Your Information
                </h2>

                <div className="row">
                  <div className="col-md-6">
                    <div className="form-group">
                      <label htmlFor="name" className="fl-left">
                        First name
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        id="name"
                        name="name"
                        placeholder="Shrishailya Deshmukh"
                      />
                    </div>
                    <div className="form-group">
                      <label htmlFor="Email" className="fl-left">
                        Email
                      </label>
                      <input
                        type="email"
                        className="form-control"
                        id="Email"
                        name="Email"
                        placeholder="shri@hellosme.com"
                      />
                    </div>
                    <div className="form-group">
                      <label htmlFor="confirm_password" className="fl-left">
                        New Password
                      </label>
                      <input
                        type="password"
                        className="form-control"
                        id="new_password"
                        name="new_password"
                        placeholder="Leave blank if unchanged"
                      />
                    </div>
                    <div className="form-group">
                      <label htmlFor="Title" className="fl-left">
                        Title
                      </label>
                      <select className="form-control select2" id="u_title" name="u_title">
                        <option value="default">Select Title</option>
                        {Title.map((c, i) => (
                          <option key={i} value={c.title} selected>
                            {c.title}
                          </option>
                        ))}
                      </select>
                    </div>
                    {/* <div className="form-group">
                      <label htmlFor="Title" className="fl-left">Title</label>
                      <input type="text" className="form-control" id="Title" name="Title" placeholder="Director" />
                    </div> */}
                  </div>

                  <div className="col-md-6">
                    <div className="form-group">
                      <label htmlFor="name" className="fl-left">
                        Last name
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        id="name"
                        name="name"
                        placeholder="Shrishailya Deshmukh"
                      />
                    </div>

                    <div className="form-group">
                      <label htmlFor="phone" className="fl-left">
                        Phone
                      </label>
                      <input type="number" className="form-control" id="phone" name="phone" placeholder="65131241241" />
                    </div>
                    <div className="form-group">
                      <label htmlFor="confirm_password" className="fl-left">
                        Confirm Password
                      </label>
                      <input
                        type="password"
                        className="form-control"
                        id="confirm_password"
                        name="confirm_password"
                        placeholder=""
                      />
                    </div>
                  </div>
                </div>

                <center>
                  <button type="button" className="btn btn-success save_changes">
                    Save changes
                  </button>
                </center>
              </fieldset>
            </form>
          </div>
        </div>
      </section>
      <style>{`
        .content {
            min-height: 250px;
            padding: 15px;
            margin-right: auto;
            margin-left: auto;
            padding-left: 15px;
            padding-right: 15px;
        }
        .captialCountry{
          text-transform: capitalize
        }
        .reg_form_new {
          background: #fff;
          border: 1px solid #dddcdc;
          border-radius: 5px;
          min-height: 100%;
          box-shadow: 0 0 15px 1px rgba(0, 0, 0, 0.07);
          margin-top: 12%;

        }
        .col-md-offset-3 {
          margin-left: 25%;
        }
        .col-md-6 {
          width: 50%;
      }
      option.capitalCountry {
        text-transform: capitalize;
    }
      #msform {
        text-align: center;
            position: relative;
        }
        .register_main_head {
          font-size: 32px;
          font-weight: bold;
          margin: 25px auto;
      }
      #msform fieldset {
        border: 0 none;
        border-radius: 0px;
        padding: 20px 30px;
        transform: unset !important;
        box-sizing: border-box;
        position: relative !important;
    }
    .fs-title {
      font-size: 21px;
      text-transform: capitalize;
      color: #333;
      margin-bottom: 20px;
      letter-spacing: 1px;
      text-align: left;
      font-weight: bold;
  }
    .fl-left {
      float: left;
    font-size: 15px;
    font-weight: 800;
  }
  label {
      display: inline-block;
      max-width: 100%;
      margin-bottom: 5px;
      font-weight: 700;
  }
  .save_changes {
    background-color: #D2232A !important;
    border: 1px solid #D2232A;
    margin-top: 25px;
    text-transform: uppercase;
    font-size: 14px;
}
.btn-success {
  background-color: #00a65a;
  border-color: #008d4c;
}
.btn {
  border-radius: 3px;
  -webkit-box-shadow: none;
  box-shadow: none;
  border: 1px solid transparent;
}

    
    `}</style>
    </div>
  );
};
export default ProfileForm;
