import React, { useState } from 'react';
import {
  Collapse,
  NavbarToggler,
  Nav,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from 'reactstrap';
import MenuItems from '../MenuItems';
import { useRouter } from 'next/router';

// conditions to follow for this component
// menu ==> logged out state
// toggle button ==> buyer / seller logged in dashboard
// blank ==> manager / admin logged in dashboard

const HeaderMenu = (props) => {

  const {isLoggedIn, menuItems} = props

  const router = useRouter();
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  const logout = () => {
    localStorage.removeItem('ftp_token');
    window.location.reload();
    router.push('/');
  };

  var logoutButton = null;
  if (isLoggedIn) {
    logoutButton = (
      <NavLink className="nav_link_head">
        <DropdownItem className="first-active-menu" key="login" onClick={logout}>
          LOGOUT
        </DropdownItem>
      </NavLink>
    );
  }

  return (
    <div>
      <NavbarToggler onClick={toggle} className="mr-2" />
      <Collapse isOpen={isOpen} navbar className="collapse navbar-collapse pull-left">
        <Nav navbar>
          {menuItems.map((m, i) =>
            m.submenu && m.submenu.length > 0 ? (
              <UncontrolledDropdown key={i}>
                <DropdownToggle nav caret className="static-menu-title">
                  {m.name}
                </DropdownToggle>
                <DropdownMenu down="true">
                  {m.submenu.map((subitem, j) => (
                    <NavLink
                      className="nav_link_head"
                      href={subitem.url != '' ? subitem.url : '#'}
                      target="_blank"
                      key={j}>
                      <DropdownItem className="first-active-menu" to={subitem.url}>
                        {subitem.name}
                      </DropdownItem>
                    </NavLink>
                  ))}
                  {logoutButton}
                </DropdownMenu>
              </UncontrolledDropdown>
            ) : (
              <MenuItems key={i} name={m.name} icon={m.icon} url={m.url} />
            )
          )}
        </Nav>
      </Collapse>

      <style>{`
        .subitem-menu{
          background-color: #ccc;
          text-transform: uppercase;
          font-size: 11px;
          font-weight: 600;
        }
      
 
        .subitem-menu:hover {
          color: #d2232a !important;
          text-decoration: none;
          background-color: #f8f9fa;
          font-size: 11px;
          text-transform: uppercase;
          font-weight: 600;
      }
        .first-active-menu{
          font-size: 11px;
          font-weight: 600;
          text-transform: uppercase;
        }
        .static-menu-title{
          text-transform: uppercase;
          font-weight: 600;
          color: rgba(37, 32, 32, 1) !important;
          font-size: 12px !important;
        }
        .static-menu-title:hover{
          background-color: transparent !important;
         color: #d2232a !important;
        }
        `}</style>
    </div>
  );
};

export default HeaderMenu;
