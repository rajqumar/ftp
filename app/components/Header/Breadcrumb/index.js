import React from 'react';
import { withRouter } from 'next/router';
import PropTypes from 'prop-types';

function Breadcrumb({ router }) {
  var path = router.pathname;
  path = path.split('/');
  var count = path.length;

  // console.log(count, 'count');

  var links = path.map((p, i) => (
    <React.Fragment key={i}>
      <li>
        <a href={router.pathname} className="white text_bred">
          &nbsp;{p} {i < count - 1 && i !== count - 1 ? ' /' : ''}
          {/* &nbsp;{p} {i < count - 1 && i !== count - 1 && count !== 2 ? ' /' : ''} */}
        </a>
      </li>
      <style jsx>{`
        .white {
          color: #fff !important;
        }
        .text_bred {
          text-transform: capitalize;
        }
      `}</style>
    </React.Fragment>
  ));

  return (
    <section className="breadcrumb_top">
      <div className="container">
        <ol className="breadcrumb">
          <li>
            <a href="/">Home </a>
          </li>
          {links}
        </ol>
      </div>

      <style jsx>{`
        .breadcrumb_top {
        
          background-color: rgba(210, 35, 42, 1.0);
    margin-top: 75px;
    position: fixed;
    width: 100%;
    z-index: 9;
        }

        .breadcrumb {
          background-color: transparent !important;
          padding: 10px 15px !important;
          margin-bottom: 0px !important;
        }

        .breadcrumb li a {
          color: #fff !important;
        }
        .white {
          color: #fff !important;
        }
      `}</style>
    </section>
  );
}

Breadcrumb.propTypes = {
  router: PropTypes.object,
};

export default withRouter(Breadcrumb);
