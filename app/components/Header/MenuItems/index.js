import React from 'react';
import { NavItem, NavLink } from 'reactstrap';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

function MenuItems(props) {
  const { name, icon, url, key } = props;

  return (
    <div key={key} className="menu_items">
      <NavItem>
        <NavLink className="nav_link_head static-menu-title" href={url}>
          <FontAwesomeIcon icon={icon} width="16" /> {name}
        </NavLink>
      </NavItem>

      <style jsx>{`
        .menu_items {
          color: #333 !important;
          padding: 0px 0px 0 20px;
          text-transform: uppercase;
        }
        .menu_items > li > a {
          color: #333 !important;
        }
        .static-menu-title {
          text-transform: uppercase;
          font-weight: 600;
          color: rgba(37, 32, 32, 1) !important;
          font-size: 12px !important;
        }

        .static-menu-title:hover {
          color: red !important;
        }
      `}</style>
    </div>
  );
}

MenuItems.propTypes = {
  name: PropTypes.string,
  icon: PropTypes.object,
  url: PropTypes.string,
  key: PropTypes.integer,
};

export default MenuItems;
