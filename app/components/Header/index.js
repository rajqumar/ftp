import React from 'react';
import { Navbar, NavbarBrand } from 'reactstrap';
import HeaderMenu from './HeaderMenu';
import {
  faSearch,
  faUser,
  faUserPlus,
  faChartBar,
  faFilePowerpoint,
  faFile,
  faMicrochip,
} from '@fortawesome/free-solid-svg-icons';

import PropTypes from 'prop-types';
import Toggle from 'components/Toggle';

function Header(props) {
  const { isLoggedIn } = props;

  const menuLoggedIn = [
    {
      name: 'SEARCH',
      icon: faMicrochip,
      submenu: [
        {
          name: 'PARTS',
          url: '',
        },
        {
          name: 'SUPPLIERS',
          url: '',
        },
      ],
    },
    {
      name: 'DASHBOARD',
      icon: faChartBar,
      submenu: [
        {
          name: 'MANAGER',
          url: '/Dashboard/manager',
        },
        {
          name: 'BUY',
          url: '/Dashboard/buyer',
        },
        {
          name: 'SELL',
          url: '/Dashboard/seller',
        },
      ],
    },
    {
      name: 'QUOTES',
      icon: faFile,
      submenu: [
        {
          name: 'PURCHASE REQUESTS',
          url: '',
        },
        {
          name: 'REQUEST FOR QUOTIONS',
          url: '',
        },
        {
          name: 'QUOTATIONS',
          url: '',
        },
      ],
    },
    {
      name: 'ORDERS',
      icon: faFilePowerpoint,
      submenu: [
        {
          name: 'MY PROFILE',
          url: '/profile',
        },
        {
          name: 'MANAGE USERS',
          url: '/ManagerUsers/users',
        },
        {
          name: 'INTEGRATE ERP',
          url: '/integrateERP',
        }
      ],
    },
    {
      name: 'PAYMENTS',
      icon: faFilePowerpoint,
      submenu: [
        {
          name: 'INVOICE',
          url: '/profile',
        },
        {
          name: 'PAYMENT',
          url: '/ManagerUsers/users',
        },
      ],
    },
    {
      name: 'MY ACCOUNT',
      icon: faFilePowerpoint,
      submenu: [
        {
          name: 'MY PROFILE',
          url: '/profile',
        },
        {
          name: 'MANAGE USERS',
          url: '/ManagerUsers/users',
        },
        {
          name: 'INTEGRATE ERP',
          url: '/integrateERP',
        },
      ],
    },
  ]

  const menuLoggedOut = [
    {
      name: 'New to ftp',
      icon: '',
      submenu: [
        {
          name: 'About Us',
          url: '',
        },
        {
          name: 'FTP Core & Advisory Teams',
          url: '',
        },
        {
          name: 'What is FTP',
          url: '',
        },
        {
          name: 'FTP Features & Benefits',
          url: '',
        },
        {
          name: 'FTP Membership Plan',
          url: '',
        },
        {
          name: 'SMEs’ FAQ',
          url: '',
        },
      ],
    },
    {
      name: 'Value-Added Services',
      icon: '',
      submenu: [
        {
          name: 'Berkley Insurance ASIA - Marine',
          url: '',
        },
        {
          name: 'OFX - Global Money Transfers',
          url: '',
        },
        {
          name: 'GOGOVAN Singapore – Last Mile Fulfilment',
          url: '',
        },
        {
          name: 'Funding Societies – SME Financing',
          url: '',
        },
        {
          name: 'FTP Discounting Solution',
          url: '',
        },
      ],
    },
    {
      name: 'Regional Markets',
      icon: '',
      submenu: [
        {
          name: 'Asean',
          url: 'https://ftp-lite-node.hellosme.com',
        },
        {
          name: 'Greater China',
          url: '',
        },
        {
          name: 'India',
          url: '',
        },
        {
          name: 'Japan',
          url: '',
        },
        {
          name: 'Korea',
          url: '',
        },
      ],
    },
    { name: 'Search Parts / suppliers', icon: faSearch, url: '/' },
    { name: 'Login', icon: faUser, url: '/login' },
    { name: 'Register', icon: faUserPlus, url: '/register' },
  ]

  return (
    <header className="main-header fixed-headbar">
      <Navbar expand="md">
        <div className="container">
          <NavbarBrand href="/" className="mr-auto">
            <a href="/" className="navbar-brand img-responsive logo_navbrand">
              <p className="logo_head">
                <img src="/static/images/logo_fav.png" height="30" width="30" />
                FTP
              </p>
              <p className="logo_subhead">
                Powered by <span className="yellow_color">Hello</span>
                <span className="red_logo">SME</span>
              </p>
            </a>
          </NavbarBrand>
          <HeaderMenu menuItems={isLoggedIn ? menuLoggedIn : menuLoggedOut} isLoggedIn={isLoggedIn}/>
          { isLoggedIn ? <Toggle /> : null }
        </div>
      </Navbar>

      <style jsx>{`
        .logo_head {
          color: #d31e25;
          font-size: 30px;
          font-weight: 700;
          margin: 0px;
        }
        .red_logo {
          color: #d31e25;
        }

        .yellow_color {
          color: #f1b702;
        }

        .logo_subhead {
          color: #333;
          font-size: 14px;
          /* font-weight: 700; */
          margin-bottom: 10px;
        }
        .logo_navbrand {
          float: left;
          height: 30px;
          width: 215px;
          padding: 10px 15px;
          font-size: 18px;
          line-height: 15px;
          padding-bottom: 41px !important;
        }
        .main-header {
          position: relative;
          max-height: 100px;
          z-index: 1030;
        }
        //navabar
        .navbar-collapse {
          width: auto;
          border-top: 0;
          -webkit-box-shadow: none;
          box-shadow: none;
        }
        .dummytoggle {
          float: right !important;
          /* margin: 0; */
          right: 0 !important;
          position: absolute;
        }
        .navbar-nav {
          float: left;
          margin: 0;
        }
        .buy_sell_toggle {
          margin-top: 15%;
        }
        .nav li {
          position: relative;
          display: block;
        }
        .btn-toggle.btn-lg.active {
          transition: background-color 0.25s;
        }
        .btn-toggle.btn-lg {
          margin: 0 5rem;
          padding: 0;
          position: relative;
          border: none;
          height: 2.5rem;
          width: 4rem;
          border-radius: 2.5rem;
        }
        .btn-toggle.active {
          background-color: #fff5d7;
        }
        .btn-toggle.active {
          transition: background-color 0.25s;
        }
        .btn.active,
        .btn:active {
          background-image: none;
          outline: 0;
          -webkit-box-shadow: inset 0 3px 5px rgba(0, 0, 0, 0.125);
          box-shadow: inset 0 3px 5px rgba(0, 0, 0, 0.125);
        }
        .btn-toggle {
          margin: 0 4rem;
          padding: 0;
          position: relative;
          border: none;
          height: 1.5rem;
          width: 3rem;
          border-radius: 1.5rem;
          color: #6b7381;
          border: 1px solid #cbc6c6 !Important;
          background: #d1edff;
        }
        .btn {
          border-radius: 3px;
          -webkit-box-shadow: none;
          box-shadow: none;
          border: 1px solid transparent;
        }
        .btn.active .btn:active {
          background-image: none;
          outline: 0;
          -webkit-box-shadow: inset 0 3px 5px rgba(0, 0, 0, 0.125);
          box-shadow: inset 0 3px 5px rgba(0, 0, 0, 0.125);
        }
        .btn-toggle {
          margin: 0 4rem;
          padding: 0;
          position: relative;
          border: none;
          height: 1.5rem;
          width: 3rem;
          border-radius: 1.5rem;
          color: #6b7381;
          border: 1px solid #cbc6c6 !Important;
          background: #d1edff;
        }
        .btn-toggle.btn-lg.active .handle {
          transition: left 0.25s;
        }
        .btn-toggle.btn-lg:before,
        .btn-toggle.btn-lg:after {
          line-height: 2.5rem;
          width: 5rem;
          text-align: center;
          font-weight: 600;
          font-size: 14px;
          color: #000;
          text-transform: uppercase;
          letter-spacing: 0px;
          position: absolute;
          bottom: 0;
        }
        .btn-toggle.btn-lg .handle {
          position: absolute;
          width: 22px;
          border: 1px solid #e1e0e0;
          height: 22px;
          border-radius: 1.875rem;
          background: #fff;
          transition: left 0.25s;
        }
        .btn-toggle.active .handle {
          left: 1.6875rem;
          transition: left 0.25s;
        }
        .btn-toggle .handle {
          position: absolute;
          top: 0;
          left: 0;
          width: 1.125rem;
          height: 1.125rem;
          border-radius: 1.125rem;
          background: #fff;
          transition: left 0.25s;
        }
        .btn-toggle.btn-lg.active:after {
          opacity: 1;
        }
        .btn-toggle.btn-lg:after {
          content: 'Sell';
          right: -5rem;
          opacity: 0.5;
        }
        .btn-toggle.btn-lg:before,
        .btn-toggle.btn-lg:after {
          line-height: 2.5rem;
          width: 5rem;
          text-align: center;
          font-weight: 600;
          font-size: 14px;
          color: #000;
          text-transform: uppercase;
          letter-spacing: 0px;
          position: absolute;
          bottom: 0;
        }

        .navbar-nav > li {
          float: left;
        }
        .btn-toggle:after {
          content: 'On';
          right: -4rem;
          opacity: 0.5;
        }
        .btn-toggle:before .btn-toggle:after {
          line-height: 1.5rem;
          width: 4rem;
          text-align: center;
          font-weight: 600;
          font-size: 0.75rem;
          text-transform: uppercase;
          letter-spacing: 2px;
          position: absolute;
          bottom: 0;
          transition: opacity 0.25s;
        }
        .btn-toggle.btn-lg.active:after {
          opacity: 1;
        }
        .btn-toggle.btn-lg:after {
          content: 'Sell';
          right: -5rem;
          opacity: 0.5;
        }

        .btn-toggle.active:after {
          /* opacity: 1; */
        }

        .btn-toggle:after {
          content: 'On';
          right: -4rem;
          opacity: 0.5;
        }

        .btn-toggle.btn-lg.active:before {
          opacity: 1;
        }
        .btn-toggle.btn-lg:before {
          content: 'BUY';
          left: -5rem;
        }

        .btn-toggle.active:before {
          opacity: 0.5;
        }

        .btn-toggle:before {
          content: 'Off';
          left: -4rem;
        }
        .btn-toggle:before,
        .btn-toggle:after {
          line-height: 1.5rem;
          width: 4rem;
          text-align: center;
          font-weight: 600;
          font-size: 0.75rem;
          text-transform: uppercase;
          letter-spacing: 2px;
          position: absolute;
          bottom: 0;
          transition: opacity 0.25s;
          color: #6b7381;
        }
        //renew start

        .Toggles {
          margin-top: 50px;
        }

        .toggleHolder {
          text-align: center;
        }

        .toggleHolder h3,
        .toggleHolder p {
          text-align: center;
          margin: 5px;
          color: rgba(0, 0, 0, 0.8);
        }

        .toggleWrapper {
          width: 100%;
        }

        .toggleWrapper .toggleLabel {
          width: 50px;
          display: inline-block;
        }

        .toggleWrapper .toggleLabel p {
          color: rgba(0, 0, 0, 0.5);
          overflow: hidden;
          transition: all 0.3s ease-in-out;
        }

        .toggleWrapper .toggleLabel.active p {
          color: rgba(0, 0, 0, 0.8);
        }

        .toggle {
          display: inline-block;
          width: 50px;
          height: 25px;
          border-radius: 100px;
          position: relative;
          margin: auto;
          overflow: hidden;
          border: solid 1px rgba(0, 0, 0, 0.3);
          transition: all 0.3s ease-in-out;
          cursor: pointer;
        }

        .toggle.active {
          border: solid 1px #00cf67;
        }

        .toggle .inside {
          width: 75%;
          height: 100%;
          border-radius: 100px;
          background: rgba(0, 0, 0, 0.3);
          position: absolute;
          left: -25%;
          transition: all 0.3s ease-in-out;
        }

        .toggle.active .inside {
          left: 50%;
          background: #00cf67;
        }
        .logo_navbrand {
          float: left;
          height: 30px;
          width: 215px;
          padding: 10px 15px;
          font-size: 18px;
          line-height: 15px;
        }
        .fixed-headbar {
          margin-left: 0 !important;
          box-shadow: 0 2px 2px -2px rgba(0, 0, 0, 0.35) !important;
          position: fixed !important;
          top: 40px !important;
          background-color: #fff !important;
          position: fixed !important;
          top: 0 !important;
          width: 100% !important;
          z-index: 999 !important;
        }        
      `}</style>
    </header>
  );
}

Header.propTypes = {
  data: PropTypes.array,
  changeUser: PropTypes.function,
};
export default Header;
