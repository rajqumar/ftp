//newCreatePurchaseReq
import React from 'react';
// import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMicrochip,faFilePowerpoint, faChartBar, faFile, faCreditCard, faUser } from '@fortawesome/free-solid-svg-icons';
import { DropdownToggle, DropdownMenu, DropdownItem, UncontrolledDropdown } from 'reactstrap';
import Router from 'next/router';
var dummyarray = [
  {
    title: 'SEARCH',
    icon: faMicrochip,
    option: [
      {
        name: 'PARTS',
        url: '#',
      },
      {
        name: 'SUPPLIERS',
        url: '#',
      },
    ],
  },
  {
    title: 'DASHBOARD',
    icon: faChartBar,
    option: [
      {
        name: 'MANAGER',
        url: '/Dashboard/manager',
      },
      {
        name: 'BUY',
        url: '/Dashboard/buyer',
      },
      {
        name: 'SELL',
        url: '/Dashboard/seller',
      },
    ],
  },
  {
    title: 'QUOTES',
    icon: faFile,
    option: [
      {
        name: 'PURCHASE REQUESTS',
        url: '/Quotes/purchaseRequests',
        ///Quotes/purchaseRequests
      },
      {
        name: 'REQUEST FOR QUOTIONS',
        url: '#',
      },
      {
        name: 'QUOTATIONS',
        url: '#',
      },
    ],
  },
  {
    title: 'ORDERS',
    icon: faFilePowerpoint,
    option: [
      {
        name: 'MY PROFILE',
        url: '/profile',
      },
      {
        name: 'MANAGE USERS',
        url: '/ManagerUsers/users',
      },
      {
        name: 'INTEGRATE ERP',
        url: '/integrateERP',
      },
    ],
  },
  {
    title: 'PAYMENTS',
    icon: faCreditCard,
    option: [
      {
        name: 'INVOICE',
        url: '#',
      },
      {
        name: 'PAYMENTS',
        url: '#',
      }
    ]
  },
  // {
  //   title: 'MY ACCOUNT',
  //   icon: faUser,
  //   option: [
  //     {
  //       name: 'My Profile',
  //       url: '/profile',
  //     },
  //     {
  //       name: 'Manage Users',
  //       url: '/ManagerUsers/users',
  //     },
  //     {
  //       name: 'Integrate ERP',
  //       url: '/integrateERP',
  //     },{
  //       name:' Logout',
  //       url:'#'
  //     }
  //   ]
  // }
];
const onSelect = e => {
  //  this.setState({dropDownValue: e.currentTarget.textContent});
  if (e.target.value == 'logout') {
    window.localStorage.removeItem('ftp_token');
    window.location.reload();
    Router.push('/login');
  } else if (e.target.value != undefined && e.target.value != 'logout' && e.target.value != '#') {
    Router.push(e.target.value);
  } else {
    // Router.push(e.target.value);
    ///profile
  }
  // console.log(e.target.value,'e.target.value')
};
function LoginMenuHeader() {
    return(
        <div className="collapse navbar-collapse pull-left mt-15" id="navbar-collapse">
        <ul className="nav navbar-nav ">
            
            {dummyarray.map((item, index) => (
        <UncontrolledDropdown nav inNavbar key={index} onClick={onSelect} className="last_child_middle">
          <DropdownToggle nav>
           
            <p className="header-title"><FontAwesomeIcon icon={item.icon} width="12" />&nbsp;{item.title}</p>
          </DropdownToggle>
          <DropdownMenu right>
            {item.option.map((opt, indexnew) => (
              <DropdownItem className="sublinkmenu" key={indexnew} value={opt.url} to={opt.url}>
                 {opt.name}
                {/* <NavLink href={opt.url != '' ? opt.url : ''} className="custom-link a-tag ">
                  {' '}
                  {opt.name}
                </NavLink> */}
              </DropdownItem>
            ))}
          </DropdownMenu>
        </UncontrolledDropdown>
      ))}
         <UncontrolledDropdown nav inNavbar onClick={onSelect}   className="last_child_middle">
        <DropdownToggle nav>
          <p className="header-title"><FontAwesomeIcon icon={faUser} width="12" />&nbsp;MY ACCOUNT</p>
        </DropdownToggle>
        <DropdownMenu right>
          <DropdownItem className="sublinkmenu" value="/profile">
            My Profile
            
          </DropdownItem>
          <DropdownItem className="sublinkmenu" value="/ManagerUsers/users">
            Manage Users
          </DropdownItem>
          <DropdownItem />
          <DropdownItem className="sublinkmenu" value="/integrateERP">
            Integrate ERP
          </DropdownItem>
          <DropdownItem value="logout" className="sublinkmenu">
            Logout
          </DropdownItem>
        </DropdownMenu>
      </UncontrolledDropdown>    
    
            </ul>
<style>{`
 .final_nav ul li a {
    color: #262626;
    font-weight: 600;
    line-height: 45px;
}
.pull-left {
    float: left!important;
}
.mt-15{
  margin-bottom: -12px;
}
.black {
  color: #333 !important;
}
.sublinkmenu {
    display: block;
    padding: 0 20px;
    line-height: 45px;
    background: #fff;
    color: #262626;
    text-decoration: none;
    font-size: 14px;
    font-weight: 600;
}

.sublinkmenu:hover {
  color: #D2232A;
  text-decoration: none;
  background-color: #f8f9fa;
}
.nav-list {
  position:absolute;
}

.second_nav ul li a:hover, .second_nav ul li a:visited:hover {
  color: #D2232A;
}
.custom-link{
text-align: left;
padding-left: 0px !important;
display: block;
padding: 3px 20px;
clear: both;
font-weight: 400;
line-height: 1.42857143;
color: #333;
white-space: nowrap;
}
}

p.header-title {
color: #333;
font-size: 14px;
padding-right: 10px;
}


li.last_child_middle.dropdown.nav-item a {
color: #333;
font-size: 12px;
padding-right: 20px;
font-weight: 600;

}
button.sublinkmenu.dropdown-item a {
color: #333 !important;
font-size: 11px;
font-weight: 500;
}


`}</style>
        </div>
    )
}
export default LoginMenuHeader;

