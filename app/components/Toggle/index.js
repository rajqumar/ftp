import React from 'react';
import PropTypes from 'prop-types';
import Router from 'next/router';
// import HeaderStaticMenu from 'components/HeaderStaticMenu';
function Toggle() {
  // const [message] = useState( { message: '' } );

  // var { changeUser } = props;
  var isChecked = false;
  var defaultColor = {};
  var defaultactiveClass = '';
  const handleChange = () => {
    isChecked = !isChecked;

    // console.log(message,'message',e.target.value)
    if (isChecked == true) {
      Router.push('/Dashboard/seller');
      defaultColor = { backgroundColor: '#D3EFFF' };
    } else {
      Router.push('/Dashboard/buyer');
      defaultColor = { backgroundColor: '#fcfba7' };
    }
  };
  if (Router.route == '/Dashboard/seller') {
    defaultactiveClass = 'sellactive';
    defaultColor = { backgroundColor: '#D3EFFF' };
  } else {
    defaultactiveClass = 'buyeractive';
    defaultColor = { backgroundColor: '#fcfba7' };
  }
  const isLoggedIn = window.localStorage.getItem('ftp_token') != null ? true : false;

  var showtoggle =isLoggedIn;
  if(Router.route == '/Dashboard/manager'){
    showtoggle = false
  }
console.log(showtoggle,'toggleshow');
  var firstactive = isChecked ? 'highlight' : '';
  return (
    <React.Fragment>
      {/* <HeaderStaticMenu /> */}
      {showtoggle ?
      <div className="main-toogle">
      <span className={'buy ' + firstactive}>Buy</span>

<label className="switch">
  <input type="checkbox" value={isChecked} onChange={handleChange} />
  <div className={defaultactiveClass == 'sellactive' ? 'sellactive' : 'slider'} style={defaultColor}></div>
</label>

<span className={'sell ' + firstactive}>Sell</span>

      </div>: null}
      
      <style>{`
    .switch {
      position: relative;
      display: inline-block;
      width: 60px;
      height: 34px;
      outline: none;
    }
    .main-toogle{
       margin-top: 9px;
    }
    .switch input {
      position: absolute;
      top: -99999px;
      left: -99999px;
    }
    .slider {
      position: absolute;
      cursor: pointer;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      background-color: #fcfba7;
      -webkit-transition: .4s;
      transition: .4s;
      border-radius: 34px;
    }
    .sellactive {
      position: absolute;
      cursor: pointer;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      background-color: #fcfba7;
      -webkit-transition: .4s;
      transition: .4s;
      border-radius: 34px;
    }
    .sellactive:before{
      position: absolute;
      content: "";
      height: 26px;
      width: 26px;
      right: 4px;
      bottom: 4px;
      background-color: white;
      -webkit-transition: .4s;
      transition: .4s;
      border-radius: 50%;
    }
    .slider:before {
      position: absolute;
      content: "";
      height: 26px;
      width: 26px;
      left: 4px;
      bottom: 4px;
      background-color: white;
      -webkit-transition: .4s;
      transition: .4s;
      border-radius: 50%;
    }
    input:checked + .slider {
      background-color: #D3EFFF;;
    }
    input:focus + .slider {
      box-shadow: 0 0 1px #2196F3;
    }
    input:checked + .slider:before {
      -webkit-transform: translateX(26px);
      -ms-transform: translateX(26px);
      transform: translateX(26px);
    }
    .highlight{
      color:grey;
    
    }
    .buy{
      padding-right: 20px;
      margin-top: 7px;
      margin-left: -35px;
      position: absolute;
    }
    .sell{
      padding-right: 20px;
      margin-left: 12px;
      margin-top: 7px;
      position: absolute;
    }
    `}</style>
    </React.Fragment>
  );
}
Toggle.propTypes = {
  changeUser: PropTypes.function,
};

export default Toggle;
