import React from 'react';
import Button from 'components/Button';
import { reduxForm, Field } from 'redux-form';
import PropTypes from 'prop-types';
import Router from 'next/router';
import Link from 'next/link';
import { FormGroup, Label, Input, Alert } from 'reactstrap';
import Loader from 'components/Loader';
// import { useToasts } from 'react-toast-notifications';

function LoginForm(props) {
  // const { addToast } = useToasts();

  const { submitForm, handleSubmit, data } = props;

  //Dashboard/manager
  const { loading, error } = data;
  var checked = false;
  var showError = false;
  if (error) {
    showError = true;
  }

  // if (token && token.length > 0) {
  //   //localStorage.setItem('ftp_token',
  //   Router.push('/Dashboard/manager');
  // }
  if (window.localStorage.getItem('ftp_token') != null) {
    Router.push('/Dashboard/manager');
    // Router.push('https://ftp-demo.hellosme.com/dashboard_manager.html')
    // window.location.href = 'https://ftp-demo.hellosme.com/dashboard_manager.html';
  }
  var email = '';
  if (window.localStorage.getItem('rememberme') != null) {
    email = localStorage.getItem('userEmail');
  }
  checked = !checked;
  if (checked == true) {
    window.localStorage.setItem('rememberme', true);
  } else {
    window.localStorage.removeItem('rememberme');
    localStorage.removeItem('userEmail');
  }
 
  const rememberme = () => {
    checked = !checked;
    if (checked == true) {
      window.localStorage.setItem('rememberme', true);
    } else {
      window.localStorage.removeItem('rememberme');
      localStorage.removeItem('userEmail');
    }
  };

  return (
    <div className="login-box">
      {showError ? <Alert color="danger">Invalid Email or Password</Alert> : null}
      {loading ? <Loader /> : null}

      <div className="login-box-body">
        <h4 className="login_head">Log In</h4>
        {/* {Alert} */}

        <form onSubmit={handleSubmit(submitForm)}>
          <div className="form-group has-feedback">
            <label htmlFor="phone" className="fl-left">
              Email
            </label>
            <Field
              type="email"
              component="input"
              name="email"
              value={email}
              placeholder="name@example.com"
              className="form-control"
              required
            />

            {/* <input type="email" className="form-control" placeholder="name@example.com" /> */}
          </div>

          <div className="form-group has-feedback">
            <label htmlFor="phone" className="fl-left">
              Password
            </label>
            <Field
              component="input"
              type="password"
              name="password"
              className="form-control"
              placeholder="Type a password here"
              required
            />
          </div>
          <div className="row">
            <div className="col-xs-12 pad_login">
              <Button
                color="green"
                value="Login"
                shape="rectangle"
                position="left"
                className="btn-block"
                icon={null}
                dynamicClassName={'btn-primary'}
              />
            </div>
          </div>
        </form>
        <FormGroup check>
          <Label check>
            <Input type="checkbox" id="rememberMe" name="rememberMe" onChange={rememberme} />
            Remeber me
          </Label>
        </FormGroup>
        <p className="ft_pass">
          <a href="#" className="forgot_pwd">
            forgot password?
          </a>
        </p>
        <p className="dont_className">
          {`Don't have an account?`}
          <Link href="/register">
            <a className="text-center red_color"> Sign up</a>
          </Link>
        </p>
      </div>
      <style>{`

.login-box, .register-box {
  width: 360px;
  margin: 7% auto;
  position:relative;
}
.ft_pass {
  margin-bottom: 5px;
}
.login-box-body, .register-box-body {
  padding: 20px;
  background: #fff;
  border: 1px solid #dddcdc;
  border-radius: 5px;
  min-height: 100%;
  box-shadow: 
}
.login_head {
  font-size: 32px;
  font-weight: bold;
  margin: 0px 0 20px 0;
  color: #333;
  text-align: center;
}
.has-feedback {
  position: relative;
}

.form-group {
  margin-bottom: 15px;
}
.fl-left {
  float: left;
  font-size: 16px;
}
label {
  display: inline-block;
  max-width: 100%;
  margin-bottom: 5px;
  font-weight: 700;
}
.form-control:not(select) {
  -webkit-appearance: none;
  -moz-appearance: none;
  appearance: none;
}
.form-control {
  border-radius: 0;
  box-shadow: none;
  border-color: #d2d6de;
}
.form-control {
  border-radius: 0;
  box-shadow: none;
  border-color: #d2d6de;
}
.col-xs-12 {
  width: 100%;
}

.pad_login
{
  padding:0px 15px;
}

.btn.btn-flat {
  border-radius: 0;
  -webkit-box-shadow: none;
  -moz-box-shadow: none;
  box-shadow: none;
  border-width: 1px;
}
.btn_sign_in {
  background-color: rgba(210, 35, 42, 1.0) !important;
  border-color: rgba(210, 35, 42, 1.0) !important;
  border-radius: 4px !important;
  margin-bottom: 20px;
  padding: 10px;
  font-size: 18px;
  font-weight:700;
}
.btn-primary {
  background-color: #3c8dbc;
  border-color: #367fa9;
}
.btn {
  border-radius: 3px;
  -webkit-box-shadow: none;
  box-shadow: none;
  border: 1px solid transparent;
}
.forgot_pwd {
  margin-bottom: 20px;
  color: #333;
  font-size: 16px;
}
.red_color {
color: #d2232a; 
}
.dont_class {
  font-size: 14px;
}
.dont_class a {
  font-size: 14px;
}
`}</style>
    </div>
  );
}

LoginForm.propTypes = {
  submitForm: PropTypes.func,
  handleSubmit: PropTypes.func,
  loginToken: PropTypes.func,
  data: PropTypes.func,
  token: PropTypes.string,
  error: PropTypes.string,
};

export default reduxForm({ form: 'login' })(LoginForm);
