import React from 'react';
import { faThumbsUp, faStar, faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import PropTypes from 'prop-types';
const onrate = rat => {
  var returnArrya = [];
  for (var i = 0; i < rat; i++) {
    var text = (
      <label key={i} aria-label="1 star" className="rating__label" htmlFor="rating-1">
        <FontAwesomeIcon icon={faStar} width="5" className="rating__icon rating__icon--star" />
      </label>
    );
    returnArrya.push(text);
  }
  return returnArrya;
};
const cssForRating = () => {
  return (
    <style>{`
          .box {
            position: relative;
            border-radius: 3px;
            background: #ffffff;
            border: 1px solid #ececec;
            margin-bottom: 20px;
            width: 100%;
            box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
        }
        .box-header.with-border {
            border-bottom: 1px solid #e9e5e5;
        }
        .box-header {
            color: #444;
            display: block;
            padding: 10px;
            position: relative;
        }
        .box-title {
            color: #333;
            font-weight: 400;
            font-size: 12px;
            line-height: 0;
            word-break: break-word;
        }
        .box-header.box-tools {
            position: absolute;
            right: 10px;
            top: 5px;
        }
        .pull-right {
            float: right!important;
        }
        .rating-group {
            display: inline-flex;
        }
        .rating__label {
            cursor: pointer;
            padding: 0 0.1em;
            font-size: 1rem;
        }
        .rating__input {
            position: absolute !important;
            left: -9999px !important;
        }
        .rating__icon {
            pointer-events: none;
        }
       
        .rating__icon--star {
            color: orange;
        }
        label {
            display: inline-block;
            max-width: 100%;
            margin-bottom: 5px;
            font-weight: 700;
        }
        .box-body {
            border-top-left-radius: 0;
            border-top-right-radius: 0;
            border-bottom-right-radius: 3px;
            border-bottom-left-radius: 3px;
            padding: 10px;
        }
        .box-head {
            margin-top: 5px;
            font-size: 16px;
            color: #c4c2c2;
            font-weight: 600;
        }
        .rating-group {
            display: inline-flex;
        }
        .yeardrop{
          margin-top: -5px !important;
         }
          `}</style>
  );
};
function Rating(props) {
  var { rating, title } = props;
  return (
    <div className="box">
      <div className="box-header with-border">
        <h3 className="box-title">
          <FontAwesomeIcon icon={faThumbsUp} width="12" />
          &nbsp; {title}
        </h3>
        <div className="box-tools pull-right yeardrop">
          <div id="full-stars-example">
            <div className="rating-group">
              <label aria-label="1 star" className="rating__label" htmlFor="rating-1">
                <FontAwesomeIcon icon={faStar} width="5" className="rating__icon rating__icon--star" />
              </label>
              <input className="rating__input" name="rating" id="rating-1" value="1" type="radio" />
              <label aria-label="2 stars" className="rating__label" htmlFor="rating-2">
                <FontAwesomeIcon icon={faStar} width="5" className="rating__icon rating__icon--star" />
              </label>
              <input className="rating__input" name="rating" id="rating-2" value="2" type="radio" />
              <label aria-label="3 stars" className="rating__label" htmlFor="rating-3">
                {' '}
                <FontAwesomeIcon icon={faStar} width="5" className="rating__icon rating__icon--star" />
              </label>
              <input className="rating__input" name="rating" id="rating-3" value="3" type="radio" defaultChecked />
              <label aria-label="4 stars" className="rating__label" htmlFor="rating-4">
                {' '}
                <FontAwesomeIcon icon={faStar} width="5" className="rating__icon rating__icon--star" />
              </label>
              <input className="rating__input" name="rating" id="rating-4" value="4" type="radio" />
              <label aria-label="5 stars" className="rating__label" htmlFor="rating-5">
                {' '}
                <FontAwesomeIcon icon={faStar} width="5" className="rating__icon rating__icon--star" />
              </label>
              <input className="rating__input" name="rating" id="rating-5" value="5" type="radio" />
            </div>
          </div>
        </div>
      </div>
      {rating.map((item, index) => (
        <div className="box-body" id={index} key={item.id}>
          <div className="row">
            <div className="col-md-6">
              <h3 className="box-head">
                <FontAwesomeIcon icon={faInfoCircle} className="black" width="12" />
                &nbsp;{item.title}
              </h3>
            </div>
            <div className="col-md-6 ">
              <div id="full-stars-example ">
                <div className="rating-group pull-right">{onrate(item.rate)}</div>
              </div>
            </div>
          </div>
        </div>
      ))}
      {cssForRating()}
    </div>
  );
}
Rating.propTypes = {
  rating: PropTypes.array,
  title: PropTypes.string,
};
export default Rating;
