import React from 'react';
import Slider from 'react-slick';
const Example = () => {
  const settings = {
    dots: false,
    infinite: true,
    slidesToShow: 8,
    slidesToScroll: 1,
    autoplay: true,
    speed: 3000,
    autoplaySpeed: 3000,
    // cssEase: "linear",
    nextArrow: (
      <div>
        <i className="arrow right right_pr icon_sld slick-arrow"></i>
      </div>
    ),
    prevArrow: (
      <div>
        <i className="arrow left left_pr icon_sld slick-arrow"></i>
      </div>
    ),
  };
  return (
    <div className="product-carousel">
      <Slider {...settings}>
        <div className="product">
          <div className="product-top">
            <a href="#" className="a-tag">
              <div id="box-1" className="top-white-box">
                <span className="box-icon">
                  <img src="/static/images/integrated_circuits.png" className="img-responsive opt_img" />
                </span>
                <strong className="box-title">Integrated Circuits</strong>
              </div>
            </a>
          </div>
        </div>
        <div className="product">
          <div className="product-top">
            <a href="#" className="a-tag">
              <div id="box-1" className="top-white-box">
                <span className="box-icon">
                  <img src="/static/images/discrete_semiconducors.png" className="img-responsive opt_img" />
                </span>
                <strong className="box-title">Discrete Semiconductors</strong>
              </div>
            </a>
          </div>
        </div>
        <div className="product">
          <div className="product-top">
            <a href="#" className="a-tag">
              <div id="box-1" className="top-white-box">
                <span className="box-icon">
                  <img src="/static/images/passive_conductors.png" className="img-responsive opt_img" />
                </span>
                <strong className="box-title">Passive Components</strong>
              </div>
            </a>
          </div>
        </div>
        <div className="product">
          <div className="product-top">
            <a href="#" className="a-tag">
              <div id="box-1" className="top-white-box">
                <span className="box-icon">
                  <img src="/static/images/circuit_protection.png" className="img-responsive opt_img" />
                </span>
                <strong className="box-title">Circuit Protection</strong>
              </div>
            </a>
          </div>
        </div>
        <div className="product">
          <div className="product-top">
            <a href="#" className="a-tag">
              <div id="box-1" className="top-white-box">
                <span className="box-icon">
                  <img src="/static/images/connectors.png" className="img-responsive opt_img" />
                </span>
                <strong className="box-title">Connectors</strong>
              </div>
            </a>
          </div>
        </div>
        <div className="product">
          <div className="product-top">
            <a href="#" className="a-tag">
              <div id="box-1" className="top-white-box">
                <span className="box-icon">
                  <img src="/static/images/sensors.png" className="img-responsive opt_img" />
                </span>
                <strong className="box-title">Sensors</strong>
              </div>
            </a>
          </div>
        </div>
        <div className="product">
          <div className="product-top">
            <a href="#" className="a-tag">
              <div id="box-1" className="top-white-box">
                <span className="box-icon">
                  <img src="/static/images/optoelectronics.png" className="img-responsive opt_img" />
                </span>
                <strong className="box-title">Optoelectronics</strong>
              </div>
            </a>
          </div>
        </div>
        <div className="product">
          <div className="product-top">
            <a href="#" className="a-tag">
              <div id="box-1" className="top-white-box">
                <span className="box-icon">
                  <img src="/static/images/power_products.png" className="img-responsive opt_img" />
                </span>
                <strong className="box-title">Power Products</strong>
              </div>
            </a>
          </div>
        </div>
        <div className="product">
          <div className="product-top">
            <a href="#" className="a-tag">
              <div id="box-1" className="top-white-box">
                <span className="box-icon">
                  <img src="/static/images/electromechanical.png" className="img-responsive opt_img" />
                </span>
                <strong className="box-title">Electromechanical</strong>
              </div>
            </a>
          </div>
        </div>
        <div className="product">
          <div className="product-top">
            <a href="#" className="a-tag">
              <div id="box-1" className="top-white-box">
                <span className="box-icon">
                  <img src="/static/images/cables_wires.png" className="img-responsive opt_img" />
                </span>
                <strong className="box-title">Cables &amp; Wire</strong>
              </div>
            </a>
          </div>
        </div>
      </Slider>

      <style>
        {`
         
        .box-icon img
              {
                height: 50px;
                  width: 50px;
                  margin: 10px auto;
              }
              
          .box-title
            {
                  color: #333;
                font-weight: 400;
                font-size: 11.5px;
                line-height: 0;
                word-break: break-word;
                text-decoration:none;
            }
      
              .top-white-box {
                display: block;
                background-color: #FAFAFA;
                border-radius: 3px;
                width: 100px;
                padding: 7px;
                height: 140px;
                text-align: center;
                box-shadow: 0 15px 45px 0 rgba(0, 0, 0, 0.05);
                transition: all 0.3s ease-in-out 0s;
                border: 1px solid #e1e0e0ba;
                transition: transform .5s ease;
            }
            
          
        .iconPrevious{
          cursor: pointer;
          background: #ccc;
          width: 40px;
          height: 130px;
        }
        .iconNext{
          cursor: pointer;
          background: #ccc;
          width: 40px;
          height: 130px;
        }
        .a-tag:hover{
          text-decoration:none;
      }

      /*new carousel start*/
      .product-carousel-header {
        background: #4a4a4a;
        color: #ffffff;
        box-sizing: border-box;
        font-family: "Open Sans", sans-serif;
        padding: 10px 14px;
        width: 100%;
      }
      .product-carousel {
          background: #ffffff;
          /* border: 1px solid #4a4a4a; */
          box-sizing: border-box;
          /* font-family: "Open Sans", sans-serif; */
          padding: 0px 40px;
          width: 100%;
      }
      .product-carousel .product {
        box-sizing: border-box;
        margin: 0 10px;
        text-align: center;
        display: -webkit-box;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
                flex-flow: column;
        align-content: space-between;
      }
      .product-carousel .product-top {
        width: 100%;
      }
      .product-carousel p,
      .product-carousel .product-image,
      .product-carousel img.review-stars {
        margin: 0 0 10px 0;
      }
      .product-carousel .product-image {
        align-self: flex-start;
        width: 100%;
      }
      img.review-stars {
        width: 100px;
        display: inline-block;
      }
      .product-carousel .product-name {
        font-weight: bold;
        display: -webkit-box;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
                flex-flow: column wrap;
        -webkit-box-pack: start;
                justify-content: flex-start;
        width: 100%;
      }
      .product-carousel .product-bottom {
        margin-top: auto;
        -webkit-box-align: end;
                align-items: flex-end;
      }
      .product-carousel .product-prices {
        display: -webkit-box;
        display: flex;
        -webkit-box-orient: horizontal;
        -webkit-box-direction: normal;
                flex-flow: row wrap;
        -webkit-box-flex: 1;
                flex: 1 0 100%;
        align-self: center;
      }
      .product-carousel .product-prices span {
        width: 100%;
      }
      .product-carousel .product-prices span.price-was {
        color: #a2a2a2;
        text-decoration: line-through;
      }
      .product-carousel .product-prices span.price-was:before {
        content: 'Was ';
      }
      .product-carousel .product-prices span.price-save {
        color: red;
      }
      .product-carousel .product-prices span.price-save:before {
        content: 'Save ';
      }
      .product-carousel .product-prices span.price-now {
        font-weight: bold;
      }
      .product-carousel .product-prices span.price-now:before {
        content: 'Now ';
      }
      .product-carousel button.shop-now {
        border: none;
        background-image: none;
        background-color: #4a4a4a;
        color: #ffffff;
        box-shadow: none;
        -webkit-box-shadow: none;
        -moz-box-shadow: none;
        font-size: 16px;
        padding: 8px 10px;
        align-self: center;
      }
      /** ARROWS **/
      .icon_sld {
        border: solid #000000;
        border-width: 0 2px 2px 0;
        display: inline-block;
        padding: 12px;
        position: absolute;
        top: calc(50% - 12px/2);
        cursor: pointer;
      }
      .right_pr {
        right: -20px;
        transform: rotate(-45deg);
        -webkit-transform: rotate(-45deg);
      }
      .left_pr {
        left: -20px;
        transform: rotate(135deg);
        -webkit-transform: rotate(135deg);
      }
      /** SLICK SLIDER CSS **/
      /* Slider */
      .slick-slider {
        position: relative;
        box-sizing: border-box;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        -webkit-touch-callout: none;
        -khtml-user-select: none;
        touch-action: pan-y;
        -webkit-tap-highlight-color: transparent;
      }
      .slick-list {
        position: relative;
        overflow: hidden;
        margin: 0;
        padding: 0;
      }
      .slick-list:focus {
        outline: none;
      }
      .slick-list.dragging {
        cursor: pointer;
        cursor: hand;
      }
      .slick-slider .slick-track,
      .slick-slider .slick-list {
        -webkit-transform: translate3d(0, 0, 0);
        transform: translate3d(0, 0, 0);
      }
      .slick-track {
        display: -webkit-box;
        display: flex;
        position: relative;
        top: 0;
        left: 0;
      }
      .slick-loading .slick-track {
        visibility: hidden;
      }
      [dir='rtl'] .slick-slide {
        float: right;
      }
      .slick-slide.slick-loading img {
        display: none;
      }
      .slick-slide.dragging img {
        pointer-events: none;
      }
      .slick-loading .slick-slide {
        visibility: hidden;
      }
      .slick-vertical .slick-slide {
        height: auto;
        border: 1px solid transparent;
      }
      .slick-arrow.slick-hidden {
        display: none;
      }
      .pt-50
      {
        padding-top:50px;
      }
      /*new carousel end*/
      

`}
      </style>
    </div>
  );
};

export default Example;
