import React, { useState } from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

export function FtpModal(props) {
    const { open, modaldata } = props;

    const [modal, setModal] = useState(open);

    const toggle = () => setModal(!modal);

    return (
        <div>
        <Modal isOpen={modal} toggle={toggle} className="">
          <ModalHeader toggle={toggle}>{modaldata.title}</ModalHeader>
          <ModalBody>
            {modaldata.content}
          </ModalBody>
          <ModalFooter>
            <Button color={modaldata.btnColor} onClick={toggle}>{modaldata.btnTitle}</Button>{' '}
          </ModalFooter>
        </Modal>
      </div>
    );
}

export default FtpModal;