import React from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export function H2(props) {
    const { data, icon, position } = props;
    
    return (
        <React.Fragment>
        
            <h2>
                { position === 'left' ?  <FontAwesomeIcon icon={icon} width="16" /> : '' }
                
                { data }

                { position === 'right' ?  <FontAwesomeIcon icon={icon} width="16" /> : '' }
            </h2>
            
            <style>{`
                h2 {
                    font-size: 21px;
                    text-transform: capitalize;
                    color: #333;
                    margin-bottom: 20px;
                    letter-spacing: 1px;
                    text-align: left;
                    font-weight: bold;
                }
            `}</style>
        </React.Fragment>
    );
}

export default H2;