import React, { Component } from 'react';
import H5 from 'components/Unused/H5'
import Rating from 'components/Unused/Rating'

const withBox = (WrappedComponent) => {
  class HOC extends Component {
    
    constructor(props) {
        super(props)
    }

    render() {
        const { heading, rating, icon } = this.props.data

        return (
        <div className="box">
            <div className="box-header with-border">
                {/* Left */}
                <H5 data={heading} icon={icon} position="left" />
                {/* Right side conditions => BUTTON / BLANK / DROPDOWN / RATING */}
                <div className="box-tools pull-right mt-10">
                    { rating ? <Rating rating={rating} /> : "" }
                </div>
            </div>
            <div className="box-body">
                <div className="row">
                    {/* Select type of box loop */}
                    <WrappedComponent data={this.props} />          
                </div>
            </div>

            <style>{`
                .box {
                    position: relative;
                    border-radius: 3px;
                    background: #ffffff;
                    border: 1px solid #ececec;
                    margin-bottom: 20px;
                    width: 100%;
                    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
                }

                .box-header.with-border {
                    border-bottom: 1px solid #e9e5e5;
                }

                .box-header {
                    color: #444;
                    display: block;
                    padding: 10px;
                    position: relative;
                }
                
                .box-header>.box-tools {
                    position: absolute;
                    right: 10px;
                    top: 5px;
                }
                
                .mt-10 {
                    margin-top: 10px !important;
                }
                
                .rating-group {
                    display: inline-flex;
                }
                
                .box-body {
                    border-top-left-radius: 0;
                    border-top-right-radius: 0;
                    border-bottom-right-radius: 3px;
                    border-bottom-left-radius: 3px;
                    padding: 10px;
                }
            `}</style>
        </div>    
      );
    }
  }
    
  return HOC;
};

export default withBox;