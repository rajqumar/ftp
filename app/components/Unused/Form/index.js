import React from 'react'
import { reduxForm, Field } from 'redux-form'

const required = value => value ? undefined : 'Required'
const maxLength = max => value =>
  value && value.length > max ? `Must be ${max} characters or less` : undefined
const maxLength15 = maxLength(15)
const email = value =>
value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value) ?
'Invalid email address' : undefined
  
const newField = ({
	input,
	type,
	placeholder,
	id,
	meta: { touched, error },
	...rest
	}) => {
	return (
		<div>
		<input {...input} placeholder={placeholder} type={type} id={id} />
		{touched && error && <p style={{ color: 'red' }}>{error}</p>}
		</div>
	);
};

const Form = props => {

  const { formElements, formName, handleSubmit, pristine, reset, submitting } = props
 
  return (
    <div>
    	<form onSubmit={handleSubmit}>
			{
				formElements.map((el, i) => (
					<React.Fragment key={i}>
						<div className="form-group">
						<label htmlFor={el.label}>{el.label}</label>
			
						<Field
							component={newField}
							type={el.type}
							name={el.name}
							id={el.id}
							className="form-control"
							placeholder={el.placeholder}
							// value={el.value}
							validate={el.validation.map(elv=>elv = eval(elv))}
						/>
						</div>
					</React.Fragment>
				))
			}
            <div>
                <button type="submit">Submit</button>
            </div>
    	</form>
		<style>{`
			label {
				font-size: 16px;
			}

			input {
				display: block;
				width: 100%;
				height: 34px;
				padding: 6px 12px;
				font-size: 14px;
				line-height: 1.42857143;
				color: #555;
				margin-bottom: 20px;
				background-color: #fff;
				background-image: none;
				border: 1px solid #ccc;
				border-radius: 4px;
				transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
			}
		`}</style>
    </div>
  )
}

export default reduxForm({form: 'ftp_form'})(Form)
