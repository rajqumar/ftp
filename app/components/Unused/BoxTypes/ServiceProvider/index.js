import React from "react";
import H5 from 'components/Unused/H5'
import withBox from 'components/Unused/Box'

export function ServiceProvider(props) {
    const serviceproviders = props.data.data;

    return (
        <React.Fragment>
            {
                serviceproviders.map((sp, i) => (
                    <div className="col-md-3">
                        <div className="gift_img_first" style={{ backgroundImage: `url(${sp.image})`}}>
                            <span className="h5_text">
                            <H5 data={sp.title} />
                            </span>
                        </div>
                    </div>
                ))
            }

            <style>{`
                .gift_img_first {
                    background-size: 100% 100%;
                    background-position: 50% 50%;
                    background-repeat: no-repeat;
                    min-height: 120px;
                }

                .h5_text {
                    position: absolute;
                    top: 33%;
                    left: 32%;                    
                }
            `}</style>
        </React.Fragment>
    );
}

export default withBox(ServiceProvider);