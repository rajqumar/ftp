import React from "react";
import H6 from 'components/Unused/H6'
import withBox from 'components/Unused/Box'


export function Profile (props) {
    // check if props data is array or simple string 
    // rendering condition based on the type of data
    const profileData = [
        {
            title: 'name',
            value: 'ACME INC'
        },
        {
            title: 'phone',
            value: '+65 65432109'
        },
        {
            title: 'email',
            value: 'sale@example.com'
        },
        {
            title: 'country',
            value: 'Singapore'
        },
        {
            title: 'country',
            value: 'Singapore'
        }
    ]

    return (
        <React.Fragment>
            { profileData.map((profile, i) => (
                <div className="col-md-6" key={i}>
                    <H6 data={profile.title} />
                    <p className="part_subhead">{profile.value}
                    </p>
                </div>
            ))}
            
            <style>{`
                .part_subhead {
                    font-size: 14px;
                    text-align: left;                        
                }
            `}</style>
        </React.Fragment>
    )
}

export default withBox(Profile);