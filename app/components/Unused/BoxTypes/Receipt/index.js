import React from "react";
import H6 from 'components/Unused/H6'
import withBox from 'components/Unused/Box'

export function Receipt(props) {
    const receipt = props.data.data;
    return (
        <div>
            <div class="row">
                <div class="col-md-6">
                    <H6 data="Payment Terms" />
                    <p>{receipt.terms}</p>
                </div>
    
                <div class="col-md-6">
                    <H6 data="Validity" />
                    <p>{receipt.validity}</p>
                </div>
            </div>
            <div class="cur_block">

            <div class="row">
                <div class="col-md-6">
                    <p>Currency</p>
                </div>
                <div class="col-md-6">
                    <p>{receipt.currency}</p>
                </div>
                <div class="col-md-6">
                    <p>Amount Payable</p>
                </div>
                <div class="col-md-6">
                    <p>{receipt.amount}</p>
                </div>
                <div class="col-md-6">
                    <p>Tax Payable</p>
                </div>
                <div class="col-md-6">
                    <p>{receipt.tax}</p>
                </div>
                <div class="col-md-6">
                    <p><strong>Total Amount</strong></p>
                </div>
                <div class="col-md-6">
                    <p>{receipt.total}</p>
                </div>
            </div>

            </div>            
            <style>{`
                p {
                    font-size: 14px;
                    text-align: left;
                }

                .cur_block {
                    border: 1px solid #ccc;
                    border-radius: 5px;
                    padding: 20px;
                }
            `}</style>
        </div>
    );
}

export default withBox(Receipt);
