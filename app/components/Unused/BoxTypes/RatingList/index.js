import React from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Rating from 'components/Unused/Rating'
import withBox from 'components/Unused/Box'

export function RatingList(props) {
    
    const ratings = props.data.data;
    console.log('*******', ratings);

    return (
        <React.Fragment>
            {
                ratings.map((rating, i) => (
                    <div className="row">
                        <div className="col-md-6">
                            <FontAwesomeIcon icon={rating.icon} width="16" /> 
                            <h3>{rating.title}</h3>
                        </div>
                        <div className="col-md-6 ">
                            <div id="full-stars-example ">
                                <div className="rating-group pull-right">
                                   <Rating rating={rating.rating} />
                                </div>
                            </div>     
                        </div>
                    </div>                     
                ))
            }
            <style>{`
                h3 {
                    margin-top: 5px;
                    font-size: 16px;
                    color: #c4c2c2;
                    font-weight: 600;
                }
            `}</style>
        </React.Fragment>
    );
}

export default withBox(RatingList);