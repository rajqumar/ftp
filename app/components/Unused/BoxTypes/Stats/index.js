import React from "react";
import withBox from 'components/Unused/Box'

export function Stats(props) {
    const statsdata = props.data.data;

    return (
        <React.Fragment>
            {
                statsdata.map((stats, i) => (
                    <div className="col-md-4" key={i}>
                        <center>
                            <div>
                                <p className="quote_side">{stats.count}</p>
                                <p>{stats.title}</p>
                            </div>
                        </center>
                    </div>
                ))
            }

            <style>{`
                p {
                    font-size: 14px;
                    font-weight: 500;
                }

                .quote_side {
                    margin-bottom: 0px;
                    font-size: 34px;
                }
            `}</style>
        </React.Fragment>
    );
}

export default withBox(Stats);