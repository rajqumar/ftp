import React from "react";
import H6 from 'components/Unused/H6'
import Form from 'components/Unused/Form'
import withBox from 'components/Unused/Box'
// import { loginRequest } from 'actions/login'

export function BoxForm (props) {
    var form = props.data.data[0].form
    
    // const { form } = props.data
    const loginRequest = () => {
        console.log('BOX FORM Submitted !!')
    }

    return (
        <div className="col-md-4">
            <Form formElements={form} onSubmit={loginRequest} />
        </div>
    )
}

export default withBox(BoxForm);