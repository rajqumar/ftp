import React from "react";
import withBox from 'components/Unused/Box'
var BarChart = require('react-d3-components').BarChart;

export function Graph(props) {
    const dataToSheet = props.data.data;
    const tooltipScatter = (x, y0, y) => y

    return (
        <div id="app">
            <BarChart
              tooltipHtml={tooltipScatter}
              data={dataToSheet}
              width={700}
              height={500}
              yAxis={[0, 100]}
              margin={{ top: 10, bottom: 50, left: 50, right: 10 }}
            />
            <style>{`
                #app {
                    width: 100%;
                    height: 100%;
                }
                
                .bar {    
                    fill: #37B300;
                    stroke: #37B300;
                }
                
                .tooltip {
                    padding: 3px;
                    border: 2px solid;
                    border-radius: 2px;
                    background-color: red;
                    opacity: 0.6;
                    justify-content: center;
                    align-items: center;
                }
            `}</style>
        </div>
    );
}

export default withBox(Graph);