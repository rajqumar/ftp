// call inside flex-container div
import React from "react";
import Link from 'next/link';

export function Statistics(props) {
    const { count, title, link } = props;

    return (
        <React.Fragment>
            <div className="box">
                <div className="box_block">
                    <div className="box_purchase">
                        <p className="count">{count}</p>
                        <p className="title">{title}</p>
                    </div>
                    <Link href={link}>
                        <p className="link">view</p>
                    </Link>
                </div>
            </div>
            <style>{`
                .box {
                    background-color: #ffffff;
                    border: 1px solid #ccc;
                    border-radius: 5px;
                    margin: 10px;
                    text-align: center;
                    width: 140px;
                    font-size: 30px;
                }

                .box_purchase {
                    padding-top: 15px;
                    padding-bottom: 15px;
                }

                .count {
                    font-size:32px;
                    margin-bottom:0px;
                }

                .title {
                    font-size:12px;
                }

                .link {
                    border-top: 1px solid #ccc;
                    font-size: 16px;
                    margin-bottom: 0px;
                    padding: 10px 0;
                    color: #d2232a;
                    text-transform: uppercase;
                    font-weight: 700;
                }

                .a-tag:hover {
                    text-decoration: none;
                }
            `}</style>
        </React.Fragment>
    );
}

export default Statistics;