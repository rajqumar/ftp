import React from "react";

export function Advertisement(props) {
    const { data, icon, position } = props;
    
    return (
        <div class="advertisement">
            <div class="centered">Advertisement</div>
            <img src="static/images/partslogged-outsearch-rectangle.png" class="img-responsive" />

            <style>{`
                .advertisement {
                    margin: 20px 0;
                }

                .centered {
                    position: absolute;
                    top: 50%;
                    left: 50%;
                    transform: translate(-50%, -50%);
                }
            `}</style>
        </div>
    );
}

export default Advertisement;