import React from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export function H5(props) {
    const { data, icon, position } = props;
    
    return (
        <React.Fragment>
            <h5>
                { position === 'left' ?  <FontAwesomeIcon icon={icon} width="16" /> : '' }
                
                { data }

                { position === 'right' ?  <FontAwesomeIcon icon={icon} width="16" /> : '' }
            </h5>
            
            <style>{`
                h5 {
                    text-align: left !important;
                    font-size: 16px;
                    font-weight: 700;
                }
            `}</style>
        </React.Fragment>
    );
}

export default H5;