import React from 'react';
import { faUser } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import PropTypes from 'prop-types';

const CreateAdminForm = props => {
  const { title } = props;
  console.log(title, 'creatadmin componentas');
  return (
    <div className="container mt-9">
      <section className="content">
        <div className="row">
          <div className="col-md-6 col-md-offset-3 reg_form_new">
            <form id="msform">
              <h5 className="register_main_head">{title}</h5>
              <fieldset>
                <h2 className="fs-title">
                  <FontAwesomeIcon icon={faUser} width="12" />
                  Basic Information
                </h2>
                <div className="row">
                  <div className="col-md-6">
                    <div className="form-group">
                      <label htmlFor="name" className="fl-left">
                        Name
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        id="name"
                        name="name"
                        placeholder="Shrishailya Deshmukh"
                      />
                    </div>
                    <div className="form-group">
                      <label htmlFor="Title" className="fl-left">
                        Title
                      </label>
                      <input type="text" className="form-control" id="Title" name="Title" placeholder="Director" />
                    </div>
                    <div className="form-group">
                      <label htmlFor="confirm_password" className="fl-left">
                        New Password
                      </label>
                      <input
                        type="password"
                        className="form-control"
                        id="new_password"
                        name="new_password"
                        placeholder="Leave blank if unchanged"
                      />
                    </div>
                    <div className="form-group">
                      <label htmlFor="confirm_password" className="fl-left">
                        Task
                      </label>
                      <div className="clearfix"></div>
                      <div className="bootstrap-demo fl-left mb-15">
                        <label className="checkbox-inline">
                          <input type="checkbox" id="inlineCheckbox1" value="option1" className="inline_check_issue" />{' '}
                          Buy
                        </label>
                        <label className="checkbox-inline">
                          <input type="checkbox" id="inlineCheckbox2" value="option2" className="inline_check_issue" />{' '}
                          Sell
                        </label>
                      </div>
                    </div>
                    <div className="clearfix"></div>
                    <div className="form-group">
                      <label htmlFor="Title" className="fl-left">
                        Role
                      </label>
                      <select className="form-control select2">
                        <option value="default">Select Role</option>
                        <option>IT Manager</option>
                        <option>Procurement Manager</option>
                        <option>Procurement Lead</option>
                        <option>Executive Assistant</option>
                      </select>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="form-group">
                      <label htmlFor="Email" className="fl-left">
                        Email
                      </label>
                      <input
                        type="email"
                        className="form-control"
                        id="Email"
                        name="Email"
                        placeholder="shri@hellosme.com"
                      />
                    </div>
                    <div className="form-group">
                      <label htmlFor="phone" className="fl-left">
                        Phone
                      </label>
                      <input type="number" className="form-control" id="phone" name="phone" placeholder="6567891234" />
                    </div>
                    <div className="form-group">
                      <label htmlFor="confirm_password" className="fl-left">
                        Confirm Password
                      </label>
                      <input
                        type="password"
                        className="form-control"
                        id="confirm_password"
                        name="confirm_password"
                        placeholder="Leave blank if unchanged"
                      />
                    </div>

                    <div className="form-group">
                      <label htmlFor="confirm_password" className="fl-left">
                        Status
                      </label>
                      <div className="clearfix"></div>
                      <div className="bootstrap-demo fl-left mb-15">
                        <label className="checkbox-inline">
                          <input type="radio" id="inlineCheckbox1" value="option1" className="inline_radio_issue" />{' '}
                          Active
                        </label>

                        <label className="checkbox-inline">
                          <input type="radio" id="inlineCheckbox2" value="option2" className="inline_radio_issue" />{' '}
                          Inactive
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
                <center>
                  <button type="button" className="btn btn-success save_changes">
                    Save changes
                  </button>
                </center>
              </fieldset>
            </form>
          </div>
        </div>
      </section>
      <style>{`
  
    .col-md-offset-3 {
      margin-left: 25%;
    }
    col-md-6 {
      width: 50%;
    }
    .mt-9{
      margin-top:9%;
     }
    .content {
      min-height: 250px;
      padding: 15px;
      margin-right: auto;
      margin-left: auto;
      padding-left: 15px;
      padding-right: 15px;
  }
  .reg_form_new {
    background: #fff;
    border: 1px solid #dddcdc;
    border-radius: 5px;
    min-height: 100%;
    box-shadow: 0 0 15px 1px rgba(0, 0, 0, 0.07);
  }
  #msform {
    text-align: center;
    position: relative;
}
.register_main_head {
  font-size: 32px;
  font-weight: bold;
  margin: 25px auto;
}
#msform fieldset {
  border: 0 none;
  border-radius: 0px;
  padding: 20px 30px;
  transform: unset !important;
  box-sizing: border-box;
  position: relative !important;
}
.fs-title {
  font-size: 21px;
  text-transform: capitalize;
  color: #333;
  margin-bottom: 20px;
  letter-spacing: 1px;
  text-align: left;
  font-weight: bold;
}
.fl-left {
  float: left;
  font-size: 16px;
}
#msform input, #msform textarea {
  display: block;
  width: 100%;
  height: 34px;
  padding: 6px 12px;
  font-size: 14px;
  line-height: 1.42857143;
  color: #555;
  margin-bottom: 20px;
  background-color: #fff;
  background-image: none;
  border: 1px solid #ccc;
  border-radius: 4px;
  -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
  box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
  -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
  -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
  transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
}
.save_changes {
  background-color: #D2232A;
  border: 1px solid #D2232A;
  margin-top: 25px;
  text-transform: uppercase;
  font-size: 14px;
}
.fl-left {
  float: left;
  font-size: 16px;
}

.checkbox-inline, .radio-inline {
  position: relative;
  display: inline-block;
  padding-left: 20px;
  margin-bottom: 0;
  font-weight: 400;
  vertical-align: middle;
  cursor: pointer;
}
.checkbox-inline, .radio-inline {
  position: relative;
  display: inline-block;
  padding-left: 20px;
  margin-bottom: 0;
  font-weight: 400;
  vertical-align: middle;
  cursor: pointer;
}
label {
  display: inline-block;
  max-width: 100%;
  margin-bottom: 5px;
  font-weight: 700;
}
.inline_check_issue
{
	height:auto !Important;
	width:auto !important;
}
.inline_radio_issue
{
	height:auto !Important;
	width:auto !important;
	position: absolute;
    margin-top: 4px;
    margin-left: -20px !important;
}
.select2-container--default .select2-selection--single {
  background-color: #fff;
  border: 1px solid #ccc;
  border-radius: 4px;
}
.mb-15 {
  margin-bottom: 15px;
}
.select2-container .select2-selection--single {
  box-sizing: border-box;
  cursor: pointer;
  display: block;
  height: 34px;
  user-select: none;
  -webkit-user-select: none;
}
.select2-container--default .select2-selection--single, .select2-selection .select2-selection--single {
  border: 1px solid #d2d6de;
  border-radius: 0;
  padding: 6px 12px;
  height: 34px;
}



  `}</style>
    </div>
  );
};
CreateAdminForm.propTypes = {
  title: PropTypes.string,
};
export default CreateAdminForm;
