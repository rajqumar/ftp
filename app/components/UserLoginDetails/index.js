import React from 'react';
import Tables from 'components/Tables';
import PropTypes from 'prop-types';

function UserLoginDetails(props) {
  var { options, userLoginData, columns } = props;

  return (
    <div className="create_ueser">
      <div className="container">
        <button className="btn btn-success create_user_new" type="button">
          CREATE USER
        </button>
        <div className="box box-widget">
          <div className="box-body">
            <Tables data={userLoginData} columns={columns} options={options} />
          </div>
        </div>
      </div>
      <style>{`
    .mob_icon {
        font-size: 24px;
        margin-right: 5px;
        color: #D2232A;
    }
    .create_user_new {
        background: #6DC144;
        border: 1px solid #6DC144;
        padding: 10px 60px;
        margin-bottom: 20px;
    }
    .mail_icon {
        font-size: 20px;
        color: #D2232A;
    }
    .dropdown-menu {
        box-shadow: none;
        border-color: #eee;
    }
    .btn-success {
        background-color: #00a65a;
        border-color: #008d4c;
    }
    .btn {
        border-radius: 3px;
        -webkit-box-shadow: none;
        box-shadow: none;
        border: 1px solid transparent;
    }
    .btn_inactive {
        background-color: #D31E25;
        border: 1px solid #D31E25;
        border-radius: 50px;
        font-size: 14px;
        padding: 0px 15px;
    }
    .btn_active {
        background-color: #6DC144;
        border: 1px solid #6DC144;
        border-radius: 50px;
        font-size: 14px;
        padding: 0px 20px;
    }
    
    `}</style>
    </div>
  );
}
UserLoginDetails.propTypes = {
  columns: PropTypes.array,
  userLoginData: PropTypes.array,
  options: PropTypes.object,
};
export default UserLoginDetails;
