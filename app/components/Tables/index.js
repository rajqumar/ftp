import React from 'react';
import MUIDataTable from 'mui-datatables';
import PropTypes from 'prop-types';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

const cssForTable = () => {
  return (
    <style>{`
  .table-responsive {
    min-height: .01%;
    overflow-x: auto;
  }
  .MuiIconButton-label{
    display:none;
  }
  .btn_inactive {
    background-color: #D31E25 !important;
    border: 1px solid #D31E25 !important;
    border-radius: 50px !important;
    font-size: 14px;
    padding: 0px 15px;
}
.btn_active
{
      background-color: #6DC144 !important;
    border: 1px solid #6DC144 !important;
    border-radius: 50px !important;
    font-size: 14px;
    padding: 0px 20px;
}
.btn-success {
    background-color: #00a65a;
    border-color: #008d4c;
}

.btn {
    border-radius: 3px;
    -webkit-box-shadow: none;
    box-shadow: none;
    border: 1px solid transparent;
}
.red_name{
  color:red;
}
.mail_icon {
  font-size: 20px;
  color: #D2232A;
}
.mob_icon {
  font-size: 24px;
  margin-right: 5px;
  color: #D2232A;
}

  `}</style>
  );
};

function Tables(props) {
  var { data, columns, options } = props;
  const getMuiTheme = () =>
    createMuiTheme({
      overrides: {
        MUIDataTable: {
          root: {},
          paper: {
            boxShadow: 'none',
          },
        },
        MUIDataTableBodyRow: {
          root: {
            '&:nth-child(odd)': {
              backgroundColor: '#dee2e6',
            },
          },
        },
        MUIDataTableBodyCell: {},
      },
    });
  return (
    <div className="table-responsive">
      <MuiThemeProvider theme={getMuiTheme()}>
        <MUIDataTable title={''} data={data} columns={columns} options={options} />
      </MuiThemeProvider>
      {cssForTable()}
    </div>
  );
}
Tables.propTypes = {
  data: PropTypes.array,
  columns: PropTypes.array,
  options: PropTypes.object,
};
export default Tables;
