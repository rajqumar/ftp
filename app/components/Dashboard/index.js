import React from 'react';
import Statistics from 'components/Statistics';
import Rating from 'components/Rating';
import YearToDate from 'components/YearToDate';
import UserLoginDetails from 'components/UserLoginDetails';
import CustomBarChart from 'components/CustomBarChart';
import { faChartBar, faMinus, faTimes, faCalendar, faBolt } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import PropTypes from 'prop-types';
const cssForDashboard = () => {
  return (
    <style>{`
      .content {
        min-height: 250px;
        padding: 15px;
        margin-right: auto;
        margin-left: auto;
        padding-left: 15px;
        padding-right: 15px;
    }
    .mt-10 {
      margin-top: 8%;
  }
    .img-responsive {
      display: block;
      max-width: 100%;
      height: auto;
      } 
   
    .col-md-6 {
    -ms-flex: 0 0 50%;
    flex: 0 0 50%;
    max-width: 50%;
    }
    .form-control {
    box-shadow: none;
    border-color: #d2d6de;
    background-color: #fff;
    border: 1px solid #ccc;
    border-radius: 4px;
    }
    .col-md-8 {
    width: 66.66666667%;
    }
   .pull-right {
    float: right!important;
    }
    h3 {
    display: block;
    font-size: 1.17em;
    margin-block-start: 1em;
    margin-block-end: 1em;
    margin-inline-start: 0px;
    margin-inline-end: 0px;
    font-weight: bold;
    }

    #app {
    width: 100%;
    height: 100%;
    }
  label {
    display: inline-block;
    max-width: 100%;
    margin-bottom: 5px;
    font-weight: 700;
    }
    
    center {
    display: block;
    text-align: -webkit-center;
    }
  `}</style>
  );
};

const DashboardForm = props => {
  var { PlatformUtilisation, datasheet, YearToDateDummy, rating, data, options, userLoginData, columns } = props;

  return (
    <div className="mt-10">
      <div className="container">
        <section className="content">
          <Statistics data={data} title="Buyer Statistics" dynamicClass="dash_head" />
          <Statistics data={data} title="Seller Statistics" dynamicClass="dash_head" />
        </section>
      </div>
      <div className="block_all">
        <div className="container">
          <div className="row">
            <div className="col-md-8">
              <CustomBarChart dataToSheet={datasheet} />
              <div className="box">
                <div className="box-header with-border">
                  <h3 className="box-title">
                    <FontAwesomeIcon icon={faChartBar} width="12" />
                    &nbsp; Sales Orders
                  </h3>
                  <div className="box-tools pull-right">
                    <button type="button" className="btn btn-box-tool" data-widget="collapse">
                      <FontAwesomeIcon icon={faMinus} width="12" className="" />
                    </button>
                    <button type="button" className="btn btn-box-tool" data-widget="remove">
                      <FontAwesomeIcon icon={faTimes} width="12" />
                    </button>
                  </div>
                </div>
                <div className="box-body">
                  <div id="app"></div>
                </div>
              </div>
            </div>
            <div className="col-md-4">
              <Rating rating={rating} title="Seller Rating" />
              <YearToDate data={YearToDateDummy} icon={faCalendar} title="Year to Date" dynamicClass="col-md-4" />
              <YearToDate
                data={PlatformUtilisation}
                icon={faBolt}
                title="Platform Utilisation"
                dynamicClass="col-md-12"
              />
            </div>
          </div>
        </div>
        <UserLoginDetails userLoginData={userLoginData} columns={columns} options={options} />
      </div>
      {cssForDashboard()}
    </div>
  );
};
DashboardForm.propTypes = {
  rating: PropTypes.array,
  PlatformUtilisation: PropTypes.array,
  datasheet: PropTypes.array,
  YearToDateDummy: PropTypes.array,
  data: PropTypes.array,
  columns: PropTypes.array,
  userLoginData: PropTypes.array,
  options: PropTypes.object,
};
export default DashboardForm;
