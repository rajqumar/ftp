import React from 'react';
import { faBolt } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
const IntegrateERPForm = () => {
  return (
    <div className="container mt-8">
      <section className="content">
        <div className="row">
          <h1 className="integrate_erp_head">Integrate Your ERP</h1>
          <div className="clearfix"></div>
          <div className="welcome_box">
            <h4>Welcome onboard, John!</h4>
            <p>To get Started, please link your ERP system to our system so that we can fetch the data over.</p>
          </div>
          <div className="choose_btn_section">
            <h4>1.Choose Your ERP System</h4>
            <ul className="btn_group_erp">
              <li>
                <a href="#">
                  <button type="button" className="btn btn-default btn_list_erp">
                    SAP HANA
                  </button>
                </a>
              </li>

              <li>
                <a href="#">
                  <button type="button" className="btn btn-default btn_list_erp active_erp">
                    ORACLE ERP CLOUD
                  </button>
                </a>
              </li>

              <li>
                <a href="#">
                  <button type="button" className="btn btn-default btn_list_erp">
                    MICROSOFT DYNAMICS
                  </button>
                </a>
              </li>

              <li>
                <a href="#">
                  <button type="button" className="btn btn-default btn_list_erp">
                    ERP 4
                  </button>
                </a>
              </li>

              <li>
                <a href="#">
                  <button type="button" className="btn btn-default btn_list_erp">
                    ERP 5
                  </button>
                </a>
              </li>
            </ul>

            <h4>2.ERP System Details</h4>
            <div className="row">
              <div className="col-md-6">
                <div className="box box-widget">
                  <div className="box-header with-border">
                    <h5 className="head_test">
                      <FontAwesomeIcon icon={faBolt} />
                      &nbsp;&nbsp;&nbsp; Testing
                    </h5>
                  </div>
                  <div className="box-body">
                    <form id="msform">
                      <fieldset className="padding_form">
                        <div className="form-group">
                          <label htmlFor="name" className="fl-left">
                            API Token
                          </label>
                          <input
                            type="text"
                            className="form-control"
                            id="name"
                            name="name"
                            placeholder="HIVE9UW739FKS"
                          />
                        </div>
                        <div className="form-group">
                          <label htmlFor="name" className="fl-left">
                            Password
                          </label>
                          <input
                            type="text"
                            className="form-control"
                            id="name"
                            name="name"
                            placeholder="Type password here"
                          />
                        </div>
                        <div className="form-group">
                          <label htmlFor="name" className="fl-left">
                            URL
                          </label>
                          <input
                            type="text"
                            className="form-control"
                            id="name"
                            name="name"
                            placeholder="Type URL Here"
                          />
                        </div>
                      </fieldset>
                    </form>
                  </div>
                </div>
              </div>

              <div className="col-md-6">
                <div className="box box-widget">
                  <div className="box-header with-border">
                    <h5 className="head_test">
                      <FontAwesomeIcon icon={faBolt} />
                      &nbsp;&nbsp;&nbsp; Production
                    </h5>
                  </div>
                  <div className="box-body">
                    <form id="msform">
                      <fieldset className="padding_form">
                        <div className="form-group">
                          <label htmlFor="name" className="fl-left">
                            API Token
                          </label>
                          <input
                            type="text"
                            className="form-control"
                            id="name"
                            name="name"
                            placeholder="HIVE9UW739FKS"
                          />
                        </div>
                        <div className="form-group">
                          <label htmlFor="name" className="fl-left">
                            Password
                          </label>
                          <input
                            type="text"
                            className="form-control"
                            id="name"
                            name="name"
                            placeholder="Type password here"
                          />
                        </div>
                        <div className="form-group">
                          <label htmlFor="name" className="fl-left">
                            URL
                          </label>
                          <input
                            type="text"
                            className="form-control"
                            id="name"
                            name="name"
                            placeholder="Type URL here"
                          />
                        </div>
                      </fieldset>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <style>{`
      .content {
        min-height: 250px;
        padding: 15px;
        margin-right: auto;
        margin-left: auto;
        padding-left: 15px;
        padding-right: 15px;
    }
          .integrate_erp_head {
            float: left;
            font-size: 32px;
            color: #000;
            margin-bottom: 30px;
            font-weight: 700;
            position:absolute;
        }
        .welcome_box {
          background-color: #fff;
          padding: 15px 35px;
          border-left: 5px solid #FFC000;
          margin-top:70px;
      }
      .choose_btn_section h4 {
        margin: 20px 0;
        color: #333;
        font-weight: 700;
        font-size: 20px;
      }
      .btn_group_erp {
        list-style: none;
        margin-left: -40px;
        margin-bottom: 20px;
      }
      .btn_group_erp li {
        display: inline-block;
        margin-right: 40px;
    }
    .col-md-6 {
      width: 50%;
  }
  .mt-8 {
    margin-top: 9%;
}
    .btn-default {
        background-color: #f4f4f4;
        color: #444;
        border-color: #ddd;
    }
    .btn {
        border-radius: 3px;
        -webkit-box-shadow: none;
        box-shadow: none;
        border: 1px solid transparent;
    }
    .active_erp {
      background-color: #d2232a !important;
      border: 1px solid #d2232a !important;
      color: #fff !important;
  }
  .btn-default:hover, .btn-default:active, .btn-default.hover {
    background-color: #e7e7e7 !important;
}
  .btn_list_erp {
      width: 170px;
      background-color: #fff;
      font-weight: 700;
      padding: 10px;
  }
  .btn-default {
      background-color: #f4f4f4;
      color: #444;
      border-color: #ddd;
  }
  .btn {
      border-radius: 3px;
      -webkit-box-shadow: none;
      box-shadow: none;
      border: 1px solid transparent;
  }
  .box-widget {
    border: 1px solid #e1dede;
    position: relative;
}
.box {
    position: relative;
    border-radius: 3px;
    background: #ffffff;
    border: 1px solid #ececec;
    margin-bottom: 20px;
    width: 100%;
    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
}
.box-header.with-border {
  border-bottom: 1px solid #e9e5e5;
}

.box-header {
  color: #444;
  display: block;
  padding: 10px;
  position: relative;
}
.head_test {
  font-size: 16px;
  font-weight: 700;
}
.box-body {
  border-top-left-radius: 0;
  border-top-right-radius: 0;
  border-bottom-right-radius: 3px;
  border-bottom-left-radius: 3px;
  padding: 10px;
}
#msform {
  text-align: center;
  position: relative;
}
#msform fieldset {
  border: 0 none;
  border-radius: 0px;
  padding: 20px 30px;
  transform: unset !important;
  box-sizing: border-box;
  position: relative !important;
}

.padding_form {
  padding: 0px 10px !important;
}
.fl-left {
  float: left;
  font-size: 16px;
}
label {
  display: inline-block;
  max-width: 100%;
  margin-bottom: 5px;
  font-weight: 700;
}
#msform input, #msform textarea {
  display: block;
  width: 100%;
  height: 34px;
  padding: 6px 12px;
  font-size: 14px;
  line-height: 1.42857143;
  color: #555;
  margin-bottom: 20px;
  background-color: #fff;
  background-image: none;
  border: 1px solid #ccc;
  border-radius: 4px;
  -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
  box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
  -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
  -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
  transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
}
.form-control:not(select) {
  -webkit-appearance: none;
  -moz-appearance: none;
  appearance: none;
}
.form-control {
  border-radius: 0;
  box-shadow: none;
  border-color: #d2d6de;
}
  
      `}</style>
    </div>
  );
};
export default IntegrateERPForm;
