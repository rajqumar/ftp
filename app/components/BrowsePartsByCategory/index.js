import React from 'react';
import Example from 'components/Example';
import { UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
const partdata = [
  {
    title: 'parts',
    partMenu: [
      {
        menutitle: 'capacitors',
      },
      {
        menutitle: 'kits',
      },
      {
        menutitle: 'filters',
      },
      {
        menutitle: 'electronics',
      },
      {
        menutitle: 'resistors',
      },
    ],
  },
  {
    title: 'suppliers',
    partMenu: [
      {
        menutitle: 'supplier 1',
      },
      {
        menutitle: 'supplier 2',
      },
      {
        menutitle: 'supplier 3',
      },
      {
        menutitle: 'supplier 4',
      },
      {
        menutitle: 'supplier 5',
      },
    ],
  },
  {
    title: 'more',
    partMenu: [
      {
        menutitle: 'plastics',
      },
      {
        menutitle: 'resins',
      },
    ],
  },
];
function BrowsePartsByCategory() {
  return (
    <div className="container-fluid">
      <div className="row mt-10">
        <div className="col-md-11 pa-0">
          <Example />
        </div>
        <div className="col-md-1 pa-0">
          <div className="part_supp">
            {partdata.map((item, i) => (
              <UncontrolledDropdown key={i} className="drop_more">
                <DropdownToggle nav caret className="new_part part_drop">
                  {item.title}
                </DropdownToggle>
                <DropdownMenu down="true" className="drop_meni">
                  {item.partMenu.map((parts, j) => (
                    <DropdownItem key={j}>
                      {parts.menutitle}
                      <DropdownItem divider />
                    </DropdownItem>
                  ))}
                </DropdownMenu>
              </UncontrolledDropdown>
            ))}
          </div>
        </div>
      </div>
      <style>{`
      //block image
      .col-md-4 {
        width: 33.33333333%;
    }
    .img-responsive {
      display: block;
      max-width: 100%;
      height: auto;
    }
    .mt-10
    {
      margin-top:5px;
    }
    .mb-20 {
        margin-bottom: 20px !important;
    }
     
      .pa-0
    {
      padding-left:0px !important;
      padding-right:0px !important;
    }
    .part_supp
    {
          display: block;
        background-color: #FAFAFA;
        border-radius: 3px;
        width: 100px;
        height: 130px;
        text-align: left;
        box-shadow: 0 15px 45px 0 rgba(0, 0, 0, 0.05);
        transition: all 0.3s ease-in-out 0s;
        border: 1px solid #e1e0e0ba;
        transition: transform .5s ease;
    }
    .drop_meni {
      width: 100px;
      min-width: 100px;
      word-break: break-word;
  }
  .drop_more {
    border-bottom: 1px solid #ccc;
}

  .drop_meni .dropdown-item{
    padding-left:12px; 
    text-align:left
  }
  .new_part {
    color: #333;
    font-size: 13px;
    background: #fff !important;
    border: 1px solid #fff !important;
    color: #333;
    padding: 0px;
    padding-left: 5px;
    text-align: left;
    margin-bottom: 10px !important;
    cursor: pointer;
    margin-top: 10px !important;
    box-shadow: unset !important;
}
.part_drop:after {
  float: right;
  margin-right: 10px;
  margin-top: 5px;
}

.new_part::after {
  display: inline-block;
  width: 0;
  height: 0;
  margin-left: .255em;
  vertical-align: .255em;
  content: "";
  border-top: .3em solid;
  border-right: .3em solid transparent;
  border-bottom: 0;
  border-left: .3em solid transparent;
}
      `}</style>
    </div>
  );
}
export default BrowsePartsByCategory;
