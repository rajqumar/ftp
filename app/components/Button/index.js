import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import PropTypes from 'prop-types';

function Button(props) {
  var { value, color, shape, position, icon, dynamicClassName } = props;

  var r_icon = icon != null ? position === 'left' ? <FontAwesomeIcon icon={icon} width="16" /> : '' : '';
  var l_icon = icon != null ? position === 'right' ? <FontAwesomeIcon icon={icon} width="16" /> : '' : '';
  var color_class = `btn_sign_in btn-block ${color} ${shape} ${dynamicClassName}`;
  
  return (
    <React.Fragment>
      <button type="submit" className={color_class}>
        {r_icon}
        {value}
        {l_icon}
      </button>

      <style jsx>{`
        .btn_sign_in {
          background-color: rgba(210, 35, 42, 1) !important;
          border: 1px solid rgba(210, 35, 42, 1) !important;
          border-radius: 4px !important;
          margin-bottom: 20px;
          padding: 0px 20px;
          font-size: 16px;
        }

        .red {
          background-color: rgba(210, 35, 42, 1);
          border: 1px solid rgba(210, 35, 42, 1);
          color: #fff;
        }

        .green {
          font-size: 14px;
          color: white !important;
          background-color: #6DC144 !important;
          border-color: #6DC144 !important;
        }

        .green:hover {
          background-color: #008d4c !important;
        }

        .silver {
          background-color: grey;
          border-color: rgba(210, 35, 42, 1) !important;
        }

        .rect {
          border-radius: 4px !important;
        }

        .oval {
          border-radius: 50px !important;
        }
        
        .btn-primary {
          color: #fff;
          background-color: #337ab7;
          border-color: #2e6da4;
        }
      `}</style>
    </React.Fragment>
  );
}

Button.propTypes = {
  value: PropTypes.string,
  color: PropTypes.string,
  shape: PropTypes.string,
  position: PropTypes.string,
  icon: PropTypes.object,
  dynamicClassName: PropTypes.string,
};

export default Button;
