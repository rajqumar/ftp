import React from 'react';
import Statistics from 'components/Statistics';
import CustomBarChart from 'components/CustomBarChart';
import Rating from 'components/Rating';
import YearToDate from 'components/YearToDate';
import { faCalendar } from '@fortawesome/free-solid-svg-icons';
import PropTypes from 'prop-types';
const cssForBuyer = () => {
  return (
    <style>{`
          .buy_back_color {
              background-color: #fcfba7;
              padding-top: 20px;
          }
          .content {
            min-height: 250px;
            padding: 15px;
            margin-right: auto;
            margin-left: auto;
            padding-left: 15px;
            padding-right: 15px;
        }
        .sell_back_color {
          background-color: #D3EFFF;
          padding-top: 20px;
      }
      .mt-10 {
        margin-top: 8%;
        
    }
      
          `}</style>
  );
};
function DashboardBuyerForm(props) {
  var { ratingTitle, dynamicClass, title, datasheet, YearToDateDummy, rating, data } = props;

  return (
    <div className="mt-10">
      <section className={dynamicClass}>
        <div className="container">
          <section className="content">
            <Statistics data={data} title={title} dynamicClass="procurement_head" />
          </section>
        </div>
        <div className="block_all">
          <div className="container">
            <div className="row">
              <div className="col-md-8">
                <CustomBarChart dataToSheet={datasheet} />
              </div>
              <div className="col-md-4">
                <Rating rating={rating} title={ratingTitle} />
                <YearToDate data={YearToDateDummy} icon={faCalendar} title="Year to Date" dynamicClass="col-md-4" />
              </div>
            </div>
          </div>
        </div>
      </section>
      {cssForBuyer()}
    </div>
  );
}
DashboardBuyerForm.propTypes = {
  title: PropTypes.string,
  ratingTitle: PropTypes.string,
  dynamicClass: PropTypes.string,
  rating: PropTypes.array,
  datasheet: PropTypes.array,
  YearToDateDummy: PropTypes.array,
  data: PropTypes.array,
};
export default DashboardBuyerForm;
