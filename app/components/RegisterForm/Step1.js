import React from 'react';
import PropTypes from 'prop-types';
import Countries from './countries';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBuilding } from '@fortawesome/free-solid-svg-icons';
import { InputGroup, InputGroupText, Input } from 'reactstrap';

function Step1(props) {
  const { step, handleChange, c_name, c_uen, c_phone, c_email, c_addr1, c_addr2, c_code, country } = props;
  if (step !== 1) {
    return null;
  }

  const showoption = () => {
    var optionarray = [];

    Countries.country_list.filter((item, index) => {
      if (country != '' && item.name == country)
        optionarray.push(
          <option key={index} value={item.name} selected className="capitalCountry">
            {item.name}
          </option>
        );
      else {
        optionarray.push(
          <option key={index} value={item.name} className="capitalCountry" selected>
            {item.name}
          </option>
        );
      }
    });
    return optionarray;
  };
  return (
    <React.Fragment>
      <fieldset>
        <h2 className="fs-title">
          <FontAwesomeIcon icon={faBuilding} />
          &nbsp;Company Information
        </h2>
        <div className="row">
          <div className="col-md-6">
            <div className="form-group">
              <label htmlFor="name" className="fl-left">
                Name
              </label>
              <input
                type="text"
                className="form-control"
                id="c_name"
                name="c_name"
                value={c_name}
                placeholder="Acme Inc"
                onChange={handleChange}
                required
              />
              {/* <span className="error">Please enter valid Company name</span> */}
            </div>
            <div className="form-group">
              <label htmlFor="email" className="fl-left">
                Email
              </label>
              <input
                type="email"
                className="form-control"
                id="c_email"
                name="c_email"
                value={c_email}
                placeholder="enquiries@example.com"
                onChange={handleChange}
                required
              />
            </div>

            <div className="form-group">
              <label htmlFor="Address" className="fl-left">
                Address
              </label>
              <input
                type="text"
                className="form-control"
                id="c_addr1"
                name="c_addr1"
                value={c_addr1}
                placeholder="Address1"
                onChange={handleChange}
                required
              />
            </div>
            <div className="form-group">
              <input
                type="text"
                className="form-control"
                id="c_addr2"
                name="c_addr2"
                value={c_addr2}
                placeholder="Address2"
                onChange={handleChange}
                required
              />
            </div>
          </div>
          <div className="col-md-6">
            <div className="form-group">
              <label htmlFor="uen" className="fl-left">
                UEN
              </label>
              <input
                type="text"
                className="form-control"
                id="c_uen"
                name="c_uen"
                value={c_uen}
                placeholder="201987654E"
                onChange={handleChange}
                required
              />
            </div>

            <div className="form-group">
              <label htmlFor="phone" className="fl-left">
                Phone
              </label>
              <InputGroup size="sm">
                <InputGroupText
                  style={{ fontSize: '12px !important', borderRadius: '0', width: '45px', height: '31px' }}
                  className="phonecode">
                  {c_code}
                </InputGroupText>
                <Input
                  type="number"
                  className="form-control"
                  id="c_phone"
                  name="c_phone"
                  value={c_phone}
                  placeholder=" 6789 1234"
                  onChange={handleChange}
                  required
                />
              </InputGroup>
            </div>

            <div className="form-group">
              <label htmlFor="email" className="fl-left">
                Country
              </label>
              <select className="form-control select2" id="country" name="country" onChange={handleChange} required>
                <option value="default">Select Country</option>
                {/* {Countries.country_list.map((c, i) => (
                  <option key={i} value={c.value} className="capitalCountry">
                    {c.value}
                  </option>
                ))} */}
                {showoption()}
              </select>
            </div>
          </div>
        </div>
      </fieldset>
      <style jsx>{`
            .action-button {
              width: 150px;
              background: #d2232a;
              color: #fff;
              border: 0 none;
              cursor: pointer;
              padding: 5px;
              float: right;
              margin: 15px auto;
          }
          .phonecode {
           
            border-radius:0;
            width: 45px;
            height: 31px;
        }
.fl-left {
  float: left;
  font-size: 16px;
  margin-bottom: 5px;
  font-weight: 700;
}
select#country {
  text-transform: capitalize;
}
 fieldset {
  border: 0 none;
  border-radius: 0px;
  padding: 20px 30px;
transform:unset !important;
  box-sizing: border-box;
  position: relative !important;
}

 fieldset:not(:first-of-type) {
  display: none;
}
.error{
  font-size: 12px;
  float: left;
  color: red;
  margin-top: 5px;
  margin-bottom: 10px;
}
/*inputs*/
 input,  textarea {
 display: block;
  width: 100%;
  height: 34px;
  padding: 6px 12px;
  font-size: 14px;
  line-height: 1.42857143;
  color: #555;
//margin-bottom:20px;
  background-color: #fff;
  background-image: none;
  border: 1px solid #ccc;
  border-radius: 4px;
  -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
  box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
  -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
  -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
  transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
}

 input:focus,  textarea:focus {
  -moz-box-shadow: none !important;
  -webkit-box-shadow: none !important;
  box-shadow: none !important;
  border: 1px solid #d2232a;
  outline-width: 0;
 
}

/*buttons*/

 .action-button {
    width: 150px;
  background: #d2232a;
  color: #fff;
  border: 0 none;
  cursor: pointer;
  padding: 5px;
float:right;
  margin: 15px auto;
}
 .back_to_home
{
  width: 150px;
  background: #d2232a;
  color: #fff;
  border: 0 none;
  cursor: pointer;
  padding: 5px;
  margin: 15px auto;
}
.fs-title {
  font-size: 21px;
  text-transform: capitalize;
  color: #333;
  margin-bottom: 20px;
  letter-spacing: 1px;
  text-align: left;
  font-weight: bold;
}
 .action-button-previous {
  
width: 150px;
  background: #d2232a;
  color: #fff;
  border: 0 none;
  display: -webkit-inline-box;
  cursor: pointer;
  padding: 5px;
float:right;
  margin: 15px 15px;
}

}
      `}</style>
    </React.Fragment>
  );
}

Step1.propTypes = {
  step: PropTypes.number,
  handleChange: PropTypes.func,
  c_name: PropTypes.string,
  c_uen: PropTypes.string,
  c_phone: PropTypes.string,
  c_email: PropTypes.string,
  c_addr1: PropTypes.string,
  c_addr2: PropTypes.string,
  country: PropTypes.string,
  c_code: PropTypes.string,
};

export default Step1;
