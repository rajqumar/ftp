import React from 'react';
import PropTypes from 'prop-types';
var BarChart = require('react-d3-components').BarChart;
import { faChartBar } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
var tooltipScatter = function(x, y0, y) {
  // return "x: " + y0 + " y: " + y;
  return y;
};
function CustomBarChart(props) {
  var { dataToSheet } = props;

  return (
    <div>
      <div className="box">
        <div className="box-header with-border">
          <h3 className="box-title">
            <FontAwesomeIcon icon={faChartBar} width="12" />
            &nbsp; Purchase Orders
          </h3>

          <div className="box-tools pull-right yeardrop">
            <div className="form-group">
              <select className="form-control select2">
                <option value="default">2019 &nbsp;&nbsp;&nbsp;</option>
                <option value="2018">2018</option>
                <option value="2017">2017</option>
                <option value="2016">2016</option>
                <option value="2015">2015</option>
                <option value="2014">2014</option>
              </select>
            </div>
          </div>
        </div>

        <div className="box-body">
          <div id="app">
            <BarChart
              tooltipHtml={tooltipScatter}
              data={dataToSheet}
              width={700}
              height={500}
              yAxis={[0, 100]}
              margin={{ top: 10, bottom: 50, left: 50, right: 10 }}
            />
            {/* <img src="/static/images/barchart.png" className="img-responsive" alt="barchar" /> */}
          </div>
        </div>
      </div>
      <style>{`
.bar:hover {    
  // fill: #E55;
}
.yeardrop{
 margin-top: -7px !important;
 
}
.bar {    
  fill: #37B300;
  stroke: #37B300;
}
.tooltip {
  padding: 3px;
  border: 2px solid;
  border-radius: 2px;
  background-color: red;
  opacity: 0.6;
  justify-content: center;
  align-items: center;
  
}
.procurement_head {
  padding-left: 10px;
  font-size: 20px;
  font-weight: 700;
}
.box {
  position: relative;
  border-radius: 3px;
  background: #ffffff;
  border: 1px solid #ececec;
  margin-bottom: 20px;
  width: 100%;
  box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
}
.box-header.with-border {
  border-bottom: 1px solid #e9e5e5;
}
.box-header {
  color: #444;
  display: block;
  padding: 10px;
  position: relative;
}
.box-title {
  color: #333;
  font-weight: 400;
  font-size: 12px;
  line-height: 0;
  word-break: break-word;
}
.box-header .box-title {
  display: inline-block;
  font-size: 18px;
  margin: 0;
  line-height: 1;
  font-weight: 700;
}
.box-header.box-tools {
  position: absolute;
  right: 10px;
  top: 5px;
}
.pull-right {
  float: right!important;
}
.select2-hidden-accessible {
  border: 0 !important;
  clip: rect(0 0 0 0) !important;
  height: 1px !important;
  margin: -1px !important;
  overflow: hidden !important;
  padding: 0 !important;
  position: absolute !important;
  width: 1px !important;
}

.form-control {
  box-shadow: none;
  border-color: #d2d6de;
  background-color: #fff;
    border: 1px solid #ccc;
    border-radius: 4px;
}
option {
  font-weight: normal;
  display: block;
  white-space: pre;
  min-height: 1.2em;
  padding: 0px 2px 1px;
}
.select2-container {
  box-sizing: border-box;
  display: inline-block;
  margin: 0;
  position: relative;
  vertical-align: middle;
}
.select2-container--default .select2-selection--single {
  background-color: #fff;
  border: 1px solid #ccc;
  border-radius: 4px;
}
.select2-container .select2-selection--single {
  box-sizing: border-box;
  cursor: pointer;
  display: block;
  height: 34px;
  user-select: none;
  -webkit-user-select: none;
}
.select2-container--default .select2-selection--single, .select2-selection .select2-selection--single {
  border: 1px solid #d2d6de;
  border-radius: 0;
  padding: 6px 12px;
  height: 34px;
}
.box-body {
  border-top-left-radius: 0;
  border-top-right-radius: 0;
  border-bottom-right-radius: 3px;
  border-bottom-left-radius: 3px;
  padding: 10px;
}
#app {
  width: 100%;
  height: 100%;
}
`}</style>
    </div>
  );
}
CustomBarChart.propTypes = {
  dataToSheet: PropTypes.array,
};

export default CustomBarChart;
