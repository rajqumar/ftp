import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import PropTypes from 'prop-types';

function SearchInput(props) {
  const { title } = props;
  return (
    <div className="search_part ">
      <div className="container">
        <div className="row p-0-20">
          <h2 className="seadrch_head">{title}</h2>
          <div className="row">
            <div className="col-md-8">
              <div className="span12">
                <form action="#" method="get" className="sidebar-form">
                  <div className="input-group">
                    <input
                      type="text"
                      name="q"
                      className="form-control search-form-control"
                      placeholder="Search by keywords, specifications or part numbers"
                    />
                    <span className="input-group-btn">
                      <button type="submit" name="search" id="search-btn" className="btn btn-flat btn_search_new">
                        <FontAwesomeIcon icon={faSearch} width="12" />
                      </button>
                    </span>
                  </div>
                </form>
                <div className="strip_search">
                  <div className="container">
                    <p>
                      HelloFTP is the easiest Search engine for electronic parts. Search across hundreds of suppliers
                      and thousands of manufacturers.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <style jsx>{`
        .search_part {
          background-color: #d31e25;
          padding: 0px;
          margin-top: 78px;
        }

        .p-0-20 {
          padding: 0 20px;
        }
        .seadrch_head {
          color: #fff;
          font-weight: 600;
          font-size: 24px;
          margin-top: 8px;
          margin-bottom: 8px;
        }
        #search-btn {
          background: #fff !important;
          border: 0px !important;
          border-radius: 0px 5px 5px 0px;
          margin-right: 20px;
          padding: 7px;
          border-left: none !important;
        }
        .sidebar-form,
        .sidebar-menu > li.header {
          overflow: hidden;
          text-overflow: clip;
        }
        .strip_search p {
          color: #fff;
          padding: 15px 0;
          margin: 0px 0px 0px -10px;
        }
        .pt-10 {
          padding-top: 10px;
        }
        .search_note {
          color: #fff;
          font-size: 12px;
        }
      `}</style>
    </div>
  );
}
SearchInput.propTypes = {
  title: PropTypes.string,
};
export default SearchInput;
