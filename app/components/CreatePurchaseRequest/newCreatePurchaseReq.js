//newCreatePurchaseReq
import React from 'react';
// import PropTypes from 'prop-types';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Link from 'next/link';

function NewCreatePurchaseReq() {
  return (
    <React.Fragment>
      <div className="row">
        <div className="col-md-6">
          <h4 className="create_purchase">Create Purchase Request</h4>
        </div>
        <div className="col-md-6">
          <Link href="/Quotes/purchaseRequests">
            <button className="btn btn-success purchase_save" type="button">
              SAVE
            </button>
          </Link>
        </div>
      </div>
      <div className="box">
        <div className="box-header with-border">
          <h5 className="head_test">
            <FontAwesomeIcon icon={faInfoCircle} className="black" />
            &nbsp;&nbsp;Purchase Request Details
          </h5>
        </div>
        <div className="box-body">
          <form id="msform">
            <fieldset className="padding_form">
              <div className="row">
                <div className="col-md-6">
                  <div className="form-group">
                    <label htmlFor="name" className="fl-left">
                      Request Number
                    </label>
                    <input type="text" className="form-control" id="name" name="name" placeholder="R1248979UW739FKS" />
                  </div>
                  <div className="form-group">
                    <label htmlFor="name" className="fl-left">
                      Shipment Address{' '}
                    </label>

                    <select className="form-control select2" style={{ width: ' 100%' }}>
                      <option value="default">HQ:1 Yishun Industrial Street 1, APosh Bizhub</option>
                      <option value="2018">HQ:2 Yishun Industrial Street 1, APosh Bizhub</option>
                      <option value="2017">HQ:3 Yishun Industrial Street 1, APosh Bizhub</option>
                      <option value="2016">HQ:4 Yishun Industrial Street 1, APosh Bizhub</option>
                      <option value="2015">HQ:5 Yishun Industrial Street 1, APosh Bizhub</option>
                      <option value="2014">HQ:6 Yishun Industrial Street 1, APosh Bizhub</option>
                    </select>
                  </div>
                </div>

                <div className="col-md-6">
                  <div className="form-group">
                    <label htmlFor="name" className="fl-left">
                      Expected Date
                    </label>
                    <input type="text" className="form-control" id="name" name="name" placeholder="1 Jan 2020" />
                  </div>
                </div>
              </div>
            </fieldset>
          </form>
        </div>
      </div>

      <style>{`.create_purchase {
            font-size: 24px;
            font-weight: 700;
        }
        .purchase_save {
            background: #6DC144;
            border: 1px solid #6DC144;
            padding: 10px 60px;
            margin-bottom: 20px;
            float: right;
        }
        
        .btn-success {
            background-color: #00a65a;
            border-color: #008d4c;
        }
        .btn {
            border-radius: 3px;
            -webkit-box-shadow: none;
            box-shadow: none;
            border: 1px solid transparent;
        }
        .box {
            position: relative;
            border-radius: 3px;
            background: #ffffff;
            border: 1px solid #ececec;
            margin-bottom: 20px;
            width: 100%;
            box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
        }
        .box-header.with-border {
            border-bottom: 1px solid #e9e5e5;
        }
        
        .box-header {
            color: #444;
            display: block;
            padding: 10px;
            position: relative;
        }
        .box-header:before, .box-body:before, .box-footer:before, .box-header:after, .box-body:after, .box-footer:after {
            content: " ";
            display: table;
        }
        .head_test {
            font-size: 16px;
            font-weight: 700;
        }
        .black {
            color: #333 !important;
        }
        .box-body {
            border-top-left-radius: 0;
            border-top-right-radius: 0;
            border-bottom-right-radius: 3px;
            border-bottom-left-radius: 3px;
            padding: 10px;
        }
        .box-header:before, .box-body:before, .box-footer:before, .box-header:after, .box-body:after, .box-footer:after {
            content: " ";
            display: table;
        }
        #msform {
            text-align: center;
            position: relative;
        }
        #msform fieldset {
            border: 0 none;
            border-radius: 0px;
            padding: 20px 30px;
            transform: unset !important;
            box-sizing: border-box;
            position: relative !important;
        }
        
        .padding_form {
            padding: 0px 10px !important;
        }
        .fl-left {
            float: left;
            font-size: 16px;
        }
        
        label {
            display: inline-block;
            max-width: 100%;
            margin-bottom: 5px;
            font-weight: 700;
        }
        #msform input, #msform textarea {
            display: block;
            width: 100%;
            height: 34px;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.42857143;
            color: #555;
            margin-bottom: 20px;
            background-color: #fff;
            background-image: none;
            border: 1px solid #ccc;
            border-radius: 4px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
            box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
            -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
            -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        }
        .form-control:not(select) {
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
        }
        
        .form-control {
            border-radius: 0;
            box-shadow: none;
            border-color: #d2d6de;
        }
        .ship-dropdown{
            width:100%;
            float:left;
        }
        .ship-dropdown >div > div {
            height:34px !important;
        }
        `}</style>
    </React.Fragment>
  );
}
// CreatePurchaseForm.propTypes = {
//   dataToSheet: PropTypes.array,
// };

export default NewCreatePurchaseReq;
