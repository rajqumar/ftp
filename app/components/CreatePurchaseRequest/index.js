import React from 'react';
// import PropTypes from 'prop-types';
import NewCreatePurchaseReq from './newCreatePurchaseReq';
import PartList from './purchaseRequestPartList';
import PartFooter from './partfooter';
function CreatePurchaseForm() {
  return (
    <section className="buy_back_color pt-20 mt-8">
      <div className="block_all">
        <div className="container">
          <div className="row">
            <div className="create_ueser">
              <div className="container">
                <NewCreatePurchaseReq />
                <PartList />
                <PartFooter />
              </div>
            </div>
          </div>
        </div>
      </div>
      <style>{`
      .mt-8{
        margin-top:8%;
      }
            .pt-20 {
                padding-top: 20px;
            }
            .buy_back_color {
                background-color: #FFFFD7;
                margin-top:110px;
            }
            .create_ueser{
                width:100%;
            }
            
        
        `}</style>
    </section>
  );
}
// CreatePurchaseForm.propTypes = {
//   dataToSheet: PropTypes.array,
// };

export default CreatePurchaseForm;
