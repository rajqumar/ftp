import React, { useState } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
// import PropTypes from 'prop-types';
import Tables from 'components/Tables';

import { faPlus, faMicrochip } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
const columns = [
  {
    name: 'Part No',
  },

  {
    name: 'Description',
  },
  {
    name: 'Quantity',
  },
  {
    name: 'Currency',
  },
  {
    name: 'Unit Price	',
  },
  {
    name: 'Subtotal',
  },
];

var description = (
  <div>
    <p className="">Text Instrumnets</p>
    <p className="mb-0">Timer IC operatings temprature:0-70c</p>
  </div>
);

var nameTitle = (
  <div>
    <p>
      {' '}
      <FontAwesomeIcon className="red_name" icon={faPlus} />
      &nbsp;&nbsp;&nbsp; NE88889
    </p>
  </div>
);
const tableData = [
  [nameTitle, description, '1,000', 'USD', '0.10', '1,350'],
  [nameTitle, description, '1,000', 'USD', '0.10', '1,350'],
  [nameTitle, description, '1,000', 'USD', '0.10', '1,350'],
];
const options = {
  filterType: 'dropdown',
  pagination: false,
  search: false,
  filter: true,
  download: false,
  print: false,
  viewColumns: false,
  toolbar: false,
  filterTable: false,
  searchOpen: false,
  selectableRows: false,
};
function PartList() {
  // const { className } = props;
  const [modal, setModal] = useState(false);

  const toggle = () => setModal(!modal);

  return (
    <React.Fragment>
      <div className="box">
        <div className="box-header with-border">
          <h5 className="head_test">
            <FontAwesomeIcon className="black" icon={faMicrochip} />
            &nbsp;&nbsp;Parts
          </h5>
          <div className="box-tools pull-right">
            <button className="btn btn-danger mt-5" type="button" onClick={toggle}>
              <FontAwesomeIcon icon={faPlus} />
            </button>
          </div>
        </div>
        <div className="box-body">
          <div className="table-responsive table-bordered">
            <Tables data={tableData} columns={columns} options={options} />
          </div>
        </div>
      </div>
      <Modal isOpen={modal} toggle={toggle}>
        <ModalHeader toggle={toggle}>Select Part</ModalHeader>
        <ModalBody>
          <form id="msform">
            <fieldset className="padding_form">
              <div className="row">
                <div className="col-md-4">
                  <div className="form-group">
                    <label htmlFor="name" className="fl-left">
                      Select Part
                    </label>
                    <select className="form-control select2 part-drop" style={{ width: ' 100%;' }}>
                      <option value="default">NE555P</option>
                      <option value="2018">NE556P</option>
                      <option value="2017">NE557P</option>
                      <option value="2016">NE558P</option>
                      <option value="2015">NE559P</option>
                      <option value="2014">NE560P</option>
                    </select>{' '}
                  </div>
                </div>

                <div className="col-md-4">
                  <div className="form-group">
                    <label htmlFor="name" className="fl-left">
                      Unit Price
                    </label>
                    <input type="text" className="form-control" id="name" name="name" placeholder="0.00" />
                  </div>
                </div>

                <div className="col-md-4">
                  <div className="form-group">
                    <label htmlFor="name" className="fl-left">
                      Quantity
                    </label>
                    <input type="number" className="form-control" id="name" name="name" placeholder="1" />
                  </div>
                </div>
              </div>
            </fieldset>
          </form>
        </ModalBody>
        <ModalFooter>
          <Button onClick={toggle} className="btn btn-success modal-btn">
            Add To PR
          </Button>
          {/* <button type="button" className="btn btn-success" data-dismiss="modal">Add To PR</button> */}
        </ModalFooter>
      </Modal>
      <style>{`
                    .box {
                        position: relative;
                        border-radius: 3px;
                        background: #ffffff;
                        border: 1px solid #ececec;
                        margin-bottom: 20px;
                        width: 100%;
                        box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
                    }
                    .box-header.with-border {
                        border-bottom: 1px solid #e9e5e5;
                    }
                    
                    .box-header {
                        color: #444;
                        display: block;
                        padding: 10px;
                        position: relative;
                    }
                    .head_test {
                        font-size: 14px;
                        font-weight: 800;
                    }
                    .box-header>.box-tools {
                        position: absolute;
                        right: 10px;
                        top: 5px;
                    }
                    .mt-5 {
                        margin-top: 0px !important;
                    }
                    
                    .btn-danger {
                        background-color: #dd4b39;
                        border-color: #d73925;
                        font-size: 12px;
                    }
                    .btn {
                        border-radius: 3px;
                        -webkit-box-shadow: none;
                        box-shadow: none;
                        border: 1px solid transparent;
                    }
                    .box-body {
                        border-top-left-radius: 0;
                        border-top-right-radius: 0;
                        border-bottom-right-radius: 3px;
                        border-bottom-left-radius: 3px;
                        padding: 10px;
                    }
                    //modal css
                    .part-drop {
                        border-radius: 4px;
                        border: 1px solid #ccc;
                        width: 100%;
                        height: 34px;
                        padding: 6px 12px;
                        font-size: 14px;
                    }
                    #msform {
                        text-align: center;
                        position: relative;
                    }
                    #msform fieldset {
                        border: 0 none;
                        border-radius: 0px;
                        padding: 20px 30px;
                        transform: unset !important;
                        box-sizing: border-box;
                        position: relative !important;
                    }
                    fl-left {
                        float: left;
                        font-size: 16px;
                    }
                     label {
                        display: inline-block;
                        max-width: 100%;
                        margin-bottom: 5px;
                        font-weight: 700;
                    }
                   
                    .padding_form {
                        padding: 0px 10px !important;
                    }
                    #msform input, #msform textarea {
                        display: block;
                        width: 100%;
                        height: 34px;
                        padding: 6px 12px;
                        font-size: 14px;
                        line-height: 1.42857143;
                        color: #555;
                        margin-bottom: 20px;
                        background-color: #fff;
                        background-image: none;
                        border: 1px solid #ccc;
                        border-radius: 4px;
                        -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
                        box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
                        -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
                        -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
                        transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
                    }
                    
                    .form-control:not(select) {
                        -webkit-appearance: none;
                        -moz-appearance: none;
                        appearance: none;
                    }
                    .form-control {
                        border-radius: 0;
                        box-shadow: none;
                        border-color: #d2d6de;
                    }
                    .modal-btn{
                        font-size: 12px;
                        font-weight: 400;
                    }
                    .MuiToolbar-regular {
                        min-height: 64px;
                        display: none;
                    }
                    .table-bordered {
                        border: 1px solid #f4f4f4;
                    }
                    `}</style>
    </React.Fragment>
  );
}

export default PartList;
