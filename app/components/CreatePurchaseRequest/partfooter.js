//newCreatePurchaseReq
import React from 'react';
// import PropTypes from 'prop-types';
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

function PartFooter() {
  return (
    <div className="part-footer">
      <div className="row">
        <div className="col-md-6 pl-0">
          <div className="box">
            <div className="box-header with-border">
              <h5 className="head_test">
                <FontAwesomeIcon icon={faInfoCircle} className="black"></FontAwesomeIcon>
                &nbsp;&nbsp;Notes
              </h5>
            </div>
            <div className="box-body notes_padding">
              <form id="msform">
                <fieldset className="padding_form">
                  <div className="row">
                    <div className="form-group">
                      <label htmlFor="name" className="fl-left">
                        Notes
                      </label>
                      <textarea className="form-control height_txt_area" rows="12" id="comment"></textarea>
                    </div>
                  </div>
                </fieldset>
              </form>
            </div>
          </div>
        </div>
        <div className="col-md-6 pr-0">
          <div className="box">
            <div className="box-header with-border">
              <h5 className="head_test">
                <i className="fa fa-usd black" aria-hidden="true"></i>
                &nbsp;&nbsp;Request Value
              </h5>
            </div>
            <div className="box-body notes_padding">
              <div className="row">
                <div className="col-md-6">
                  <p className="req_value_data">Currency</p>
                </div>
                <div className="col-md-6">
                  <p className="req_value_cur">USD</p>
                </div>
                <div className="col-md-6">
                  <p className="req_value_data">Amount Payable</p>
                </div>
                <div className="col-md-6">
                  <p className="req_value_cur">60.00</p>
                </div>

                <div className="col-md-6">
                  <p className="req_value_data">Tax Payable</p>
                </div>
                <div className="col-md-6">
                  <p className="req_value_cur">6.00</p>
                </div>

                <div className="col-md-6">
                  <p className="req_value_data">
                    <strong>Total Amount</strong>
                  </p>
                </div>
                <div className="col-md-6">
                  <p className="req_value_cur">66.00</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <style>{`
            .part-footer{
                margin: 0px 15px;
            }
            .pl-0 {
                padding-left: 0px !Important;
            }
            .pr-0 {
                padding-right: 0px !Important;
            }

            #msform textarea {
                display: block;
                width: 100%;
                height: 34px;
                padding: 6px 12px;
                font-size: 14px;
                line-height: 1.42857143;
                color: #555;
                margin-bottom: 20px;
                background-color: #fff;
                background-image: none;
                border: 1px solid #ccc;
                border-radius: 4px;
                -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
                box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
                -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
                -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
                transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            }
            .height_txt_area {
                height: 75px !important;
            }
            .padding_form {
                padding: 0px 10px !important;
            }
            .notes_padding {
                padding: 20px;
            }
            .req_value_data {
                text-align: left;
                margin-bottom: 5px;

            }
            .req_value_cur {
                text-align: right;
                margin-bottom: 11px;
            }

            `}</style>
    </div>
  );
}

export default PartFooter;
