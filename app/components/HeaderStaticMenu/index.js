import React from 'react';
import { Nav } from 'reactstrap';
import { NavLink, UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
var newMenuStatic = [
  {
    title: 'New to ftp',
    submenu: [
      {
        title: 'About Us',
        url: '',
      },
      {
        title: 'FTP Core & Advisory Teams',
        url: '',
      },
      {
        title: 'What is FTP',
        url: '',
      },
      {
        title: 'FTP Features & Benefits',
        url: '',
      },
      {
        title: 'FTP Membership Plan',
        url: '',
      },
      {
        title: 'SMEs’ FAQ',
        url: '',
      },
    ],
  },
  {
    title: 'Value-Added Services',
    submenu: [
      {
        title: 'Berkley Insurance ASIA - Marine',
        url: '',
      },
      {
        title: 'OFX - Global Money Transfers',
        url: '',
      },
      {
        title: 'GOGOVAN Singapore – Last Mile Fulfilment',
        url: '',
      },
      {
        title: 'Funding Societies – SME Financing',
        url: '',
      },
      {
        title: 'FTP Discounting Solution',
        url: '',
      },
    ],
  },
  {
    title: 'Regional Markets',
    submenu: [
      {
        title: 'Asean',
        url: 'https://ftp-lite-node.hellosme.com',
      },
      {
        title: 'Greater China',
        url: '',
      },
      {
        title: 'India',
        url: '',
      },
      {
        title: 'Japan',
        url: '',
      },
      {
        title: 'Korea',
        url: '',
      },
    ],
  },
];
function HeaderStaticMenu() {
  return (
    <React.Fragment>
      <Nav navbar>
        {newMenuStatic.map((item, i) => (
          <UncontrolledDropdown key={i}>
            <DropdownToggle nav caret className="static-menu-title">
              {item.title}
            </DropdownToggle>
            <DropdownMenu down="true">
              {item.submenu.map((subitem, j) =>
                j == 0 ? (
                  <NavLink
                    key={j}
                    className="nav_link_head"
                    href={subitem.url != '' ? subitem.url : '#'}
                    target="_blank">
                    <DropdownItem className="first-active-menu" to={subitem.url}>
                      {subitem.title}
                    </DropdownItem>
                  </NavLink>
                ) : (
                  <DropdownItem key={j} className="subitem-menu">
                    {subitem.title}
                    <DropdownItem divider />
                  </DropdownItem>
                )
              )}
            </DropdownMenu>
          </UncontrolledDropdown>
        ))}
      </Nav>
      <style>{`
                  .subitem-menu{
                    background-color: #ccc;
                    text-transform: uppercase;
                    font-size: 11px;
                    font-weight: 600;
                  }
                
           
                  .subitem-menu:hover {
                    color: #d2232a !important;
                    text-decoration: none;
                    background-color: #f8f9fa;
                    font-size: 11px;
                    text-transform: uppercase;
                    font-weight: 600;
                }
                  .first-active-menu{
                    font-size: 11px;
                    font-weight: 600;
                    text-transform: uppercase;
                  }
                  .static-menu-title{
                    text-transform: uppercase;
                    font-weight: 600;
                    color: rgba(37, 32, 32, 1) !important;
                    font-size: 12px !important;
                  }
                  .static-menu-title:hover{
                    background-color: transparent !important;
                   color: #d2232a !important;
                  }
                 
                  `}</style>
    </React.Fragment>
  );
}
export default HeaderStaticMenu;
