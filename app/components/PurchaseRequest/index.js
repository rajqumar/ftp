import React from 'react';
import { faCog } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Tables from 'components/Tables';
import Link from 'next/link';
import { UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
const PurchaseRequestForm = () => {
  const columns = [
    {
      name: '#',
    },
    {
      name: 'Requester',
    },

    {
      name: 'Approver',
    },
    {
      name: 'Desired Delivery Date	',
    },
    {
      name: 'Status',
    },
    {
      name: 'Actions',
    },
  ];
  const options = {
    filterType: 'dropdown',
    pagination: false,
    search: true,
    filter: true,
    download: false,
    print: false,
    viewColumns: false,
    toolbar: false,
    filterTable: false,
    searchOpen: true,
  };
  var statusBtn = (
    <button type="button" className="btn btn-success btn_active ">
      Pending
    </button>
  );

  var nameTitle = (
    <div>
      <p className="red_name">Vijendra Patil</p>
      <p className="mb-0">
        <a>Vijendra@gmail.com</a>
      </p>
    </div>
  );
  var approverTitle = (
    <div>
      <p className="red_name">Shrishailya Deshmukh</p>
      <p className="mb-0">
        <a>shri@gmail.com</a>
      </p>
    </div>
  );
  var action = (
    <UncontrolledDropdown className="action_btn">
      <DropdownToggle caret>
        <FontAwesomeIcon icon={faCog} />
      </DropdownToggle>
      <DropdownMenu className="action_menu">
        <DropdownItem>Settings1</DropdownItem>
        <DropdownItem> Settings2</DropdownItem>
      </DropdownMenu>
    </UncontrolledDropdown>
  );

  const userLoginData = [
    ['###', nameTitle, approverTitle, '1 Jan 2019', statusBtn, action],
    ['###', nameTitle, approverTitle, '2 Jan 2019', statusBtn, action],
    ['###', nameTitle, approverTitle, '3 Jan 2019', statusBtn, action],
  ];

  return (
    <section className="buy_back_color pt-50 mt-8 ">
      <div className="block_all">
        <div className="container">
          <div className="row">
            <div className="create_ueser  col-md-12">
              <div className="container">
                <Link href="/Quotes/Request/New">
                  <button className="btn btn-success create_user_new" type="button">
                    CREATE PURCHASE REQUESTS
                  </button>
                </Link>

                <div className="box box-widget">
                  <div className="box-body">
                    <Tables data={userLoginData} columns={columns} options={options} />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <style>{`
            .mt-8{
              margin-top:8%;
            }
            .pt-50 {
                padding-top: 50px;
            }
            
            .buy_back_color {
                background-color: #FFFFD7;
            }
            .create_user_new {
                background: #6DC144;
                border: 1px solid #6DC144 !important;
                padding: 10px 40px;
                font-size:12px;
                margin-bottom: 20px;
                
            }
            
            .btn-success {
                background-color: #00a65a;
                border-color: #008d4c ;
            }
            .btn {
                border-radius: 3px;
                -webkit-box-shadow: none;
                box-shadow: none;
                border: 1px solid transparent;
            }
            .btn-success:hover, .btn-success:active, .btn-success.hover {
                background-color: #008d4c;
            }
            .box-widget {
                border: 1px solid #e1dede;
                position: relative;
            }
            .box {
                position: relative;
                border-radius: 3px;
                background: #ffffff;
                border: 1px solid #ececec;
                margin-bottom: 20px;
                width: 100%;
                box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
            }
            .box-body {
                border-top-left-radius: 0;
                border-top-right-radius: 0;
                border-bottom-right-radius: 3px;
                border-bottom-left-radius: 3px;
                padding: 10px;
            }
            .action_btn button {
              font-size: 12px;
              width: 50px;
          }
          .mb-0 {
            margin-bottom: 0px;
        }
        .mb-0 a{
          color: #3c8dbc;
          font-size: 13px;
}
        }
        .red_name{
          font-size: 13px;
          color: red;

        }
        .action_menu{
          width: 100px;
    min-width: 100px;
        }
            
            `}</style>
    </section>
  );
};
export default PurchaseRequestForm;
