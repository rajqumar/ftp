import React from 'react';
import Dashboard from 'containers/Dashboard';

class DashboardPage extends React.PureComponent {
  render() {
    return <Dashboard />;
  }
}

export default DashboardPage;
