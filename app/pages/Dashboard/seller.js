import React from 'react';
import DashboardSeller from 'containers/DashboardSeller';

class DashboardSellerPage extends React.PureComponent {
  render() {
    return <DashboardSeller />;
  }
}

export default DashboardSellerPage;
