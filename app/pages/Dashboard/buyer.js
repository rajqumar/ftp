import React from 'react';
import DashboardBuyer from 'containers/DashboardBuyer';

class DashboardBuyerPage extends React.PureComponent {
  render() {
    return <DashboardBuyer />;
  }
}

export default DashboardBuyerPage;
