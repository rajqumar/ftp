import React from "react";
import withBox from 'components/Unused/Box'
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import MUIDataTable from 'mui-datatables';
import { UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import { faCog } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export function Table(props) {
    const getMuiTheme = () =>
    createMuiTheme({
      overrides: {
        MUIDataTable: {
          root: {},
          paper: {
            boxShadow: 'none',
          },
        },
        MUIDataTableBodyRow: {
          root: {
            '&:nth-child(odd)': {
              backgroundColor: '#dee2e6',
            },
          },
        },
        MUIDataTableBodyCell: {},
      },
    });
    var statusBtn = (
        <button type="button" className="btn btn-success btn_active ">
          Pending
        </button>
      );
    
      var nameTitle = (
        <div>
          <p className="red_name">Vijendra Patil</p>
          <p className="mb-0">
            <a>Vijendra@gmail.com</a>
          </p>
        </div>
      );
      var approverTitle = (
        <div>
          <p className="red_name">Shrishailya Deshmukh</p>
          <p className="mb-0">
            <a>shri@gmail.com</a>
          </p>
        </div>
      );
      var action = (
        <UncontrolledDropdown className="action_btn">
          <DropdownToggle caret>
            <FontAwesomeIcon icon={faCog} />
          </DropdownToggle>
          <DropdownMenu className="action_menu">
            <DropdownItem>Settings1</DropdownItem>
            <DropdownItem> Settings2</DropdownItem>
          </DropdownMenu>
        </UncontrolledDropdown>
      );
    const statsdata = props.data.data;
    const userLoginData = [
        ['###', nameTitle, approverTitle, '1 Jan 2019', statusBtn, action],
        ['###', nameTitle, approverTitle, '2 Jan 2019', statusBtn, action],
        ['###', nameTitle, approverTitle, '3 Jan 2019', statusBtn, action],
      ];
      const columns = [
        {
          name: '#',
        },
        {
          name: 'Requester',
        },
    
        {
          name: 'Approver',
        },
        {
          name: 'Desired Delivery Date	',
        },
        {
          name: 'Status',
        },
        {
          name: 'Actions',
        },
      ];
      const options = {
        filterType: 'dropdown',
        pagination: false,
        search: true,
        filter: true,
        download: false,
        print: false,
        viewColumns: false,
        toolbar: false,
        filterTable: false,
        searchOpen: true,
      };

    return (
        <React.Fragment>
        <MuiThemeProvider theme={getMuiTheme()}>
        <MUIDataTable title={''} data={userLoginData} columns={columns} options={options} />
        </MuiThemeProvider>

<style>{`
.table-responsive {
  min-height: .01%;
  overflow-x: auto;
}
.MuiIconButton-label{
  display:none;
}
.btn_inactive {
  background-color: #D31E25 !important;
  border: 1px solid #D31E25 !important;
  border-radius: 50px !important;
  font-size: 14px;
  padding: 0px 15px;
}
.btn_active
{
    background-color: #6DC144 !important;
  border: 1px solid #6DC144 !important;
  border-radius: 50px !important;
  font-size: 14px;
  padding: 0px 20px;
}
.btn-success {
  background-color: #00a65a;
  border-color: #008d4c;
}

.btn {
  border-radius: 3px;
  -webkit-box-shadow: none;
  box-shadow: none;
  border: 1px solid transparent;
}
.red_name{
color:red;
}
.mail_icon {
font-size: 20px;
color: #D2232A;
}
.mob_icon {
font-size: 24px;
margin-right: 5px;
color: #D2232A;
}

`}</style>
</React.Fragment>
    );
}

export default withBox(Table);