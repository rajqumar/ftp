import React from 'react';
import Document, { Html, Head, Main, NextScript } from 'next/document';
import { Global, css } from '@emotion/core';

class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx);
    return { ...initialProps };
  }

  render() {
    return (
      <Html>
        <Global
          styles={css`
            @import url('https://fonts.googleapis.com/css?family=Source+Sans+Pro&display=swap');

            body,
            html,
            container {
              font-family: 'Source Sans Pro';
              font-weight: 400;
              overflow-x: hidden;
              overflow-y: auto;
            }
          `}
        />
        <Head />
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default MyDocument;
