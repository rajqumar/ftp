import React from 'react';
import CreatePurchaseRequest from 'containers/CreatePurchaseRequest';

class CreatePurchaseRequestPage extends React.PureComponent {
  render() {
    return <CreatePurchaseRequest />;
  }
}

export default CreatePurchaseRequestPage;
