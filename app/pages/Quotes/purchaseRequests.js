import React from 'react';
import PurchaseRequest from 'containers/PurchaseRequest';

class PurchaseRequestPage extends React.PureComponent {
  render() {
    return <PurchaseRequest />;
  }
}

export default PurchaseRequestPage;
