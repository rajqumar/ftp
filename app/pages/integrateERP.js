import React from 'react';
import IntegrateERP from 'containers/IntegrateERP';

class IntegrateERPPage extends React.PureComponent {
  render() {
    return <IntegrateERP />;
  }
}

export default IntegrateERPPage;
