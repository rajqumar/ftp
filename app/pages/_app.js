import React from 'react';

import { Provider } from 'react-redux';
import { ToastProvider } from 'react-toast-notifications';

import Head from 'next/head';
import App from 'next/app';
import { persistStore } from 'redux-persist';
import { PersistGate } from 'redux-persist/integration/react';

import withReduxStore from 'utils/with-redux-store';
import 'typeface-metropolis';
import '@typefaces-pack/typeface-inter';
import 'bootstrap/dist/css/bootstrap.min.css';

class Srr extends App {
  constructor(props) {
    super(props);
    this.persistor = persistStore(props.reduxStore);
  }

  render() {
    const { Component, pageProps, reduxStore } = this.props;
    // let persistor = persistStore(reduxStore);

    return (
      <React.Fragment>
        <Head>
          <title>FTP</title>
        </Head>

        <ToastProvider>
          <Provider store={reduxStore}>
            <PersistGate loading={null} persistor={this.persistor}>
              <Component {...pageProps} />
            </PersistGate>
          </Provider>
        </ToastProvider>
      </React.Fragment>
    );
  }
}

export default withReduxStore(Srr);
