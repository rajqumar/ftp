import React, { useState } from 'react';
import { Button } from 'reactstrap'
import HomeLoggedOut from 'containers/HomeLoggedOut';

import Statistics from 'components/Unused/Cards/Statistics'
import H2 from 'components/Unused/H2'
import H3 from 'components/Unused/H3'
import H4 from 'components/Unused/H4'
import BoxForm from 'components/Unused/BoxTypes/BoxForm'
import Profile from 'components/Unused/BoxTypes/Profile'
import Graph from 'components/Unused/BoxTypes/Graph'
import RatingList from 'components/Unused/BoxTypes/RatingList'
import Receipt from 'components/Unused/BoxTypes/Receipt'
import ServiceProvider from 'components/Unused/BoxTypes/ServiceProvider'
import Stats from 'components/Unused/BoxTypes/Stats'
import Table from 'components/Unused/BoxTypes/Table'
import Modal from 'components/Unused/Modal'
import Advertisement from 'components/Unused/Cards/Advertisement'

import Layout from 'containers/Layout'

import { faBuilding } from '@fortawesome/free-solid-svg-icons';

class IndexPage extends React.PureComponent {

  constructor(props) {
    super(props);
    this.state = { 
      modal: false
    };
  }

  componentDidMount() {
    localStorage.setItem('ftp_user', false);
  }

  toggle = () => this.setState({modal: !this.state.modal});

  render() {
  
  console.log(this.state.modal)
  
  const modaldata = {
    title: 'Select Part',
    content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    btnTitle: 'Add to PR',
    btnColor: 'success'
  }

  const formdata = [ 
      {
      form : [{
      type: "text",
      name: "email",
      placeholder: "email@provider.com",
      id: "email",
      value: "",
      label: "Email",
      validation: ['required', 'email']
    }, {
      type: "password",
      name: "password",
      placeholder: "************",
      id: "password",
      value: "",
      label: "Password",
      validation: ['required', 'maxLength15']
    }]
    },
    { data: {heading: 'Seller Profile',
    rating: 4,
    icon: faBuilding}}
  ]

  const data = {
    heading: 'Buyer Profile',
    rating: 5,
    icon: faBuilding
  }

  const datasheet = [
    {
      label: 'somethingA',
  
      values: [
        { x: 'Jan', y: 57 },
        { x: 'Feb', y: 62 },
        { x: 'Mar', y: 9 },
        { x: 'Apr', y: 3 },
        { x: 'May', y: 66 },
        { x: 'Jun', y: 10 },
        { x: 'Jul', y: 28 },
        { x: 'Aug', y: 56 },
        { x: 'Sep', y: 83 },
        { x: 'Oct', y: 11 },
        { x: 'Nov', y: 75 },
        { x: 'Dec', y: 93 },
      ],
    },
  ];

  const ratings = [
    {
      icon: faBuilding,
      title: 'Payment',
      rating: 2
    },
    {
      icon: faBuilding,
      title: 'Responsiveness',
      rating: 3
    },
    {
      icon: faBuilding,
      title: 'Shipment',
      rating: 5
    }
  ]

  const receipt = {
    terms: 'PIA',
    validity: '60 Days',
    currency: 'USD',
    amount: '60.00',
    tax: '6.00',
    total: '66.00'
  }

  const serviceproviders = [
    {
      title: 'Service Provider 1',
      image: 'https://ftp-demo.hellosme.com/dist/img/service_provider1.png'
    },
    {
      title: 'Service Provider 2',
      image: 'https://ftp-demo.hellosme.com/dist/img/service_provider1.png'
    },
    {
      title: 'Service Provider 3',
      image: 'https://ftp-demo.hellosme.com/dist/img/service_provider1.png'
    },
    {
      title: 'Service Provider 4',
      image: 'https://ftp-demo.hellosme.com/dist/img/service_provider1.png'
    }
  ]

  const statsdata = [
    {
    count: 200,
    title: 'Quotations Sent'
    },
    {
      count: 150,
      title: 'RFQ Received'
    },
    {
      count: 45,
      title: 'Shipments Completed'
    }
  ]

     {/* <Advertisement /> */}

      {/* <Button color="danger" onClick={this.toggle}>Click</Button> */}

        {/* <BoxForm data={formdata} /> */}
        {/* { this.state.modal ? <Modal open={this.state.modal} modaldata={modaldata} /> : null } */}
        
        {/* <Graph data={datasheet} /> */}
        {/* <Profile data={data} /> */}
        {/* <Table data={data} /> */}
        
  return (
        <HomeLoggedOut />
    )
  }
}

export default IndexPage;
