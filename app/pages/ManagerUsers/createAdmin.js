import React from 'react';
import CreateAdmin from 'containers/CreateAdmin';

class CreateAdminPage extends React.PureComponent {
  render() {
    return <CreateAdmin title={'Create Admin'} />;
  }
}

export default CreateAdminPage;
