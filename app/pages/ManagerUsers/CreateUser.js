import React from 'react';
import CreateAdmin from 'containers/CreateAdmin';

class CreateUserPage extends React.PureComponent {
  render() {
    return <CreateAdmin title={'Create User'} />;
  }
}

export default CreateUserPage;
